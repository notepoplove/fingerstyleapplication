package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LogWriter;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.FileActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.MainActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.BackgroundListTab;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.CheckBoxItems;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.CustomAdapter;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_BPM;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_CREATOR;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_DATE;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_FROM_SONG_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_FROM_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_LONGTIME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_SIGNATURE;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_STATUS;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.TABLE_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.COLUMN_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.TABLE_USER_NAME;

/**
 * A simple {@link Fragment} subclass.
 */
public class PublicFragment extends Fragment implements View.OnClickListener,View.OnLongClickListener, TextWatcher {
    private View view;
    private TextView textViewItems;
    private EditText editTextSearch;

    private LinearLayout linearBackGroundTab;
    private BackgroundListTab SelectedBackgroundListTab;
    private BackgroundListTab[] linearBackGroundListTab;

    private CheckBoxItems checkBoxAllItems;
    private RelativeLayout relativeBackGroundItems;
    private RelativeLayout relativeHeadBackgroundPlay;
    private RelativeLayout relativeHeadBackgroundName;
    private RelativeLayout relativeHeadBackgroundCreator;
    private RelativeLayout relativeHeadBackgroundDate;
    private RelativeLayout relativeLineTab;
    private LinearLayout.LayoutParams layoutParamsListTab;
    private LinearLayout.LayoutParams layoutParamsItemsTab;
    private RelativeLayout.LayoutParams layoutLineTab;
    private RelativeLayout.LayoutParams layoutItemsTab;
    private RelativeLayout.LayoutParams layoutTextPlay;
    private AppCompatImageView imageViewSearch;
    private CircleImageView imageViewDownload;
    private CircleImageView imageViewDelete;


    private String userID = null;
    private String fileID = null;
    private String stringModeSearch;
    private float density;
    private int sizeDatabase;
    List<String> list;

    private SQLiteHelper sqLiteHelper;
    private Cursor cursor;
    private JsonObject jsonObjectFile;
    private JsonObject jsonObjectUser;
    private JsonObject jsonObject;
    private DecimalFormat decimalFormat;
    private Spinner spinnerModeShow;
    private SimpleDateFormat sdf;
//    private JSONArray jsonArray;


    public PublicFragment() {

        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_public, container, false);

        init();

        obscureImageButton();

        Cursor cursorUserId = getUserID();
        cursorUserId.moveToNext();
        Log.w("cursor",""+cursorUserId.getCount());
        if (cursorUserId.getCount() > 0) {
            userID = cursorUserId.getString(0);
            Log.w("UserID",""+userID);
        }
        else {
            userID = null;
            Log.w("UserID",""+userID);
        }
        stringModeSearch = "All";
        selectAllFileXML(stringModeSearch);

//        sqLiteHelper.close();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        editTextSearch.setText("");
        onBackPressed();
    }

    private void init() {
        linearBackGroundTab = (LinearLayout)view.findViewById(R.id.fragmentPublic_ll_backGroundTab);
        editTextSearch = (EditText)view.findViewById(R.id.fragmentPublic_editText_search);
        imageViewSearch = (AppCompatImageView)view.findViewById(R.id.fragmentPublic_image_search);
        imageViewDownload = (CircleImageView)view.findViewById(R.id.fragmentPublic_image_download);
        imageViewDelete = (CircleImageView)view.findViewById(R.id.fragmentPublic_image_delete);
        spinnerModeShow = (Spinner)view.findViewById(R.id.fragmentPublic_spinner_search);

        sqLiteHelper = new SQLiteHelper(getContext());
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        list = new ArrayList<String>();
        decimalFormat = new DecimalFormat(".##");
        ArrayList<String> data = new ArrayList<String>();

        data.add("All");
        data.add("Profile");

        CustomAdapter customAdapter = new CustomAdapter(getContext(), data);
        spinnerModeShow.setAdapter(customAdapter);
        spinnerModeShow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = spinnerModeShow.getSelectedItem().toString();
                Log.i("Spinner",""+text);
                stringModeSearch = text;
                selectAllFileXML(stringModeSearch);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        decimalFormat.setRoundingMode(RoundingMode.CEILING);

        editTextSearch.addTextChangedListener(this);

        imageViewDownload.setOnClickListener(this);
        imageViewDelete.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == imageViewDownload){
            if(fileID != null && userID !=null) {
                loadFileXML();
                setNumberDownload();
            }
        }
        else if(v == imageViewDelete){
            if(fileID != null && userID !=null) {
                deleteFile();
            }
        }
        else {
            if(SelectedBackgroundListTab != null) {
                SelectedBackgroundListTab.setBackgroundColor(this.getResources().getColor(R.color.colorWhiteTransparent));
            }
            BackgroundListTab backgroundListTab = (BackgroundListTab) v;
            backgroundListTab.setBackgroundColor(this.getResources().getColor(R.color.colorClick));
            if (userID != null) {
                Intent intent = new Intent(getActivity(), FileActivity.class);
                intent.putExtra("user_id", userID);
                intent.putExtra("file_id", backgroundListTab.getFileId()); //Optional parameters
                getActivity().startActivity(intent);
            }
        }

    }

    @Override
    public boolean onLongClick(View v) {
        obscureImageButton();
        SelectedBackgroundListTab = (BackgroundListTab)v;
        Log.w("FILEID", SelectedBackgroundListTab.getFileId());
        fileID = SelectedBackgroundListTab.getFileId();
        Log.w("Check UserId",userID +" = "+ SelectedBackgroundListTab.getUserId());
        if(userID!=null) {
            if (userID.equals(SelectedBackgroundListTab.getUserId().toString())) {
                showImageButton();
            } else {
                imageViewDownload.setVisibility(View.VISIBLE);
            }
            SelectedBackgroundListTab.setBackgroundColor(this.getResources().getColor(R.color.colorClick));
            for (int i = 0; i < linearBackGroundListTab.length; i++) {
                if (linearBackGroundListTab[i] != SelectedBackgroundListTab) {
                    linearBackGroundListTab[i].setBackgroundColor(this.getResources().getColor(R.color.colorWhiteTransparent));
                }
            }
        }
        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(editTextSearch.getText().length()>0){
            imageViewSearch.setVisibility(View.INVISIBLE);
        }
        else {
            imageViewSearch.setVisibility(View.VISIBLE);
        }
        selectAllFileXML(stringModeSearch);
    }

    public void onBackPressed()
    {
        fileID = null;
        try {
            obscureImageButton();
            SelectedBackgroundListTab.setBackgroundColor(this.getResources().getColor(R.color.colorWhiteTransparent));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        //Pop Fragments off backstack and do your other checks
    }

    private void selectAllFileXML(String modeShow) {
//        Log.i("Spinner",""+spinnerModeShow.);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id",userID);
        jsonObject.addProperty("mode_search",modeShow);
        jsonObject.addProperty("message_search",editTextSearch.getText().toString());
        Ion.with(getContext())
                .load("http://guitar.msuproject.net/public/manageFile.php/selectFile")
//                .setBodyParameter("message_search",editTextSearch.getText().toString())
                .setJsonObjectBody(jsonObject)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        Log.i("JsonArray",""+result.size());
                        showTabGuitar(result);
                    }
                });
    }

    private void loadFileXML() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("POST","http://guitar.msuproject.net/public/manageFile.php/selectFileByID")
                .setJsonObjectBody(jsonObject)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        final String userID = result.get("user_id").getAsString();
                        final String fileID = result.get("fileServer_id").getAsString();
                        final String fileName = result.get("fileServer_name").getAsString();
                        final String userName = result.get("user_name").getAsString();
                        final String fileBpm = result.get("fileServer_bpm").getAsString();
                        final String fileSignature = result.get("fileServer_signature").getAsString();
                        String str[] = fileSignature.split("/");
                        final String signature1 = str[0];
                        final String signature2 = str[1];
                        final String fileLongtime = result.get("fileServer_longtime").getAsString();
                        final String currentTime = sdf.format(new Date());
                        Log.i("Shoe","From ID:"+userID+" File ID"+fileID+" File Name:"+fileName+" User Name:"+userName+" File Bpm:"+fileBpm+" File Signature:"+fileSignature);
                        Ion.with(getContext())
                                .load("POST","http://guitar.msuproject.net/public/manageFile.php/downloadFileXML")
                                .setJsonObjectBody(jsonObject)
                                .asString()
                                .withResponse()
                                .setCallback(new FutureCallback<Response<String>>() {
                                    @Override
                                    public void onCompleted(Exception e, Response<String> result) {
                                        Log.i("XML",""+result.getResult());
                                        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
                                        ContentValues values = new ContentValues();
                                        values.put(COLUMN_NAME,fileName);
                                        values.put(COLUMN_CREATOR,userName);
                                        values.put(COLUMN_BPM,fileBpm);
                                        values.put(COLUMN_SIGNATURE,signature1+"/"+signature2);
                                        values.put(COLUMN_LONGTIME,fileLongtime);
                                        values.put(COLUMN_DATE,currentTime);
                                        values.put(COLUMN_STATUS,"download");
                                        values.put(COLUMN_FROM_SONG_ID,fileID);
                                        values.put(COLUMN_FROM_USER_ID,userID);
                                        db.insertOrThrow(TABLE_NAME,null,values);

                                        Cursor cursorMaxId = getMAxID();
                                        cursorMaxId.moveToNext();
                                        String path = saveXML(cursorMaxId.getString(0).toString(),fileName,result.getResult());
                                        setUpdatePath(path,cursorMaxId.getString(0).toString());
                                    }
                                });
                    }
                });
    }

    private void deleteFile() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("POST", "http://guitar.msuproject.net/public/manageFile.php/deleteFile")
                .setJsonObjectBody(jsonObject)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
//                        textViewDownload.setText(result.getResult());
                        Log.i("Number Download",""+result.getResult());
                        if(result.getResult().equals("Success")){
                            Toast.makeText(getContext(),"Delete success", Toast.LENGTH_LONG).show();
                            selectAllFileXML(stringModeSearch);
                        }
                    }
                });
    }

    private void setNumberDownload() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("POST", "http://guitar.msuproject.net/public/manageFile.php/numberDownloadFile")
                .setJsonObjectBody(jsonObject)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
//                        textViewDownload.setText(result.getResult());
                        Log.i("Number Download",""+result.getResult());
                    }
                });
    }

    private void showTabGuitar(JsonArray jsonArray) {
        int n =0;
        linearBackGroundTab.removeAllViews();
        linearBackGroundListTab = new BackgroundListTab[jsonArray.size()];
        try {
            for(int i=0;i<jsonArray.size();i++){
                Log.i("jsonArray",""+jsonArray.get(i));
                JsonObject jsonObject = (JsonObject)jsonArray.get(i);
    //            Log.w("ID",cursor.getString(0));
                if(getContext() != null) {
                    linearBackGroundListTab[n] = new BackgroundListTab(getContext());

                    linearBackGroundListTab[n].setOnClickListener(this);
                    linearBackGroundListTab[n].setOnLongClickListener(this);

                    linearBackGroundListTab[n].setUserId(jsonObject.get("user_id").getAsString());
                    linearBackGroundListTab[n].setFileId(jsonObject.get("fileServer_id").getAsString());

                    relativeLineTab = new RelativeLayout(getContext());

                    linearBackGroundListTab[n].setOrientation(LinearLayout.HORIZONTAL);
                    relativeLineTab.setBackgroundColor(this.getResources().getColor(R.color.colorWhite));


                    layoutParamsListTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(35));
                    layoutLineTab = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(2));
                    //            layoutParamsListTab.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                    for (int j = 0; j < 4; j++) {
                        relativeBackGroundItems = new RelativeLayout(getContext());
                        textViewItems = new TextView(getContext());
                        layoutItemsTab = new RelativeLayout.LayoutParams(textViewItems.getWidth(), textViewItems.getHeight());
                        layoutItemsTab.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                        layoutItemsTab.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        layoutItemsTab.addRule(RelativeLayout.CENTER_VERTICAL);
                        if (j == 0) {
                            //                    relativeBackGroundItems.setBackgroundColor(this.getResources().getColor(R.color.colorRed));
                            layoutParamsItemsTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT, 1);
                            textViewItems.setText(jsonObject.get("fileServer_name").getAsString());
                            layoutItemsTab.leftMargin = dpToPx(7);

                            relativeBackGroundItems.addView(textViewItems, layoutItemsTab);
                            linearBackGroundListTab[n].addView(relativeBackGroundItems, layoutParamsItemsTab);
                        } else if (j == 1) {
                            //                    relativeBackGroundItems.setBackgroundColor(this.getResources().getColor(R.color.colorWhite));
                            layoutParamsItemsTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT, 1);
                            textViewItems.setText(jsonObject.get("user_firstName").getAsString()+" "+jsonObject.get("user_lastName").getAsString());
                            layoutItemsTab.leftMargin = dpToPx(7);
                            relativeBackGroundItems.addView(textViewItems, layoutItemsTab);
                            linearBackGroundListTab[n].addView(relativeBackGroundItems, layoutParamsItemsTab);
                        } else if (j == 2) {
                            //                    relativeBackGroundItems.setBackgroundColor(this.getResources().getColor(R.color.colorGreen));
                            layoutParamsItemsTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT, 1);
//                            textViewItems.setText(jsonObject.get("fileServer_date").getAsString());
                            String dateString = jsonObject.get("fileServer_date").getAsString();
                            try {
                                Date date = sdf.parse(dateString);
                                SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy HH:mm");
                                textViewItems.setText(formatter.format(date));
                            } catch (ParseException ex) {
                                Log.v("Exception", ex.getLocalizedMessage());
                            }
                            layoutItemsTab.leftMargin = dpToPx(7);

                            relativeBackGroundItems.addView(textViewItems, layoutItemsTab);
                            linearBackGroundListTab[n].addView(relativeBackGroundItems, layoutParamsItemsTab);

                        } else if (j == 3) {
                            //                    relativeBackGroundItems.setBackgroundColor(this.getResources().getColor(R.color.colorWhite));

                            Double score = Double.parseDouble(jsonObject.get("score_song").getAsString());
                            if (score > 0) {
                                textViewItems.setText("" + decimalFormat.format(score));
                            } else {
                                textViewItems.setText("0");
                            }

                            layoutParamsItemsTab = new LinearLayout.LayoutParams(dpToPx(50), ViewGroup.LayoutParams.MATCH_PARENT);
                            layoutItemsTab.addRule(RelativeLayout.CENTER_IN_PARENT);

                            relativeBackGroundItems.addView(textViewItems, layoutItemsTab);
                            linearBackGroundListTab[n].addView(relativeBackGroundItems, layoutParamsItemsTab);
                        }
                    }

                    linearBackGroundTab.addView(linearBackGroundListTab[n], layoutParamsListTab);
                    linearBackGroundTab.addView(relativeLineTab, layoutLineTab);
                    n++;
                }
            }
        } catch (Exception e1) {
//            Log.i("JSONException","Kuy");
            e1.printStackTrace();
        }
    }

    private String saveXML(String fileID, String fileName, String fileXML) {
        String path = "";
        StringWriter writer = new StringWriter();
        Log.i("MAX Id",""+fileID);
        Log.i("XML",""+writer.toString());

        File internalDir = getActivity().getFilesDir();
        File dir = new File(internalDir.getAbsolutePath()+"/Fingerstyle XML");
        if(!dir.exists()){
            dir.mkdir();
        }

        File file = new File(dir,"FID"+fileID+fileName+".xml");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(fileXML.toString().getBytes());
            fileOutputStream.close();
            Toast.makeText(getContext(),"Download success", Toast.LENGTH_LONG).show();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        path = file.getAbsolutePath();
        Log.w("PATH",path);
        return path;
    }

    private void obscureImageButton() {
        imageViewDownload.setVisibility(View.INVISIBLE);
        imageViewDelete.setVisibility(View.INVISIBLE);
    }
    private void showImageButton() {
        imageViewDownload.setVisibility(View.VISIBLE);
        imageViewDelete.setVisibility(View.VISIBLE);
    }

    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    private Cursor getDatabase() {
        String sql = "SELECT * FROM FileTabGuitar";
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    private Cursor getUserID() {
        String sql = "SELECT "+COLUMN_USER_ID+" FROM "+ TABLE_USER_NAME;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }

    private Cursor getMAxID() {
        String sql = "SELECT MAX(_id) FROM FileTabGuitar";
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    private void setUpdatePath(String path,String fileId) {
        String sql = "UPDATE FileTabGuitar SET path = ? WHERE _id = ?";
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        String[] dataPath = {path,fileId};
        db.execSQL(sql,dataPath);
    }

    public EditText getEditTextSearch() {
        return editTextSearch;
    }

    public void setEditTextSearch(EditText editTextSearch) {
        this.editTextSearch = editTextSearch;
    }

}
