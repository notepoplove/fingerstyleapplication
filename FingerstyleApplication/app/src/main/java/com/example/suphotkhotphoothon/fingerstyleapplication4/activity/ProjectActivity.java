package com.example.suphotkhotphoothon.fingerstyleapplication4.activity;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.CreateProjectFragment;

public class ProjectActivity extends AppCompatActivity implements View.OnClickListener{

    private AppCompatImageButton imageButtonBack;
    private FragmentTransaction fragmentTran;
    private FrameLayout frameProject;
    private FragmentManager fragmentManager;
    private CreateProjectFragment createProjectFragment;
    private AppCompatImageButton imageButtonBackProject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        fragmentTran = getSupportFragmentManager().beginTransaction();

        frameProject = (FrameLayout) findViewById(R.id.project_frame);
        imageButtonBackProject = (AppCompatImageButton)findViewById(R.id.project_imb_back);
        fragmentManager = getSupportFragmentManager();
        createProjectFragment = new CreateProjectFragment();

        setFragment(createProjectFragment);

        imageButtonBackProject.setOnClickListener(this);
    }
    private void setFragment(Fragment fragment) {
        fragmentTran.replace(frameProject.getId(),fragment);
        //fragmentTran.addToBackStack(null);
        fragmentTran.commit();
    }

    @Override
    public void onClick(View v) {
        if(v == imageButtonBackProject){
            onBackPressed();
//            if(fragmentManager.getBackStackEntryCount()>0){
//                fragmentManager.popBackStack();
//            }
//            else {
//                Toast.makeText(ProjectActivity.this,"Back",Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(ProjectActivity.this, MainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                finish();
//            }
        }
    }
}
