package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import java.util.ArrayList;

public class StringListener implements View.OnClickListener {
    TabActivity tabActivity;
    ArrayList<Note> collectionNote;
    AppCompatImageView imageViewStringNumber[];
    RelativeLayout relativeBackground[];
    RelativeLayout relativeBackgroundString[];
    Note selectionNote[];
    int stingNumber = 0;

    public StringListener(TabActivity tabActivity,int stingNumber) {
        this.tabActivity = tabActivity;
        this.stingNumber = stingNumber;
        this.collectionNote = tabActivity.collectionNote;
        this.imageViewStringNumber =tabActivity.imageViewStringNumber;
        this.relativeBackgroundString = tabActivity.relativeBackgroundString;
        this.relativeBackground = tabActivity.relativeBackground;
        this.selectionNote = tabActivity.selectionNote;
    }
    @Override
    public void onClick(View v) {
//        Log.i("Sting Number",""+stingNumber);
//        Log.i("SIZE",""+collectionNote.size());
        if(selectionNote[0]!=null){
            selectionNote[0].setString(stingNumber);

            tabActivity.linearBackGroundTab.removeAllViews();
            for(int i=0;i<relativeBackgroundString.length;i++){
                relativeBackgroundString[i].removeAllViews();
            }
            for(int i=0;i<relativeBackground.length;i++){
                relativeBackground[i].removeAllViews();
            }
            tabActivity.createTab();

            imageViewStringNumber[stingNumber-1].setBackgroundResource(R.color.colorGreen);
            for(int i=0;i<imageViewStringNumber.length;i++){
                if (i!=stingNumber-1){
                    imageViewStringNumber[i].setBackgroundResource(0);
                }
            }
            selectionNote[0].setBackgroundResource(R.color.colorGreen);
        }
        else if(collectionNote.size()>0){

            for(int i=0;i<collectionNote.size();i++) {
                collectionNote.get(i).setString(stingNumber);
            }

            tabActivity.linearBackGroundTab.removeAllViews();
            for(int i=0;i<relativeBackgroundString.length;i++){
                relativeBackgroundString[i].removeAllViews();
            }
            for(int i=0;i<relativeBackground.length;i++){
                relativeBackground[i].removeAllViews();
            }
            tabActivity.createTab();

            imageViewStringNumber[stingNumber-1].setBackgroundResource(R.color.colorGreen);
            for(int i=0;i<imageViewStringNumber.length;i++){
                if (i!=stingNumber-1){
                    imageViewStringNumber[i].setBackgroundResource(0);
                }
            }
        }

    }
}
