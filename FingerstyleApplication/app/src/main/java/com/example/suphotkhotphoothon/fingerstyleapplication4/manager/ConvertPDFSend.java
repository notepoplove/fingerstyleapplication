package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.PDFActivity;
import com.google.gson.JsonObject;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ConvertPDFSend {
    private String path;
    private JsonObject jsonObject;
    public ConvertPDFSend(final PDFActivity pdfActivity, ArrayList<LinearLayout> pdf, final String name, final String email) {

        File internalDir = Environment.getExternalStorageDirectory();
        File dir = new File(internalDir.getAbsolutePath()+"/Send PDF");//tabEX.pdf
        if(!dir.exists()){
            dir.mkdir();
        }
        String directoryPath =  dir.getAbsolutePath();
        File file = new File(directoryPath, pdfActivity.nameSong+".pdf");
        Log.w("Path:",""+file.getAbsolutePath());
        path = file.getAbsolutePath();

        Document document = new Document(PageSize.A4, 20, 20, 20, 20);
        try {
            PdfWriter.getInstance(document, new FileOutputStream(file)); //  Change pdf's name.
            document.open();
            for(int i=0;i<pdf.size();i++){
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                pdf.get(i).setDrawingCacheEnabled(true);
                pdf.get(i).measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                pdf.get(i).buildDrawingCache(true);
                Bitmap b = Bitmap.createBitmap(pdf.get(i).getDrawingCache());
                pdf.get(i).setDrawingCacheEnabled(false);
                b.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());
                float scale = ((document.getPageSize().getWidth() - document.leftMargin()
                        - document.rightMargin() - 0) / image.getWidth()) * 100;
                image.scalePercent(scale);
                image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
                document.add(image);
            }
//            Toast.makeText(pdfActivity,"Save success", Toast.LENGTH_LONG).show();
            document.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            File filesSend = new File(path);
            FileInputStream stream = new FileInputStream(filesSend);
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int len = 0;
            while ((len = stream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            byte[] pdfByte = byteBuffer.toByteArray();
            String encodedPdf = Base64.encodeBytes(pdfByte);
            Log.w("Byte:",""+encodedPdf);
            jsonObject = new JsonObject();
            jsonObject.addProperty("file_name",pdfActivity.nameSong);
            jsonObject.addProperty("file_base64",encodedPdf);
            Ion.with(pdfActivity)
                    .load("POST","http://guitar.msuproject.net/public/manageFile.php/sendMail")
                    .setJsonObjectBody(jsonObject)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            Log.i("Result",""+result);
                            Intent intent = new Intent(Intent.ACTION_SEND);
//                            intent.setType("message/rfc822");
                            intent.setType("text/html");
                            intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"recipient@example.com"});
                            intent.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                            Uri myUri = Uri.parse("http://"+result);
                            String body = "<a href=\"" + myUri + "\">" + myUri+ "</a>";
                            intent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(body));
                            try {
                                pdfActivity.startActivity(Intent.createChooser(intent, "Send mail..."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(pdfActivity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            boolean deleted = file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
