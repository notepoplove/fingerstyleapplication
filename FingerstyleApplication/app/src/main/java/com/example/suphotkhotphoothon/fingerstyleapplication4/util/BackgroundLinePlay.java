package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;

public class BackgroundLinePlay extends RelativeLayout {
    TabActivity tabActivity;
    ImageView imageViewLine;
    private float density;
    public BackgroundLinePlay(TabActivity tabActivity) {
        super(tabActivity);
        this.tabActivity = tabActivity;
        imageViewLine = new ImageView(tabActivity);
        imageViewLine.setBackgroundColor(tabActivity.getResources().getColor(R.color.colorRed));
        LayoutParams layoutParamsString = layoutParamsString();
        addView(imageViewLine,layoutParamsString);
    }

    public ImageView getImageViewLine(){ return this.imageViewLine; }
    public void setImageViewLine(ImageView imageViewLine){
        this.imageViewLine = imageViewLine;
    }

    private LayoutParams layoutParamsString() {
        LayoutParams layoutParamsString = new LayoutParams(dpToPx(3),ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParamsString.addRule(RelativeLayout.CENTER_HORIZONTAL);
        return layoutParamsString;
    }
    public int dpToPx(int dp) {
        density = tabActivity.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
}
