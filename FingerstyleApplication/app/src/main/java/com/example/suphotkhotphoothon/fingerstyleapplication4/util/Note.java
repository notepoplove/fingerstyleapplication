package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.RelativeLayout;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;

public class Note extends AppCompatImageButton implements Comparable<Note>{
    private int string;
    private int fret;
    private int time;
    private boolean room;
    public Note(Context context,int string,int fret,int time,boolean room) {
        super(context);
        this.string = string;
        this.fret = fret;
        this.time = time;
        this.room = room;
    }

    public int getString() {
        return string;
    }

    public void setString(int string) {
        this.string = string;
    }

    public int getFret() {
        return fret;
    }

    public void setFret(int fret) {
        this.fret = fret;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean getRoom() {
        return room;
    }

    public void setRoom(boolean room) {
        this.room = room;
    }
    @Override
    public int compareTo(@NonNull Note note) {
        int compareTime = ((Note)note).getTime();
        return this.time - compareTime;
    }
}
