package com.example.suphotkhotphoothon.fingerstyleapplication4.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.AccountFragment;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.LoginFragment;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.COLUMN_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.TABLE_USER_NAME;

public class AccountActivity extends AppCompatActivity implements View.OnClickListener{

    private FragmentTransaction fragmentTran;
    private FrameLayout frameAccount;
    private FragmentManager fragmentManager;
    private AppCompatImageButton imageButtonBackAccount;
    private AppCompatImageView imageViewLogout;

    private LoginFragment loginFragment;
    private AccountFragment accountFragment;
    SQLiteHelper sqLiteHelper;
    private Cursor cursor;
    private String userID = null;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        frameAccount = (FrameLayout) findViewById(R.id.account_frame);
        imageButtonBackAccount = (AppCompatImageButton)findViewById(R.id.account_imb_back);
        imageViewLogout = (AppCompatImageView)findViewById(R.id.account_imb_logout);

        loginFragment = new LoginFragment();
        accountFragment = new AccountFragment();
        fragmentManager = getSupportFragmentManager();
        sqLiteHelper = new SQLiteHelper(this);

        fragmentTran = getSupportFragmentManager().beginTransaction();

        cursor = getUserID();
        cursor.moveToNext();
        Log.w("cursor",""+cursor.getCount());
        if (cursor.getCount() > 0) {
            userID = cursor.getString(0);
            Log.w("UserID",""+userID);
            manageFragment(accountFragment);
        }
        else {
            userID = null;
            Log.w("UserID",""+userID);
            manageFragment(loginFragment);
        }



        imageButtonBackAccount.setOnClickListener(this);
        imageViewLogout.setOnClickListener(this);
    }

    private void manageFragment(Fragment fragment) {
        fragmentTran = getSupportFragmentManager().beginTransaction();
        if(fragment == loginFragment){
            imageViewLogout.setVisibility(View.INVISIBLE);
            fragmentTran.replace(frameAccount.getId(),fragment);
        }
        else if(fragment == accountFragment){
            imageViewLogout.setVisibility(View.VISIBLE);
            fragmentTran.replace(frameAccount.getId(),fragment);
        }
//        fragmentTran.addToBackStack(null);
        fragmentTran.commit();
    }

    @Override
    public void onClick(View v) {

        if(v == imageButtonBackAccount){
            if(fragmentManager.getBackStackEntryCount()>0){
                fragmentManager.popBackStack();
            }
            else {
                Toast.makeText(AccountActivity.this,"Back",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(AccountActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }
        else if(v == imageViewLogout){
            messageDialogLogout();
        }
    }

    private void messageDialogLogout() {
        String message = "";
        new AlertDialog.Builder(this)
                .setIcon(R.mipmap.logout_32)
                .setTitle("Logout")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteUser();
                        cursor = getUserID();
                        cursor.moveToNext();
                        Log.w("cursor",""+cursor.getCount());
                        if (cursor.getCount() > 0) {
                            userID = cursor.getString(0);
                            Log.w("UserID",""+userID);
                            manageFragment(accountFragment);
                        }
                        else {
                            userID = null;
                            Log.w("UserID",""+userID);
                            manageFragment(loginFragment);
                        }

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Cancel","Cancel");
                    }
                })
                .show();
    }

    private Cursor getUserID() {
        String sql = "SELECT "+COLUMN_USER_ID+" FROM "+ TABLE_USER_NAME;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }

    private void deleteUser() {
        String sql = "DELETE FROM "+ TABLE_USER_NAME;
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        db.execSQL(sql);
    }
//    public String getUserId(){
//        return this.userID;
//    }
//    public void setUserId(String userID){
//        this.userID = userID;
//    }
}
