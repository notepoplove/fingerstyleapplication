package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import java.util.ArrayList;

public class SeekBarOnClickListener implements SeekBar.OnSeekBarChangeListener {
    TabActivity tabActivity;
    Note note;
    SeekBar seekBarFret;

    public SeekBarOnClickListener(TabActivity tabActivity, Note note, SeekBar seekBarFret) {
        this.tabActivity = tabActivity;
        this.note = note;
        this.seekBarFret = seekBarFret;
        if(note == null){
            setFretSeekBar(0);
        }
        else{
            setFretSeekBar(note.getFret());
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        seekBar.setThumb(getThumb(progress));
//        note.setFret(progress);
        note.setFret(progress);
        note.setImageBitmap(Bitmap.createScaledBitmap(tabActivity.getImageBitmap(progress), note.getWidth(), note.getHeight(), false));
        tabActivity.saveFileXML();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public Drawable getThumb(int progress) {
        TextView textView = new TextView(tabActivity);
        textView.setText(""+progress);
        textView.setTextSize(16);
        textView.setTextColor(Color.parseColor("#2adbc9"));

        textView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(textView.getMeasuredWidth(), textView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        textView.layout(0, 0, textView.getMeasuredWidth(), textView.getMeasuredHeight());
        textView.draw(canvas);

        return new BitmapDrawable(tabActivity.getResources(), bitmap);
    }
    private void setFretSeekBar(int fret){
        this.seekBarFret.setProgress(fret);
        this.seekBarFret.setThumb(getThumb(fret));
    }
}
