package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.widget.LinearLayout;

public class BackgroundListTab extends LinearLayout{
    String userId = null;
    String fileId = null;
    public BackgroundListTab(Context context) {
        super(context);

    }
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public void setFileId(String fileId){
        this.fileId = fileId;
    }
    public String getFileId(){
        return this.fileId;
    }
}
