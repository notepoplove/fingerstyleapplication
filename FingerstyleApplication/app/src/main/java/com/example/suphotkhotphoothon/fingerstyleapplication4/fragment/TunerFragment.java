package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.SilenceDetector;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TunerFragment extends Fragment implements View.OnClickListener{

    private View view;

    private SilenceDetector silenceDetector;
    private AudioDispatcher dispatcher;

    private TextView textViewFrequency;
    private TextView textViewCheck;
    private TextView textStandardString;
    private TextView textStatusSting;
    private RelativeLayout relativeCheckFrequency;
    private RelativeLayout relativeBackgroundStatus;
    private LinearLayout linearBackgroundTuner;
    private AppCompatImageView imageViewCheckFrequency;
    private CircleImageView imageViewString[] = new CircleImageView[6];

    private ArrayList<String> countData;
    private ArrayList<Float> modelFrequency;
    private ArrayList<String> boxFrequency;
    private ArrayList<String> audioInfo;

    private String checkClickString = null;
    private int countAudioInfo = 0;
    private int moveX = 250;
    private int maximumFrequency = 22;
    private float difference[] = new float[25+maximumFrequency];
    private float density;
    private float pitchInHz;
    private float resultFrequency = 0;
    private float frequency[] = new float[6];
    private double levelAudio;
    private boolean checkEject = true;

    private Handler handler;
    private Runnable runnable;

    public TunerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tuner, container, false);

        init();

        double threshold = 0;
        dispatcher = AudioDispatcherFactory.fromDefaultMicrophone( 44100, AudioRecord.getMinBufferSize
                (44100, AudioFormat.CHANNEL_IN_MONO,AudioFormat.ENCODING_PCM_16BIT),0);
        silenceDetector = new SilenceDetector(threshold,false);
        dispatcher.addAudioProcessor(silenceDetector);
        detector();
        Log.w("TunerFragment","TunerFragment");
        new Thread(dispatcher,"Audio Dispatcher").start();

        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.w("DestroyView","DestroyView");
        dispatcher.stop();
//        handler.removeCallbacksAndMessages(runnable);
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        Future longRunningTaskFuture = executorService.submit(runnable);
//        longRunningTaskFuture.cancel(true);
    }

    private void init() {
        textViewFrequency = (TextView)view.findViewById(R.id.fragmentTuner_textView_frequency);
        textStandardString = (TextView)view.findViewById(R.id.fragmentTuner_textView_standardString);
        textStatusSting = (TextView)view.findViewById(R.id.fragmentTuner_textView_statusString);
        relativeCheckFrequency = (RelativeLayout)view.findViewById(R.id.fragmentTuner_rl_backgroundFrequency);
        relativeBackgroundStatus = (RelativeLayout)view.findViewById(R.id.fragmentTuner_rl_backgroundStatus);
        linearBackgroundTuner = (LinearLayout)view.findViewById(R.id.fragmentTuner_ll_backgroundTuner) ;
        imageViewCheckFrequency = (AppCompatImageView)view.findViewById(R.id.fragmentTuner_image_checkFrequency);
        imageViewString[0] = (CircleImageView)view.findViewById(R.id.fragmentTuner_image_sting1);
        imageViewString[1] = (CircleImageView)view.findViewById(R.id.fragmentTuner_image_sting2);
        imageViewString[2] = (CircleImageView)view.findViewById(R.id.fragmentTuner_image_sting3);
        imageViewString[3] = (CircleImageView)view.findViewById(R.id.fragmentTuner_image_sting4);
        imageViewString[4] = (CircleImageView)view.findViewById(R.id.fragmentTuner_image_sting5);
        imageViewString[5] = (CircleImageView)view.findViewById(R.id.fragmentTuner_image_sting6);

        boxFrequency = new ArrayList<String>();
        modelFrequency = new ArrayList<Float>();
        audioInfo = new ArrayList<String>();
        countData = new ArrayList<String>();

        standardFrequency();
        checkClickString = "1";

        frequency[0] = 329.63f;
        frequency[1] = 246.94f;
        frequency[2] = 196.00f;
        frequency[3] = 146.83f;
        frequency[4] = 110.00f;
        frequency[5] = 82.42f;

        resultFrequency = frequency[0];
        imageViewString[0].setCircleBackgroundColorResource(R.color.colorYellow);
        textStandardString.setText("("+frequency[0]+"Hz)");

        setFocus();

//        imageViewString1.setCircleBackgroundColorResource(R.color.colorGreen);
        for(int i=0;i<imageViewString.length;i++){
            imageViewString[i].setOnClickListener(this);
        }
    }
    @Override
    public void onClick(View v) {

        for(int i=0;i<imageViewString.length;i++){
            imageViewString[i].setCircleBackgroundColorResource(R.color.colorWhite);
        }

        if(v == imageViewString[0]){
            checkClickString = "1";
            textStandardString.setText("("+frequency[0]+"Hz)");
            resultFrequency = frequency[0];
            imageViewString[0].setCircleBackgroundColorResource(R.color.colorYellow);
        }
        else if(v == imageViewString[1]){
            checkClickString = "2";
            textStandardString.setText("("+frequency[1]+"Hz)");
            resultFrequency = frequency[1];
            imageViewString[1].setCircleBackgroundColorResource(R.color.colorYellow);
        }
        else if(v == imageViewString[2]){
            checkClickString = "3";
            textStandardString.setText("("+frequency[2]+"Hz)");
            resultFrequency = frequency[2];
            imageViewString[2].setCircleBackgroundColorResource(R.color.colorYellow);
        }
        else if(v == imageViewString[3]){
            checkClickString = "4";
            textStandardString.setText("("+frequency[3]+"Hz)");
            resultFrequency = frequency[3];
            imageViewString[3].setCircleBackgroundColorResource(R.color.colorYellow);
        }
        else if(v == imageViewString[4]){
            checkClickString = "5";
            textStandardString.setText("("+frequency[4]+"Hz)");
            resultFrequency = frequency[4];
            imageViewString[4].setCircleBackgroundColorResource(R.color.colorYellow);
        }
        else if(v == imageViewString[5]){
            checkClickString = "6";
            textStandardString.setText("("+frequency[5]+"Hz)");
            resultFrequency = frequency[5];
            imageViewString[5].setCircleBackgroundColorResource(R.color.colorYellow);
        }
    }

    public void detector(){
        dispatcher.addAudioProcessor(new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.FFT_YIN, 44100, AudioRecord.getMinBufferSize
                (44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT), new PitchDetectionHandler() {
            @Override
            public void handlePitch(PitchDetectionResult pitchDetectionResult,
                                    final AudioEvent audioEvent) {
                pitchInHz = pitchDetectionResult.getPitch();
                levelAudio = silenceDetector.currentSPL();
                if(getActivity() != null){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (levelAudio >= -70 && pitchInHz != -1) {
//                                Log.i("Result: ", String.format("%.4f", pitchInHz) + "#" + String.format("%.4f", levelAudio));
//                            Log.i("Height",""+relativeCheckFrequency.getWidth());
//                                audioInfo.add("0#" + String.format("%.4f", pitchInHz) + "#" + String.format("%.4f", levelAudio));
                                textViewFrequency.setText(String.format("%.3f", pitchInHz) + "Hz");
//                                Log.w("Check",""+countAudioInfo+" | "+checkEject);
                                resultFrequency = pitchInHz;
                                /*if(countAudioInfo>=0 && countAudioInfo<5 && checkEject){
//                                    Log.i("Result: ", String.format("%.4f", pitchInHz) + "#" + String.format("%.4f", levelAudio));
                                    audioInfo.add("0#" + String.format("%.4f", pitchInHz) + "#" + String.format("%.4f", levelAudio));
                                }
                                else if(checkEject){
                                    countAudioInfo = 0;
                                    checkEject = false;
                                    String str[] =  detectoinFrequency(modelFrequency,audioInfo).split("#");
//                                    resultFrequency =  Float.parseFloat(str[1]);
                                    Log.w("Send1","-> "+ resultFrequency);
                                    audioInfo.clear();
                                }
                                countAudioInfo++;
                                */
                            }
                            /*else {
//                                Log.i("Result: ", String.format("%.4f", pitchInHz) + "#" + String.format("%.4f", levelAudio));
                                pitchInHz = 0;
                                if(audioInfo.size()!=0) {
                                    String str[] =  detectoinFrequency(modelFrequency,audioInfo).split("#");
                                    resultFrequency =  Float.parseFloat(str[1]);
                                    Log.w("Send2","-> "+ resultFrequency);
                                }
                                countAudioInfo = 0;
                                checkEject = true;
                                audioInfo.clear();
//                                Log.w("--------------","------------");
                            }*/
                        }
                    });
                }
            }
        }));
    }

    private void setFocus(){
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                int numberString = Integer.parseInt(checkClickString);
                int positionX = Math.round(relativeCheckFrequency.getWidth()/(frequency[numberString-1]*2)*resultFrequency);
                if (positionX > moveX) {
                    if(moveX<relativeCheckFrequency.getWidth()-(imageViewCheckFrequency.getWidth()/2)) {
                        moveX++;
                    }
                } else if (positionX < moveX) {
                    if(moveX>(imageViewCheckFrequency.getWidth()/2)) {
                        moveX--;
                    }
                }
                if(resultFrequency<frequency[numberString-1]-1){
                    textStatusSting.setText("SLACK");
                    relativeBackgroundStatus.setBackgroundResource(R.color.colorWhite);
                }
                else if(resultFrequency>frequency[numberString-1]+1){
                    textStatusSting.setText("TIGHT");
                    relativeBackgroundStatus.setBackgroundResource(R.color.colorWhite);
                }
                else {
                    textStatusSting.setText("STANDARD");
                    relativeBackgroundStatus.setBackgroundResource(R.color.colorGreen);
                }
                Log.i("moveX",frequency[numberString-1]+"Hz : "+moveX+" Px");

                RelativeLayout.LayoutParams paramsRelativeLayoutX = paramsRelativeLayoutX(imageViewCheckFrequency.getWidth(),moveX);
                imageViewCheckFrequency.setLayoutParams(paramsRelativeLayoutX);
                handler.postDelayed(this, 0);
            }
        };

        handler.postDelayed(runnable, 0);
    }


    public void standardFrequency(){
        for(int i=0;i<6;i++){
            for(float j=0;j<=maximumFrequency;j++) {
                float frequency = 0;
                int checkBreak = 0;
                if(i==0){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*82.41));
                    if(j==4){ checkBreak =1; }
                }
                else if(i==1){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*110.00));
                    if(j==4){ checkBreak =1; }
                }
                else if(i==2){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*146.83));
                    if(j==4){ checkBreak =1; }
                }
                else if(i==3){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*196.00));
                    if(j==3){ checkBreak =1; }
                }
                else if(i==4){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*246.94));
                    if(j==4){ checkBreak =1; }
                }
                else if(i==5){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*329.63));
                }
                modelFrequency.add(frequency);
                if(checkBreak ==1){
                    break;
                }
            }
        }
    }

    private String detectoinFrequency(ArrayList<Float> modelFrequency , ArrayList<String> boxFrequency) {
        for(int i=0;i<boxFrequency.size();i++) {
            String str[] = boxFrequency.get(i).split("#");
            for(int j=0;j<modelFrequency.size();j++) {
                difference[j] = Math.abs(Float.parseFloat(str[1])-modelFrequency.get(j));
            }
            int indexDifferrence=0;
            float minDifference = difference[0];
            for(int j=0;j<difference.length;j++) {
                if(difference[j]<minDifference) {
                    minDifference = difference[j];
                    indexDifferrence = j;
                }
            }
            boxFrequency.set(i, str[0]+"#"+modelFrequency.get(indexDifferrence)+"#"+str[2]);
        }
        String result;
        countData.clear();
        for(int i=0;i<boxFrequency.size();i++) {
            String strBoxFrequency[] = boxFrequency.get(i).split("#");
            if(countData.size()==0) {
                countData.add(boxFrequency.get(i)+"#"+1);
            }
            else {
                int check = 0;
                for(int j=0;j<countData.size();j++) {
                    String strCountData[] = countData.get(j).split("#");
                    if(strCountData[1].equals(strBoxFrequency[1])) {
                        check = 1;
                        int number  = Integer.parseInt(strCountData[3]);
                        number++;
                        countData.set(j, strCountData[0]+"#"+strCountData[1]+"#"+strCountData[2]+"#"+number);
                    }
                }
                if(check == 0) {
                    countData.add(boxFrequency.get(i)+"#"+1);
                }
            }
        }
        result = countData.get(0);
        for(int i=0;i<countData.size();i++) {
            String str[] = result.split("#");
            String strCountData[] = countData.get(i).split("#");
            if(Integer.parseInt(str[3])<Integer.parseInt(strCountData[3])) {
                result = countData.get(i);
            }
        }
        String strBoxFrequency[] = boxFrequency.get(0).split("#");
        String strResult[] = result.split("#");
        result = strBoxFrequency[0]+"#"+strResult[1]+"#"+strBoxFrequency[2]+"#"+strResult[3];
        return result;
    }


    private void resultNote(String result) {
        String splitAudioInfo[] = result.split("#");
        String fretString[] = checkFret(splitAudioInfo[1]).split("#");
        Log.w("NOTE",fretString[0]+"#"+fretString[1]+"#"+splitAudioInfo[0]);
//        resultFrequency.add(fretString[0]+"#"+fretString[1]+"#"+splitAudioInfo[0]);/

    }

    private String checkFret(String frequency) {
        int indextString=0,indexFret =0;
        for(int i=0;i<6;i++){
            indextString = i;
            int checkBack = 0;
            float j=0;
            for(;j<=maximumFrequency;j++) {
                indexFret = Math.round(j);
                float frequencyModel =0;
                if(i==0){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*329.63));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==1){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*246.94));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==2){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*196.00));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==3){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*146.83));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==4){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*110.00));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==5){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*82.41));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
            }
            if(checkBack == 1){ break; }
        }
        return (indextString+1)+"#"+indexFret;
    }


    private RelativeLayout.LayoutParams paramsRelativeLayoutX(int widthImageCheck, int moveX) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = moveX - (widthImageCheck/2);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        return params;
    }

    public AudioDispatcher getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(AudioDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

}
