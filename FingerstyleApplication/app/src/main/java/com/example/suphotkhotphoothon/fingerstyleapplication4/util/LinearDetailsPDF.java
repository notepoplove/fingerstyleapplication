package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;

public class LinearDetailsPDF extends LinearLayout {
    private TextView textViewNameSong;
    private TextView textViewNameCreator;
    private TextView textViewTempo;
    private TextView textViewTrebleClef;
    private LinearLayout linearBackgroundTempo;
    private LinearLayout linearBackgroundTrebleClef;
//    private LinearLayout linearBackgroundTempo;
    private AppCompatImageView imageViewTempo;
    private AppCompatImageView imageViewTrebleClef;

    private float density;
    public LinearDetailsPDF(Context context) {
        super(context);
        setOrientation(VERTICAL);
        init();
    }

    private void init() {
        textViewNameSong = new TextView(getContext());
        textViewNameCreator = new TextView(getContext());
        textViewTempo = new TextView(getContext());
        textViewTrebleClef = new TextView(getContext());
        linearBackgroundTempo = new LinearLayout(getContext());
        linearBackgroundTrebleClef = new LinearLayout(getContext());
        imageViewTempo = new AppCompatImageView(getContext());
        imageViewTrebleClef = new AppCompatImageView(getContext());

//        linearBackgroundDetails.setOrientation(HORIZONTAL);

        LinearLayout.LayoutParams paramsCenterHorizontal = paramsCenterHorizontal();
        LinearLayout.LayoutParams paramsImageTempo = paramsImage();

        textViewNameSong.setLayoutParams(paramsCenterHorizontal);
        textViewNameCreator.setLayoutParams(paramsCenterHorizontal);
        imageViewTempo.setLayoutParams(paramsImageTempo);
        imageViewTrebleClef.setLayoutParams(paramsImageTempo);

        textViewNameSong.setTextSize(18);
        textViewNameSong.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
        textViewNameSong.setTypeface(Typeface.DEFAULT_BOLD);
        textViewNameCreator.setTextSize(14);
        textViewNameCreator.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
        textViewTempo.setTextSize(14);
        textViewTempo.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
        textViewTrebleClef.setTextSize(14);
        textViewTrebleClef.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
        imageViewTempo.setImageResource(R.mipmap.symbol_tempo);
        imageViewTrebleClef.setImageResource(R.mipmap.treble_clef_18);

        linearBackgroundTempo.addView(imageViewTempo);
        linearBackgroundTempo.addView(textViewTempo);
        linearBackgroundTrebleClef.addView(imageViewTrebleClef);
        linearBackgroundTrebleClef.addView(textViewTrebleClef);

        addView(textViewNameSong);
        addView(textViewNameCreator);
        addView(linearBackgroundTempo);
        addView(linearBackgroundTrebleClef);
    }

    private LinearLayout.LayoutParams paramsCenterHorizontal() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        return params;
    }
    private LinearLayout.LayoutParams paramsImage() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dpToPx(15),dpToPx(15));
//        params.gravity = Gravity.CENTER_HORIZONTAL;
        return params;
    }
    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    public TextView getTextViewNameSong() {
        return textViewNameSong;
    }

    public void setTextViewNameSong(TextView textViewNameSong) {
        this.textViewNameSong = textViewNameSong;
    }
    public TextView getTextViewNameCreator() {
        return textViewNameCreator;
    }

    public void setTextViewNameCreator(TextView textViewNameCreator) {
        this.textViewNameCreator = textViewNameCreator;
    }

    public TextView getTextViewTempo() {
        return textViewTempo;
    }

    public void setTextViewTempo(TextView textViewTempo) {
        this.textViewTempo = textViewTempo;
    }

    public TextView getTextViewTrebleClef() {
        return textViewTrebleClef;
    }

    public void setTextViewTrebleClef(TextView textViewTrebleClef) {
        this.textViewTrebleClef = textViewTrebleClef;
    }

}
