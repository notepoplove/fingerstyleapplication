package com.example.suphotkhotphoothon.fingerstyleapplication4.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.ConvertPDFSave;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.ConvertPDFSend;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.LinearDetailsPDF;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class PDFActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {
    private EditText editTextEmail;
    private RelativeLayout relativeBackground;
    private RelativeLayout relativeNumberPage;
    private LinearLayout linearBackgroundImage;
    public LinearLayout linearBackground;
    private AppCompatImageView imageSave;
    private AppCompatImageView imageSendMail;

    private String fileID;
    private String filePath;
    public String nameSong;
    private String nameCreator;
    private String trebleClef;
    private float mScale = 1.0f;
    private float density;
    private int defaultTime = 60000;
    private int bpm;
    private int numberRoom = 3;
    private int signature;
    private int distanceTempo = 0;
    private float distanceRoom = 0;
    private int distanceRoomPX;
    private int distanceSecond;
    private int distanceHeight;
    private float distance;
    private int longtime;
    private int widthImage;
    private int heightImage;
    private int heightImageDetails;
    private int heightImageTabOne;
    private int numberPage = 0;
    private int numberListTab;
    private int numberListTabOne;

    private SQLiteHelper sqLiteHelper;
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;
    private LinearDetailsPDF linearDetailsPDF;
    private TabActivity tabActivity;
    private PDFActivity pdfActivity;
    private Context context;
    private AlertDialog.Builder builder;
    private LayoutInflater inflater;

    private ArrayList<Note> notes;
    private ArrayList<LinearLayout> pdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        pdfActivity = PDFActivity.this;
        tabActivity = new TabActivity();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                fileID = null;
            } else {
                fileID = extras.getString("file_id");
            }
        } else {
            fileID = (String) savedInstanceState.getSerializable("file_id");
        }
        init();
        if(fileID != null){
            getDataTab();
        }
    }

    private void init() {

        relativeBackground = (RelativeLayout) findViewById(R.id.activityPDF_rl_background);
        linearBackground = (LinearLayout)findViewById(R.id.activityPDF_ll_background);
        imageSave = (AppCompatImageView)findViewById(R.id.activityPDF_image_save);
        imageSendMail = (AppCompatImageView)findViewById(R.id.activityPDF_image_sendMail);

        builder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();

        sqLiteHelper = new SQLiteHelper(this);
        scaleGestureDetector = new ScaleGestureDetector(this,scaleGestureListener);
        gestureDetector = new GestureDetector(this,gestureListener);
        notes = new ArrayList<Note>();
        pdf = new ArrayList<LinearLayout>();

        View view = inflater.inflate(R.layout.custom_from_send_email, null);
        editTextEmail = (EditText)view.findViewById(R.id.from_email);
        builder.setView(view);

        distanceSecond = 150;
        distanceHeight = 30;

        imageSave.setOnClickListener(this);
        imageSendMail.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == imageSave){
            Log.w("Number Page",""+pdf.size());
            new ConvertPDFSave(this,pdf,nameSong);
        }
        else if(v == imageSendMail){
//            builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    new ConvertPDFSend(pdfActivity,pdf,nameSong,editTextEmail.getText().toString());
//                }
//            });
//            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                }
//            });
//            try {
//                builder.show();
//            }catch (Exception ex){
//                ex.printStackTrace();
//            }
            new ConvertPDFSend(this,pdf,nameSong,editTextEmail.getText().toString());
        }
    }


    private ScaleGestureDetector.SimpleOnScaleGestureListener scaleGestureListener = new ScaleGestureDetector.SimpleOnScaleGestureListener(){
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
//            Log.i("detector:",""+detector.getScaleFactor());
            mScale *= detector.getScaleFactor();
//            Log.i("X:",""+imageViewNote.getX());
            linearBackgroundImage.setScaleX(mScale);
            linearBackgroundImage.setScaleY(mScale);
            return true;
        }

    };
    private GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener(){
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            return true;
        }
    };

    @Override
    public boolean onTouch(View v, MotionEvent event) {
//        Log.i("Check","X:"+event.getX()+" Y:"+event.getY());
        scaleGestureDetector.onTouchEvent(event);
        gestureDetector.onTouchEvent(event);
        return true;
    }

    private void getDataTab() {
        Cursor cursor = getDatabase();
        cursor.moveToNext();
        Log.w("bpm",""+cursor.getString(3));
        Log.w("sig",""+cursor.getString(4));
        Log.w("long time",""+cursor.getString(5));
        Log.w("path",""+cursor.getString(7));
        nameSong = cursor.getString(1);
        nameCreator = cursor.getString(2);
        bpm = Integer.parseInt(cursor.getString(3));
        trebleClef = cursor.getString(4);
        String splitSignature[] =  trebleClef.split("/");
        signature = Integer.parseInt(splitSignature[0]);
        String millisecondLongtime = cursor.getString(5);
        filePath = cursor.getString(7);
        longtime = setSecond(Integer.parseInt(millisecondLongtime)); //Error longtime = "" (/)

        distanceTempo = setMillisecondTempo(bpm);
        Log.w("Distance Tempo","Time:"+distanceTempo);
        distanceRoom = distanceTempo*signature;
        Log.w("Distance Room Default","Time:"+distanceRoom);

        linearBackgroundImage = new LinearLayout(this);
        linearBackgroundImage.setBackgroundResource(R.color.colorWhite);
        linearBackgroundImage.setOrientation(LinearLayout.VERTICAL);
        pdf.add(linearBackgroundImage);
        linearBackground.addView(linearBackgroundImage,paramsBackgroundImage());
        createDetails();
        try {

            FileInputStream fis = new FileInputStream (new File(filePath));
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fis);
            Element element = doc.getDocumentElement();
            element.normalize();

            NodeList nList = doc.getElementsByTagName("Note");

            for (int i=0; i<nList.getLength(); i++) {

                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element2 = (Element) node;
                    notes.add(new Note(this,
                            Integer.parseInt(getValue("String", element2)),
                            Integer.parseInt(getValue("Fret", element2)),
                            Integer.parseInt(getValue("Time", element2)),
                            Boolean.parseBoolean(getValue("Room", element2))));
                }
            }
            fis.close();
        } catch (Exception e) {e.printStackTrace(); }
        getHeightLinearDetailsPDF();

    }

    private void createDetails() {
        linearDetailsPDF = new LinearDetailsPDF(this);
        linearDetailsPDF.getTextViewNameSong().setText(nameSong);
        linearDetailsPDF.getTextViewNameCreator().setText(nameCreator);
        linearDetailsPDF.getTextViewTempo().setText("= "+bpm);
        linearDetailsPDF.getTextViewTrebleClef().setText(trebleClef);
//        linearDetailsPDF.setBackgroundResource(R.color.colorRed);
        linearBackgroundImage.addView(linearDetailsPDF,0);
    }

    private void getHeightLinearDetailsPDF() {
        ViewTreeObserver viewTreeObserver = linearDetailsPDF.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                linearDetailsPDF.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int width  = linearDetailsPDF.getMeasuredWidth();
                int height = linearDetailsPDF.getMeasuredHeight();
//                Log.w("LinearDetailsPDF","Height:"+height);
                heightImageDetails = height;
                widthImage = linearBackgroundImage.getWidth();
                heightImage = (int)(widthImage *1.415f);
                heightImageTabOne = heightImage - height;

                distanceRoomPX = (int) Math.floor(widthImage /numberRoom);
                numberListTab = (int) Math.floor(heightImage /(distanceHeight*7));
                numberListTabOne = (int) Math.floor(heightImageTabOne /(distanceHeight*7));
                distance = distanceRoomPX/distanceRoom;

                Log.w("Number List Tab:",""+numberListTab);
                Log.w("Number List Tab One:",""+numberListTabOne);
                Log.w("distance:",distanceRoomPX+"/"+distanceRoom+" = "+distance);


                createTab();
            }
        });

    }
    LinearLayout linearBackgroundListTab;
    LinearLayout linearBackgroundSting;
    RelativeLayout relativeBackgroundSting[];
    ImageView imageLineString;
    ImageView imageLinePage;
    ImageView imageLineRoomLeft;
    ImageView imageLineRoomRight;
    TextView textNumberPage;
    private void createTab() {

        int beforeTimeRoom = 0;
        int countRoom = 0;
        int countListTab = 0;
        boolean checkNewRoom = true;
        int numberPageList = numberListTabOne;
        relativeBackgroundSting = new RelativeLayout[6];
        for (int i=1;i<notes.size();i++){
            if(countListTab>=numberPageList){
                linearBackgroundImage = new LinearLayout(this);
                relativeNumberPage = new RelativeLayout(this);
                imageLinePage = new ImageView(this);
                textNumberPage = new TextView(this);

                RelativeLayout.LayoutParams paramsLinePage = paramsRelativePage();
                RelativeLayout.LayoutParams paramsRelativeCenter = paramsRelativeCenter();

                linearBackgroundImage.setBackgroundResource(R.color.colorWhite);
                relativeNumberPage.setBackgroundResource(R.color.colorWhite);
                imageLinePage.setBackgroundResource(R.color.colorBlueStar);
                linearBackgroundImage.setOrientation(LinearLayout.VERTICAL);
                textNumberPage.setText(""+(++numberPage));
                textNumberPage.setPadding(0,0,0,0);


                relativeNumberPage.addView(textNumberPage,paramsRelativeCenter);
                relativeNumberPage.addView(imageLinePage,paramsLinePage);
                linearBackground.addView(relativeNumberPage, paramsRelativePageNumber());
                pdf.add(linearBackgroundImage);
                linearBackground.addView(linearBackgroundImage,paramsBackgroundImage());

                numberPageList = numberListTab;
                countListTab = 0;
            }
            if(countRoom%numberRoom == 0 && checkNewRoom == true){
                linearBackgroundListTab = new LinearLayout(this);
                LinearLayout.LayoutParams paramsBackground = paramsBackground();
                linearBackgroundListTab.setOrientation(LinearLayout.HORIZONTAL);
                linearBackgroundImage.addView(linearBackgroundListTab,paramsBackground);
                countListTab++;
            }

            if(checkNewRoom){
                linearBackgroundSting = new LinearLayout(this);
                linearBackgroundSting.setOrientation(LinearLayout.VERTICAL);

                LinearLayout.LayoutParams paramsBackgroundString = paramsBackgroundString();

                for(int j=0;j<relativeBackgroundSting.length;j++){
                    relativeBackgroundSting[j] = new RelativeLayout(this);

                    imageLineString = new ImageView(this);
                    imageLineRoomLeft = new ImageView(this);
                    imageLineRoomRight = new ImageView(this);

                    imageLineString.setBackgroundColor(this.getResources().getColor(R.color.colorGray));
                    imageLineRoomLeft.setBackgroundColor(this.getResources().getColor(R.color.colorGray));
                    imageLineRoomRight.setBackgroundColor(this.getResources().getColor(R.color.colorGray));

                    RelativeLayout.LayoutParams paramsString = paramsString();
                    RelativeLayout.LayoutParams paramsLineString = paramsRelativeString();
                    RelativeLayout.LayoutParams paramsLineRoomLeft = null;
                    RelativeLayout.LayoutParams paramsLineRoomRight = null;
                    if(countRoom%numberRoom == 0) {
                        paramsLineRoomLeft = paramsLineRoomLeft(2);
                        paramsLineRoomRight = paramsLineRoomRight(1);
                    }
                    if(countRoom%numberRoom == 1) {
                        paramsLineRoomLeft = paramsLineRoomLeft(1);
                        paramsLineRoomRight = paramsLineRoomRight(1);

                    }
                    if(countRoom%numberRoom == 2) {
                        paramsLineRoomLeft = paramsLineRoomLeft(1);
                        paramsLineRoomRight = paramsLineRoomRight(2);

                    }

                    relativeBackgroundSting[j].addView(imageLineString,paramsLineString);
                    if(j==0){
                        paramsLineRoomLeft.topMargin = distanceHeight/2+1;
                        paramsLineRoomRight.topMargin = distanceHeight/2+1;
                    }
                    else if(j==5){
                        paramsLineRoomLeft.bottomMargin = distanceHeight/2+1;
                        paramsLineRoomRight.bottomMargin = distanceHeight/2+1;
                    }

                    relativeBackgroundSting[j].addView(imageLineRoomLeft,paramsLineRoomLeft);
                    relativeBackgroundSting[j].addView(imageLineRoomRight,paramsLineRoomRight);

                    linearBackgroundSting.addView(relativeBackgroundSting[j],paramsString);
                }

                linearBackgroundListTab.addView(linearBackgroundSting,paramsBackgroundString);
                countRoom++;
                checkNewRoom = false;

            }

            if(notes.get(i).getRoom() == false){
//                Log.i("Note","S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
                Log.i("Sting:",""+notes.get(i).getString());
                Log.i("Fret:",""+notes.get(i).getFret());
                Log.i("Time:",""+notes.get(i).getTime());
                Log.i("Room:",""+notes.get(i).getRoom());
                TextView textNote = new TextView(this);
                RelativeLayout.LayoutParams paramsRelativeNote = paramsRelativeNote(Math.round((notes.get(i).getTime()-beforeTimeRoom)*distance));
                Resources res = getBaseContext().getResources();
                int rawId;
                rawId = res.getIdentifier("number_"+ notes.get(i).getFret()+"_black", "mipmap", getBaseContext().getPackageName());
                Log.i("Note", "number_" + notes.get(i).getFret()+"_black"+ " = " + rawId);
                Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), rawId);
                AppCompatImageView imageView = new AppCompatImageView(this);
                imageView.setImageBitmap(bitmap);
                relativeBackgroundSting[notes.get(i).getString()-1].addView(imageView,paramsRelativeNote);
            }
            if(notes.get(i).getRoom() == true){
                beforeTimeRoom = notes.get(i).getTime();
                Log.i("Line:","----------------------- T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
                checkNewRoom = true;
            }
        }
        relativeNumberPage = new RelativeLayout(this);
        imageLinePage = new ImageView(this);
        textNumberPage = new TextView(this);

        RelativeLayout.LayoutParams paramsLinePage = paramsRelativePage();
        RelativeLayout.LayoutParams paramsRelativeCenter = paramsRelativeCenter();

        relativeNumberPage.setBackgroundResource(R.color.colorWhite);
        imageLinePage.setBackgroundResource(R.color.colorBlueStar);
        textNumberPage.setText(""+(++numberPage));
//        textNumberPage.setTextSize(6);
        textNumberPage.setPadding(0,0,0,0);


        relativeNumberPage.addView(textNumberPage,paramsRelativeCenter);
        relativeNumberPage.addView(imageLinePage,paramsLinePage);
        linearBackground.addView(relativeNumberPage, paramsRelativePageNumber());
    }


    private String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }
    private int setSecond(int milliSeconds) {
        return (int) Math.ceil(milliSeconds/1000.f);
    }
    private int setMillisecondTempo(int bpm) {
        return Math.round(defaultTime/bpm);
    }
    public int millisecondToPx(int time) {
        return Math.round(time/(1000/distanceSecond));
    }

    private LinearLayout.LayoutParams paramsImageTab(int width, int height) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,height);
        return params;
    }
    private LinearLayout.LayoutParams paramsBackground() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.topMargin = distanceHeight;
        return params;
    }

    private LinearLayout.LayoutParams paramsBackgroundString() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(distanceRoomPX,ViewGroup.LayoutParams.WRAP_CONTENT);
        return params;
    }
    private LinearLayout.LayoutParams paramsBackgroundImage() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        return params;
    }
    private RelativeLayout.LayoutParams paramsString() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,distanceHeight);
        return params;
    }
    private RelativeLayout.LayoutParams paramsRelativeString() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,2);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        return params;
    }
    private RelativeLayout.LayoutParams paramsRelativeNote(int px) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = px-10;
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        return params;
    }
    private RelativeLayout.LayoutParams paramsRelativeCenter() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        return params;
    }
    private RelativeLayout.LayoutParams paramsRelativePage() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,4);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        return params;
    }
    private RelativeLayout.LayoutParams paramsRelativePageNumber() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        return params;
    }
    private RelativeLayout.LayoutParams paramsLineRoomLeft(int size) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(size,ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        return params;
    }
    private RelativeLayout.LayoutParams paramsLineRoomRight(int size) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(size,ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        return params;
    }

    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    private Cursor getDatabase() {
        String sql = "SELECT * FROM FileTabGuitar WHERE _id = "+fileID;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
}
