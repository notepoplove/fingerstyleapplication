package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;

public class CircleImageView extends de.hdodenhof.circleimageview.CircleImageView {
    String id = null;
    private float density;
    public CircleImageView(Context context) {
        super(context);
        setLayoutParams(paramsLinearBackGroundImageUser());
    }

    private LinearLayout.LayoutParams paramsLinearBackGroundImageUser() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dpToPx(30),dpToPx(30));
        layoutParams.rightMargin = dpToPx(5);
        return layoutParams;
    }

    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    public void setID(String id){
        this.id = id;
    }
    public String getID(){
        return this.id;
    }
}
