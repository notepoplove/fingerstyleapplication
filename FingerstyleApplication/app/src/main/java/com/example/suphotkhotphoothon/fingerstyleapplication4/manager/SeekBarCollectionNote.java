package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

public class SeekBarCollectionNote implements SeekBar.OnSeekBarChangeListener {
    TabActivity tabActivity;
    SeekBar seekBarFret;

    public SeekBarCollectionNote(TabActivity tabActivity, SeekBar seekBarFret) {
        this.tabActivity = tabActivity;
        this.seekBarFret = seekBarFret;

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        seekBar.setThumb(getThumb(progress));
//        note.setFret(progress);
        for(int i=0;i<tabActivity.collectionNote.size();i++) {
            tabActivity.collectionNote.get(i).setFret(progress);
            tabActivity.collectionNote.get(i).setImageBitmap(Bitmap.createScaledBitmap(tabActivity.getImageBitmap(progress),
                    tabActivity.collectionNote.get(i).getWidth(), tabActivity.collectionNote.get(i).getHeight(), false));
        }
        tabActivity.saveFileXML();
//        note.setFret(progress);
//        note.setImageBitmap(Bitmap.createScaledBitmap(tabActivity.getImageBitmap(progress), note.getWidth(), note.getHeight(), false));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public Drawable getThumb(int progress) {
        TextView textView = new TextView(tabActivity);
        textView.setText(""+progress);
        textView.setTextSize(16);
        textView.setTextColor(Color.parseColor("#2adbc9"));

        textView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(textView.getMeasuredWidth(), textView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        textView.layout(0, 0, textView.getMeasuredWidth(), textView.getMeasuredHeight());
        textView.draw(canvas);

        return new BitmapDrawable(tabActivity.getResources(), bitmap);
    }
    private void setFretSeekBar(int fret){
        this.seekBarFret.setProgress(fret);
        this.seekBarFret.setThumb(getThumb(fret));
    }
}
