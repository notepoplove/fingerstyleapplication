package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.google.gson.JsonObject;
import com.itextpdf.text.pdf.codec.Base64;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BackgroundListReply extends LinearLayout {
    String commentId = null;
    private float density;
    private CircleImageView circleImageViewUses;
    private LinearLayout linearBackgroundMessage;
    private LinearLayout linearBackgroundName;
    private RelativeLayout relativeBackgroundMessage;
    private TextView textViewName;
    private TextView textViewDate;
    private TextView editTextMessage;
    private SimpleDateFormat sdf;

    public BackgroundListReply(Context context, JsonObject jsonObject) {
        super(context);
        commentId = jsonObject.get("comment_id").getAsString();
        LayoutParams paramsLinearBackGroundComment = paramsLinearBackGroundComment();
        LayoutParams paramsLinearBackGroundMessage = paramsLinearBackGroundMessage();
        LayoutParams paramsLinearMarginLeft = paramsLinearMarginLeft();
        RelativeLayout.LayoutParams paramsRelativeBackReply = paramsRelativeBackReply();
        ViewGroup.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(paramsLinearBackGroundComment);

        setOrientation(LinearLayout.HORIZONTAL);
//        setBackgroundResource(R.color.colorYellow);

        circleImageViewUses = new CircleImageView(context);
        linearBackgroundMessage = new LinearLayout(context);
//        linearBackgroundMessage.setBackgroundResource(R.color.colorGreen);
        linearBackgroundName = new LinearLayout(context);

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        relativeBackgroundMessage = new RelativeLayout(context);
        textViewName = new TextView(context);
        textViewDate = new TextView(context);
        editTextMessage = new TextView(context);

        linearBackgroundMessage.setOrientation(LinearLayout.VERTICAL);
        linearBackgroundName.setOrientation(LinearLayout.HORIZONTAL);


        relativeBackgroundMessage.setBackgroundResource(R.drawable.background_message);
        textViewName.setTypeface(Typeface.DEFAULT_BOLD);
        textViewName.setTextColor(context.getResources().getColor(R.color.colorBlack));
        textViewName.setText(jsonObject.get("user_name").getAsString());
        textViewDate.setTypeface(Typeface.SERIF);
        String dateString = jsonObject.get("commentReply_date").getAsString();
        try {
            Date date = sdf.parse(dateString);
            SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy HH:mm");
            textViewDate.setText(formatter.format(date));
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
        }
        editTextMessage.setText(jsonObject.get("commentReply_message").getAsString());

//        circleImageViewUses.setImageResource(R.mipmap.note);
        byte[] bytes = Base64.decode(jsonObject.get("user_image").getAsString());
        Bitmap bitmapResult = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        Log.i("Bitmap Input:",""+bitmapResult);
        circleImageViewUses.setImageBitmap(bitmapResult);

        linearBackgroundName.addView(textViewName);
        linearBackgroundName.addView(textViewDate,paramsLinearMarginLeft);
        relativeBackgroundMessage.addView(editTextMessage);

        linearBackgroundMessage.addView(linearBackgroundName);
        linearBackgroundMessage.addView(relativeBackgroundMessage,params);

        addView(circleImageViewUses);
        addView(linearBackgroundMessage,paramsLinearBackGroundMessage);

    }

    private LayoutParams paramsLinearBackGroundComment() {
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.leftMargin = dpToPx(38);
        layoutParams.topMargin = dpToPx(3);
        return layoutParams;
    }
    private LayoutParams paramsLinearMarginLeft() {
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = dpToPx(8);
        return layoutParams;
    }
    private LayoutParams paramsLinearBackGroundMessage() {
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        return layoutParams;
    }
    private RelativeLayout.LayoutParams paramsRelativeBackReply() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        return layoutParams;
    }
    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    public void setCommentId(String commentId){
        this.commentId = commentId;
    }
    public String getCommentId(){
        return this.commentId;
    }
}
