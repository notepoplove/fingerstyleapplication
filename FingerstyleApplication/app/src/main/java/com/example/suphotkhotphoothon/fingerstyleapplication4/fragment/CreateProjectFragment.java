package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.TABLE_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_CREATOR;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_BPM;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_SIGNATURE;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_DATE;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_STATUS;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateProjectFragment extends Fragment implements View.OnClickListener{

    SQLiteHelper sqLiteHelper;

    private View view;
    private FragmentTransaction fragmentTran;
    private EditText editTextTitle;
    private EditText editTextBy;
    private EditText editTextBpm;
    private EditText editTextSignature1;
    private EditText editTextSignature2;
    private TextView textErrorTitle;
    private TextView textErrorCreator;
    private TextView textErrorBpm;
    private TextView textErrorSignature;
    private Button createProjectButtonOk;

    private SimpleDateFormat sdf;

    private static final Pattern VALID_NUMBER_REGEX = Pattern.compile("[^0-9]|^'|'$|''\"");
    public CreateProjectFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_create_project, container, false);

        init();

        return view;
    }

    private void init() {
        fragmentTran = getActivity().getSupportFragmentManager().beginTransaction();
        createProjectButtonOk = (Button)view.findViewById(R.id.fragmentCreateProject_bt_ok);
        editTextTitle = (EditText) view.findViewById(R.id.fragmentCreateProject_ed_title);
        editTextBy =  (EditText) view.findViewById(R.id.fragmentCreateProject_ed_by);
        editTextBpm = (EditText) view.findViewById(R.id.fragmentCreateProject_ed_bpm);
        editTextSignature1 = (EditText) view.findViewById(R.id.fragmentCreateProject_ed_signature1);
        editTextSignature2 = (EditText) view.findViewById(R.id.fragmentCreateProject_ed_signature2);
        textErrorTitle = (TextView)view.findViewById(R.id.fragmentCreateProject_textView_errorTitle);
        textErrorCreator = (TextView)view.findViewById(R.id.fragmentCreateProject_textView_errorCreator);
        textErrorBpm = (TextView)view.findViewById(R.id.fragmentCreateProject_textView_errorBpm);
        textErrorSignature = (TextView)view.findViewById(R.id.fragmentCreateProject_textView_errorSignature);


        sqLiteHelper = new SQLiteHelper(getContext());
//        Date currentTime = Calendar.getInstance().getTime();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        createProjectButtonOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v == createProjectButtonOk){
            String title = editTextTitle.getText().toString();
            String creator = editTextBy.getText().toString();
            String bpm = editTextBpm.getText().toString();
            String signature1 = editTextSignature1.getText().toString();
            String signature2 = editTextSignature2.getText().toString();
            String currentTime = sdf.format(new Date());

            Boolean checkCreateProject = true;
            textErrorTitle.setText("");
            textErrorCreator.setText("");
            textErrorBpm.setText("");
            textErrorSignature.setText("");

            if(title.equals("")){
                checkCreateProject = false;
                textErrorTitle.setText("*please enter a title.");
            }

            if(creator.equals("")){
                checkCreateProject = false;
                textErrorCreator.setText("*please enter a creator.");
            }

            if(bpm.equals("")){
                checkCreateProject = false;
                textErrorBpm.setText("*please enter a bpm.");
            }
            else {
                if(validateNumber(bpm)){
                    checkCreateProject = false;
                    textErrorBpm.setText("*please enter 0-9 only.");
                }
            }

            if(signature1.equals("") || signature2.equals("")){
                checkCreateProject = false;
                textErrorSignature.setText("*please enter a signature.");
            }
            else {
                if(validateNumber(signature1) || validateNumber(signature2)){
                    checkCreateProject = false;
                    textErrorSignature.setText("*please enter 0-9 only.");
                }
            }

            if(checkCreateProject) {
                SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
                ContentValues values = new ContentValues();
                values.put(COLUMN_NAME, title);
                values.put(COLUMN_CREATOR, creator);
                values.put(COLUMN_BPM, bpm);
                values.put(COLUMN_SIGNATURE, signature1 + "/" + signature2);
                values.put(COLUMN_DATE, currentTime);
                values.put(COLUMN_STATUS, "original");
                db.insertOrThrow(TABLE_NAME, null, values);

                Cursor cursor = getMAxID();
                cursor.moveToNext();
                Intent intent = new Intent(getActivity(), TabActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("file_id", cursor.getString(0)); //Optional parameters
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        }

    }
    private void setFragment(Fragment fragment) {
        fragmentTran.replace(R.id.project_frame, fragment);
        fragmentTran.addToBackStack(null);
        fragmentTran.commit();
    }

    public static boolean validateNumber(String numberString) {
        Matcher matcher = VALID_NUMBER_REGEX.matcher(numberString);
        return matcher.find();
    }
    private Cursor getMAxID() {
        String sql = "SELECT MAX(_id) FROM FileTabGuitar";
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
}

