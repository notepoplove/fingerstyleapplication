package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import java.util.ArrayList;
import java.util.Collections;

public class NoteListener implements View.OnClickListener,View.OnLongClickListener{
    TabActivity tabActivity;
    ArrayList<Note> collectionNote;
    ArrayList<ArrayList<Object>> collectionRoom;
    ArrayList<Note> notes;
    public Note note;
    SeekBar seekBarFret;
    SeekBarOnClickListener seekBarOnClickListener;

    SeekBarCollectionNote seekBarCollectionNote;
    LinearLayout linearBackgroundSetTime;
    RelativeLayout relativeLayoutBackgroundSeekBar;
    RelativeLayout relativeBackgroundShowTime;
    RelativeLayout relativeBackgroundDeleteTime;
    RelativeLayout relativeBackgroundPlusTime;
    TextView textViewShowTime;
    ImageView imageViewDelete;
    AppCompatImageView imageViewSetPlusTime;
    AppCompatImageView imageViewSetDeleteTime;
    AppCompatImageView imageViewStringNumber[];
    Note selectionNote[];

    public NoteListener(TabActivity tabActivity, Note note, SeekBar seekBarFret){
        this.tabActivity = tabActivity;
        this.relativeLayoutBackgroundSeekBar = tabActivity.relativeLayoutBackgroundSeekBar;
        this.notes = tabActivity.notes;
        this.collectionNote = tabActivity.collectionNote;
        this.collectionRoom = tabActivity.collectionRoom;
        this.note = note;
        this.selectionNote = tabActivity.selectionNote;
        this.seekBarFret = seekBarFret;
        this.imageViewStringNumber = tabActivity.imageViewStringNumber;
        this.relativeBackgroundShowTime = tabActivity.relativeBackgroundShowTime;
        this.relativeBackgroundDeleteTime = tabActivity.relativeBackgroundDeleteTime;
        this.relativeBackgroundPlusTime = tabActivity.relativeBackgroundPlusTime;
        this.textViewShowTime = tabActivity.textViewShowTime;
        this.imageViewSetDeleteTime = tabActivity.imageViewSetDeleteTime;
        this.imageViewSetPlusTime = tabActivity.imageViewSetPlusTime;
        this.imageViewDelete = tabActivity.imageViewDelete;

    }
    @Override
    public void onClick(View v) {

        if(v == note&&!tabActivity.checkSelectionNote){
            tabActivity.setDefaultColorRoom();

            Log.i("Note "," S:"+note.getString()+" F:"+note.getFret()+" T:"+note.getTime()+" R:"+note.getRoom());
            Log.i("Selection",""+tabActivity.getCheckSelectionNote());
            selectionNote[0] = note;
            tabActivity.setTextViewShowTime(note.getTime());
//            long UpdateTime = note.getTime();
//            long timeStamp = UpdateTime;
//            long Seconds = (int) (UpdateTime / 1000);
//            long Minutes = Seconds / 60;
//            Seconds = Seconds % 60;
//            int MilliSeconds = (int) (UpdateTime % 1000);

            seekBarOnClickListener = new SeekBarOnClickListener(tabActivity, note, seekBarFret);

            note.setBackgroundResource(R.color.colorGreen);
            imageViewStringNumber[note.getString()-1].setBackgroundResource(R.color.colorGreen);
//            textViewShowTime.setText("" +String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds) + ":" + String.format("%03d", MilliSeconds));

            seekBarFret.setOnSeekBarChangeListener(seekBarOnClickListener);
            //            textViewShowTime.setText("" +String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds) + ":" + String.format("%03d", MilliSeconds));

            RelativeLayout.LayoutParams relativeLayoutSeekBar = layoutParamsRelativeLayoutSeekBar(seekBarFret);

            relativeLayoutBackgroundSeekBar.removeAllViews();

            relativeLayoutBackgroundSeekBar.addView(seekBarFret, relativeLayoutSeekBar);

            for (int i=0;i<notes.size();i++){
                if(notes.get(i) != note) {
                    notes.get(i).setBackgroundResource(0);
                }
            }
            for(int i=0;i<imageViewStringNumber.length;i++){
                if(i!=note.getString()-1){
                    imageViewStringNumber[i].setBackgroundResource(0);
                }
            }

            imageViewDelete.setVisibility(View.VISIBLE);
        }
        else if(v == note&&tabActivity.checkSelectionNote){

            boolean checkEqualsNote = false;
            for(int i=0;i<collectionNote.size();i++) {
                checkEqualsNote = collectionNote.get(i).equals(note);
                if(checkEqualsNote){
                    break;
                }
            }
            if(checkEqualsNote){
                note.setBackgroundResource(0);
                collectionNote.remove(note);
            }
            else{
                note.setBackgroundResource(R.color.colorYellow);
                collectionNote.add(note);
            }
            if(collectionNote.size()>0){
                Collections.sort(collectionNote);
                tabActivity.setTextViewShowTime(collectionNote.get(0).getTime());
            }
            else {
                tabActivity.setCheckSelectionNote(false);
                imageViewDelete.setVisibility(View.INVISIBLE);
            }

//            long UpdateTime = collectionNote.get(0).getTime();
//            long timeStamp = UpdateTime;
//            long Seconds = (int) (UpdateTime / 1000);
//            long Minutes = Seconds / 60;
//            Seconds = Seconds % 60;
//            int MilliSeconds = (int) (UpdateTime % 1000);
//            textViewShowTime.setText("" +String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds) + ":" + String.format("%03d", MilliSeconds));

//            for(int i=0;i<collectionNote.size();i++) {
//                Log.i("Note"+i," S:"+note.getString()+" F:"+note.getFret()+" T:"+note.getTime()+" R:"+note.getRoom());
//            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(v == note){
            selectionNote[0] = null;
            collectionNote.clear();
            tabActivity.setDefaultColorRoom();
            tabActivity.setCheckSelectionNote(true);
            note.setBackgroundResource(R.color.colorYellow);
            collectionNote.add(note);
            Log.i("Note"," S:"+note.getString()+" F:"+note.getFret()+" T:"+note.getTime()+" R:"+note.getRoom());
            tabActivity.setTextViewShowTime(collectionNote.get(0).getTime());
//            long UpdateTime = collectionNote.get(0).getTime();
//            long timeStamp = UpdateTime;
//            long Seconds = (int) (UpdateTime / 1000);
//            long Minutes = Seconds / 60;
//            Seconds = Seconds % 60;
//            int MilliSeconds = (int) (UpdateTime % 1000);
//            textViewShowTime.setText("" +String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds) + ":" + String.format("%03d", MilliSeconds));

//            for(int i=0;i<collectionNote.size();i++) {
//                Log.i("Note"+i," S:"+note.getString()+" F:"+note.getFret()+" T:"+note.getTime()+" R:"+note.getRoom());
//            }

            for (int i=0;i<notes.size();i++){
                if(notes.get(i) != note) {
                    notes.get(i).setBackgroundResource(0);
                }
            }
            for(int i=0;i<imageViewStringNumber.length;i++){
                imageViewStringNumber[i].setBackgroundResource(0);
            }
            seekBarCollectionNote = new SeekBarCollectionNote(tabActivity, seekBarFret);
            seekBarFret.setOnSeekBarChangeListener(seekBarCollectionNote);
            RelativeLayout.LayoutParams relativeLayoutSeekBar = layoutParamsRelativeLayoutSeekBar(seekBarFret);
            relativeLayoutBackgroundSeekBar.removeAllViews();
            relativeLayoutBackgroundSeekBar.addView(seekBarFret, relativeLayoutSeekBar);
            imageViewDelete.setVisibility(View.VISIBLE);
        }
        return true;
    }

    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutSeekBar(SeekBar seekBar) {
        RelativeLayout.LayoutParams relativeSeekBar = new RelativeLayout.LayoutParams(seekBar.getWidth(),seekBar.getHeight());
        relativeSeekBar.width = ViewGroup.LayoutParams.MATCH_PARENT;
        relativeSeekBar.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        relativeSeekBar.addRule(RelativeLayout.CENTER_VERTICAL);
        return relativeSeekBar;
    }
}
