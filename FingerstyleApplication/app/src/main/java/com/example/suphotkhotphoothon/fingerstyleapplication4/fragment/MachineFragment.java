package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.PDFActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.ProjectActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.BackgroundListTab;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.CheckBoxItems;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.ImageFileTab;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;


//import org.json.XML;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_CREATOR;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.TABLE_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_PATH;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.COLUMN_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.TABLE_USER_NAME;

/**
 * A simple {@link Fragment} subclass.
 */
public class MachineFragment extends Fragment implements View.OnClickListener,View.OnLongClickListener,View.OnKeyListener, KeyEvent.Callback
    , TextWatcher {
    private View view;
    private TextView textViewItems;
    private TextView textViewFile;
    private EditText editTextSearch;
    private LinearLayout linearBackGroundTab;
    private BackgroundListTab[] linearBackGroundListTab;
    private CheckBoxItems[] checkBoxItems;
    private CheckBoxItems checkBoxAllItems;
    private ImageFileTab[] imageFileTab;
    private RelativeLayout relativeBackGroundItems;
    private RelativeLayout relativeHeadBackgroundPlay;
    private RelativeLayout relativeHeadBackgroundName;
    private RelativeLayout relativeHeadBackgroundCreator;
    private RelativeLayout relativeHeadBackgroundDate;
    private RelativeLayout[] relativeBackgroundPlay;
    private RelativeLayout relativeLineTab;
    private LinearLayout.LayoutParams layoutParamsListTab;
    private LinearLayout.LayoutParams layoutParamsItemsTab;
    private RelativeLayout.LayoutParams layoutLineTab;
    private RelativeLayout.LayoutParams layoutItemsTab;
    private RelativeLayout.LayoutParams layoutTextPlay;
    private CircleImageView imageCreateProject;
    private CircleImageView imageShare;
    private CircleImageView imageDelete;
    private CircleImageView imagePDF;
    private AppCompatImageView imageSearch;

    private JsonObject jsonObjectShare;
    private FragmentManager fragmentManager;
    private SimpleDateFormat sdf;

    private SQLiteHelper sqLiteHelper;
    private Cursor cursor;
    private float density;

    private String userID = null;
    private String choiceHeadFile;
    private String sortingName = null;
    private String sortingCreator = null;
    private String sortingDate = null;
    private boolean statusCheckBox;

    public MachineFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_machine, container, false);

        init();
        cursor = getUserID();
        cursor.moveToNext();
        Log.w("cursor",""+cursor.getCount());
        if (cursor.getCount() > 0) {
            userID = cursor.getString(0);
            Log.w("UserID",""+userID);
        }
        else {
            userID = null;
            Log.w("UserID",""+userID);
        }

        createTextPlay();
        obscureImageButton();

        Cursor cursorSearch = getSearchString(editTextSearch.getText().toString());
        setDefaultSize(cursorSearch.getCount());
        choiceHeadFile = "file";
        showTabGuitar(cursorSearch);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        editTextSearch.setText("");
        onBackPressed();
    }

    private void obscureImageButton() {
        imageDelete.setVisibility(View.INVISIBLE);
        imageShare.setVisibility(View.INVISIBLE);
        imagePDF.setVisibility(View.INVISIBLE);
    }
    private void showImageButton() {
        imageDelete.setVisibility(View.VISIBLE);
        imageShare.setVisibility(View.VISIBLE);
        imagePDF.setVisibility(View.VISIBLE);
    }

    private void init() {
        editTextSearch = (EditText) view.findViewById(R.id.fragmentMachine_editText_search);
        imageSearch = (AppCompatImageView) view.findViewById(R.id.fragmentMachine_image_search);
        imageCreateProject = (CircleImageView)view.findViewById(R.id.fragmentMachine_image_createProject);
        imageShare = (CircleImageView)view.findViewById(R.id.fragmentMachine_image_share);
        imageDelete = (CircleImageView)view.findViewById(R.id.fragmentMachine_image_delete);
        imagePDF = (CircleImageView)view.findViewById(R.id.fragmentMachine_image_pdf);
        relativeHeadBackgroundPlay = (RelativeLayout)view.findViewById(R.id.fragmentMachine_rl_headBackgroundPlay);
        relativeHeadBackgroundName = (RelativeLayout)view.findViewById(R.id.fragmentMachine_rl_headBackgroundName);
        relativeHeadBackgroundCreator = (RelativeLayout)view.findViewById(R.id.fragmentMachine_rl_headBackgroundCreator);
        relativeHeadBackgroundDate = (RelativeLayout)view.findViewById(R.id.fragmentMachine_rl_headBackgroundDate);
        linearBackGroundTab = (LinearLayout)view.findViewById(R.id.fragmentCreateProject_ll_backGroundTab);
        fragmentManager = getActivity().getSupportFragmentManager();

        statusCheckBox = false;

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        textViewFile = new TextView(getContext());
        checkBoxAllItems = new CheckBoxItems(getContext());
        sqLiteHelper = new SQLiteHelper(getContext());

        textViewFile.setText(this.getResources().getText(R.string.text_file));

        view.setOnClickListener(this);
        imageCreateProject.setOnClickListener(this);
        imageShare.setOnClickListener(this);
        imageDelete.setOnClickListener(this);
        checkBoxAllItems.setOnClickListener(this);
        relativeHeadBackgroundName.setOnClickListener(this);
        relativeHeadBackgroundCreator.setOnClickListener(this);
        relativeHeadBackgroundDate.setOnClickListener(this);
        imagePDF.setOnClickListener(this);

        editTextSearch.addTextChangedListener(this);
    }

    private void setDefaultSize(int sizeDatabase) {
        Log.w("MaxRow", sizeDatabase +"");
        linearBackGroundListTab = new BackgroundListTab[sizeDatabase];
        relativeBackgroundPlay =new RelativeLayout[sizeDatabase];
        imageFileTab = new ImageFileTab[sizeDatabase];
        checkBoxItems = new CheckBoxItems[sizeDatabase];
    }

    @Override
    public void onClick(View v) {

        if(v == imageCreateProject){
            for(int i=0;i<linearBackGroundListTab.length;i++){
                linearBackGroundListTab[i].setBackgroundColor(this.getResources().getColor(R.color.colorWhiteTransparent));
            }
            Intent intent = new Intent(getActivity(), ProjectActivity.class);
            intent.putExtra("key", "ds"); //Optional parameters
            getActivity().startActivity(intent);
        }
        else if(v == imageShare){
            messageDialogShare();
        }
        else if(v == imageDelete){
            messageDialogDelete();

        }
        else if(v == checkBoxAllItems){
            if(checkBoxAllItems.isChecked()){
                for(int i=0;i<checkBoxItems.length;i++){
                    checkBoxItems[i].setChecked(true);
                }
            }
            else {
                for(int i=0;i<checkBoxItems.length;i++){
                    checkBoxItems[i].setChecked(false);
                }
            }
        }
        else if(v == relativeHeadBackgroundName){
            Log.w("HEAD","name");
            obscureImageButton();
            linearBackGroundTab.removeAllViews();
            sortingName = setChoiceSorting(sortingName);
            cursor = getSortName();
            setDefaultSize(cursor.getCount());

            checkBoxAllItems.setChecked(false);
            if(choiceHeadFile.equals("file")) {
                createTextPlay();
            }

            showTabGuitar(cursor);
        }
        else if(v == relativeHeadBackgroundCreator){
            Log.w("HEAD","creator");
            obscureImageButton();
            linearBackGroundTab.removeAllViews();
            sortingCreator = setChoiceSorting(sortingCreator);
            cursor = getSortCreator();
            setDefaultSize(cursor.getCount());

            checkBoxAllItems.setChecked(false);
            if(choiceHeadFile.equals("file")) {
                createTextPlay();
            }

            showTabGuitar(cursor);
        }
        else if(v == relativeHeadBackgroundDate){
            Log.w("HEAD","date");
            obscureImageButton();
            linearBackGroundTab.removeAllViews();
            sortingDate = setChoiceSorting(sortingDate);
            cursor = getSortDate();
            setDefaultSize(cursor.getCount());
            checkBoxAllItems.setChecked(false);
            if(choiceHeadFile.equals("file")) {
                createTextPlay();
            }
            showTabGuitar(cursor);
        }
        else if(v == imagePDF){
            int countItems =0;
            String fileId = "";
            for (int i = 0; i < linearBackGroundListTab.length; i++) {
                if(checkBoxItems[i].isChecked()){
                    Log.i("File ID",""+linearBackGroundListTab[i].getFileId());
                    fileId = linearBackGroundListTab[i].getFileId();
                    countItems++;
                }
            }
            if(countItems==1 && fileId != ""){
                Intent intent = new Intent(getActivity(), PDFActivity.class);
                intent.putExtra("file_id", fileId); //Optional parameters
                getActivity().startActivity(intent);
//                getActivity().finish();
            }
            else {


            }
        }
        else{
            if(statusCheckBox){
                for(int i=0;i<linearBackGroundListTab.length;i++){
                    if(v == linearBackGroundListTab[i]){
                        Log.w("STATUS CHECKBOX",""+checkBoxItems[i].isChecked());
                        checkBoxItems[i].setChecked(!checkBoxItems[i].isChecked());
                        linearBackGroundListTab[i].setBackgroundColor(this.getResources().getColor(R.color.colorClick));
                    }
                    else{
                        linearBackGroundListTab[i].setBackgroundColor(this.getResources().getColor(R.color.colorWhiteTransparent));
                    }
                }
            }
            else {
                for (int i = 0; i < linearBackGroundListTab.length; i++) {
                    if (v == linearBackGroundListTab[i]) {
                        Log.w("FILEID", linearBackGroundListTab[i].getFileId());
                        linearBackGroundListTab[i].setBackgroundColor(this.getResources().getColor(R.color.colorClick));

                        Intent intent = new Intent(getActivity(), TabActivity.class);
                        intent.putExtra("file_id", linearBackGroundListTab[i].getFileId()); //Optional parameters
                        getActivity().startActivity(intent);
                    } else {
                        linearBackGroundListTab[i].setBackgroundColor(this.getResources().getColor(R.color.colorWhiteTransparent));
                    }
                }
            }
        }
        closeKeyboard();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        return true;
    }

    @Override
    public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
        return false;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(editTextSearch.getText().length()>0){
           imageSearch.setVisibility(View.INVISIBLE);
        }
        else {
            imageSearch.setVisibility(View.VISIBLE);
        }
        Cursor cursorSearch = getSearchString(editTextSearch.getText().toString());
        linearBackGroundTab.removeAllViews();
        setDefaultSize(cursorSearch.getCount());
        createTextPlay();
//        choiceHeadFile = "play";
        showTabGuitar(cursorSearch);
    }

    @Override
    public boolean onLongClick(View v) {
        showImageButton();
        choiceHeadFile = "checkBox";
        relativeHeadBackgroundPlay.removeAllViews();
        relativeHeadBackgroundPlay.addView(checkBoxAllItems);
        for(int i=0;i<linearBackGroundListTab.length;i++){
            relativeBackgroundPlay[i].removeAllViews();
            relativeBackgroundPlay[i].addView(checkBoxItems[i]);
            if(v == linearBackGroundListTab[i]){
                statusCheckBox = true;
                Log.w("FILEID",linearBackGroundListTab[i].getFileId());
                linearBackGroundListTab[i].setBackgroundColor(this.getResources().getColor(R.color.colorClick));
                checkBoxItems[i].setChecked(true);
            }
            else{
                linearBackGroundListTab[i].setBackgroundColor(this.getResources().getColor(R.color.colorWhiteTransparent));
            }
        }
        return true;
    }

    private String setChoiceSorting(String sortingName) {
        if (sortingName == null||sortingName=="DESC") {
            sortingName = "ASC";
        }
        else if(sortingName=="ASC") {
            sortingName = "DESC";
        }
        return sortingName;
    }

    private void messageDialogShare() {
        String message = "";
        for(int i=0;i<checkBoxItems.length;i++){
            if(checkBoxItems[i].isChecked()){
                message = "Share";
                break;
            }

        }
        if(message == ""){
            message = "Please select a file.";
        }
        new AlertDialog.Builder(getActivity())
                .setIcon(R.mipmap.baseline_share_black_18dp)
                .setTitle("Share")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for (int i = 0; i < linearBackGroundListTab.length; i++) {
                            if(checkBoxItems[i].isChecked()){
                                Log.i("File ID",""+linearBackGroundListTab[i].getFileId());
                                sendFileXML(linearBackGroundListTab[i].getFileId());
                            }
                        }
//                        Log.w("Ok",""+clickFileID);
//                        sendFileXML(clickFileID);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Cancel","Cancel");
                    }
                })
                .show();
    }

    private void messageDialogDelete() {

        new AlertDialog.Builder(getActivity())
                .setIcon(R.mipmap.baseline_delete_black_18dp)
                .setTitle("Delete")
                .setMessage("Are you sure you want to delete?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for(int i=0;i<checkBoxItems.length;i++){
                            if(checkBoxItems[i].isChecked() == true){
                                Log.w("DELETE ID",checkBoxItems[i].getFileId());
                                setDelete(checkBoxItems[i].getFileId());
                            }
                        }
                        linearBackGroundTab.removeAllViews();
                        Cursor cursorSearch = getSearchString(editTextSearch.getText().toString());
                        setDefaultSize(cursorSearch.getCount());
                        if(checkBoxItems.length == 0){
                            createTextPlay();
                            choiceHeadFile = "file";
                            showTabGuitar(cursorSearch);
                        }else{
                            choiceHeadFile = "checkBox";
                            showTabGuitar(cursorSearch);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Cancel","Cancel");
                    }
                })
                .show();
    }

    private void sendFileXML(String clickFileID) {
        Cursor cursorPathFileXML = getFileById(clickFileID);
        cursorPathFileXML.moveToNext();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(new Date());
        if(cursorPathFileXML.getString(7)!=null) {
            StringBuilder stringXML = readFileXml(cursorPathFileXML.getString(7));
            jsonObjectShare = new JsonObject();
            jsonObjectShare.addProperty("file_xml",stringXML.toString());
            jsonObjectShare.addProperty("user_id",userID);
            jsonObjectShare.addProperty("fileServer_bpm",cursorPathFileXML.getString(3));
            jsonObjectShare.addProperty("fileServer_signature",cursorPathFileXML.getString(4));
            jsonObjectShare.addProperty("fileServer_name",cursorPathFileXML.getString(1));
            jsonObjectShare.addProperty("fileServer_longtime",cursorPathFileXML.getString(5));
            jsonObjectShare.addProperty("fileServer_date",currentTime);
            jsonObjectShare.addProperty("fileServer_songStatus",cursorPathFileXML.getString(8));
            jsonObjectShare.addProperty("fileServer_fromSongId",cursorPathFileXML.getString(9));
            jsonObjectShare.addProperty("fileServer_fromUserId",cursorPathFileXML.getString(10));
            Log.i("XML",""+stringXML.toString());
            Ion.with(this)
                    .load("POST","http://guitar.msuproject.net/public/manageFile.php/uploadFile")
                    .setJsonObjectBody(jsonObjectShare)
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, Response<String> result) {
                            String message = result.getResult().toString();
                            Log.w("Response",""+message);
                            if(message.equals("Success")){
                                Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getActivity(), result.getResult().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    private StringBuilder readFileXml(String path) {
        File file = new File(path);
        StringBuilder stringXML = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                stringXML.append(line);
                stringXML.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }
        return stringXML;
    }

    private void showTabGuitar(Cursor cursor) {
        int n =0;
        while (cursor.moveToNext()){
//            Log.w("ID",cursor.getString(0));
            linearBackGroundListTab[n] = new BackgroundListTab(getContext());
            relativeBackgroundPlay[n] = new RelativeLayout(getContext());
            imageFileTab[n] = new ImageFileTab(getContext());
            checkBoxItems[n] = new CheckBoxItems(getContext());

            linearBackGroundListTab[n].setOnClickListener(this);
            linearBackGroundListTab[n].setOnLongClickListener(this);
            imageFileTab[n].setOnClickListener(this);
            checkBoxItems[n].setOnClickListener(this);
            linearBackGroundListTab[n].setFileId(cursor.getString(0));
            imageFileTab[n].setFileId(cursor.getString(0));
            checkBoxItems[n].setFileId(cursor.getString(0));

            relativeLineTab = new RelativeLayout(getContext());

            linearBackGroundListTab[n].setOrientation(LinearLayout.HORIZONTAL);
            relativeLineTab.setBackgroundColor(this.getResources().getColor(R.color.colorWhite));


            layoutParamsListTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,dpToPx(35));
            layoutLineTab = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,dpToPx(2));
//            layoutParamsListTab.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

            for(int i=0;i<4;i++){
                relativeBackGroundItems = new RelativeLayout(getContext());
                textViewItems = new TextView(getContext());
                layoutItemsTab = new RelativeLayout.LayoutParams(textViewItems.getWidth(),textViewItems.getHeight());
                layoutItemsTab.width=ViewGroup.LayoutParams.WRAP_CONTENT;
                layoutItemsTab.height=ViewGroup.LayoutParams.WRAP_CONTENT;
                layoutItemsTab.addRule(RelativeLayout.CENTER_VERTICAL);
                if(i==0){
//                    relativeBackGroundItems.setBackgroundColor(this.getResources().getColor(R.color.colorRed));
                    layoutParamsItemsTab = new LinearLayout.LayoutParams(dpToPx(40),ViewGroup.LayoutParams.MATCH_PARENT);
                    layoutItemsTab.addRule(RelativeLayout.CENTER_IN_PARENT);
                    imageFileTab[n].setImageResource(R.mipmap.baseline_queue_music_white_48dp);
                    imageFileTab[n].setColorFilter(this.getResources().getColor(R.color.colorBlack));
//                    imageFileTab[n].setBackgroundColor(this.getResources().getColor(R.color.colorWhiteTransparent));
                    if(choiceHeadFile.equals("file")){
                        relativeBackgroundPlay[n].addView(imageFileTab[n],layoutItemsTab);
                    }
                    else if(choiceHeadFile.equals("checkBox")){
                        layoutItemsTab.addRule(RelativeLayout.CENTER_IN_PARENT);
                        relativeBackgroundPlay[n].addView(checkBoxItems[n]);
                    }
                    linearBackGroundListTab[n].addView(relativeBackgroundPlay[n],layoutParamsItemsTab);
                }
                else if(i==1) {
//                    relativeBackGroundItems.setBackgroundColor(this.getResources().getColor(R.color.colorWhite));
                    layoutParamsItemsTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT,1);
                    textViewItems.setText(cursor.getString(1));
                    layoutItemsTab.leftMargin = dpToPx(7);

                    relativeBackGroundItems.addView(textViewItems,layoutItemsTab);
                    linearBackGroundListTab[n].addView(relativeBackGroundItems,layoutParamsItemsTab);
                }
                else if(i==2){
//                    relativeBackGroundItems.setBackgroundColor(this.getResources().getColor(R.color.colorGreen));
                    layoutParamsItemsTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT,1);
                    textViewItems.setText(cursor.getString(2));
                    layoutItemsTab.leftMargin = dpToPx(7);
                    relativeBackGroundItems.addView(textViewItems,layoutItemsTab);
                    linearBackGroundListTab[n].addView(relativeBackGroundItems,layoutParamsItemsTab);
                }
                else if(i==3){
//                    relativeBackGroundItems.setBackgroundColor(this.getResources().getColor(R.color.colorWhite));
                    layoutParamsItemsTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT,1);
                    String dateString = cursor.getString(6).toString();
                    try {
                        Date date = sdf.parse(dateString);
                        SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy HH:mm");
                        textViewItems.setText(formatter.format(date));
                    } catch (ParseException ex) {
                        Log.v("Exception", ex.getLocalizedMessage());
                    }

                    layoutItemsTab.leftMargin = dpToPx(7);
                    relativeBackGroundItems.addView(textViewItems,layoutItemsTab);
                    linearBackGroundListTab[n].addView(relativeBackGroundItems,layoutParamsItemsTab);
                }
            }

            linearBackGroundTab.addView(linearBackGroundListTab[n],layoutParamsListTab);
            linearBackGroundTab.addView(relativeLineTab,layoutLineTab);
            n++;
        }
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(getActivity().getCurrentFocus() != null) {
            inputManager.hideSoftInputFromWindow(this.getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void onBackPressed()
    {
        checkBoxAllItems.setChecked(false);
        createTextPlay();
        obscureImageButton();
        if(statusCheckBox){
            for(int i=0;i<linearBackGroundListTab.length;i++){
                relativeBackgroundPlay[i].removeAllViews();
                relativeBackgroundPlay[i].addView(imageFileTab[i]);
                checkBoxItems[i].setChecked(false);
            }
            statusCheckBox = false;
        }
        //Pop Fragments off backstack and do your other checks
    }

    private void createTextPlay() {
        relativeHeadBackgroundPlay.removeAllViews();
        layoutTextPlay = new RelativeLayout.LayoutParams(textViewFile.getWidth(), textViewFile.getHeight());
        layoutTextPlay.width=ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutTextPlay.height=ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutTextPlay.addRule(RelativeLayout.CENTER_IN_PARENT);
        relativeHeadBackgroundPlay.addView(textViewFile,layoutTextPlay);
    }

    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    private Cursor getDatabase() {
        String sql = "SELECT * FROM FileTabGuitar";
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    private void setDelete(String fileId) {
        Cursor cursor = getFileById(fileId);
        cursor.moveToNext();
        File file = new File(cursor.getString(cursor.getColumnIndex(COLUMN_PATH)));
        boolean deleted = file.delete();
        Log.i("Delete :",""+deleted);
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        String sql = "_id=?";
        String[] args = {fileId};
        db.delete(TABLE_NAME, sql, args);
    }
    private Cursor getFileById(String clickFileID) {
        String sql = "SELECT * FROM FileTabGuitar WHERE _id = "+clickFileID;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    private Cursor getSearchString(String message) {
        String sql = "SELECT * FROM FileTabGuitar WHERE "+COLUMN_NAME+" LIKE '%"+message+"%' or "+COLUMN_CREATOR+" LIKE '%"+message+"%'";
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    private Cursor getSortName() {
        String sql = "SELECT * FROM FileTabGuitar ORDER BY name "+sortingName;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    private Cursor getSortCreator() {
        String sql = "SELECT * FROM FileTabGuitar ORDER BY creator "+sortingCreator;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    private Cursor getSortDate() {
        String sql = "SELECT * FROM FileTabGuitar ORDER BY datetime(date) "+sortingDate;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }

    private Cursor getUserID() {
        String sql = "SELECT "+COLUMN_USER_ID+" FROM "+ TABLE_USER_NAME;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }

    public EditText getEditTextSearch() {
        return editTextSearch;
    }

    public void setEditTextSearch(EditText editTextSearch) {
        this.editTextSearch = editTextSearch;
    }

}
