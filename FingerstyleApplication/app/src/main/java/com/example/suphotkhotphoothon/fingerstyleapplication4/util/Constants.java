package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.provider.BaseColumns;

public interface Constants extends BaseColumns {
    public static final String TABLE_NAME = "FileTabGuitar";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CREATOR = "creator";
    public static final String COLUMN_BPM = "bpm";
    public static final String COLUMN_SIGNATURE = "signature";
    public static final String COLUMN_LONGTIME = "longtime";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_STATUS = "status";

    public static final String COLUMN_FROM_SONG_ID = "fromSongID";
    public static final String COLUMN_FROM_USER_ID = "fromUserID";
}
