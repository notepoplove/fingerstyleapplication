package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.BackgroundRoom;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import java.util.ArrayList;

public class RoomOnClickListener implements View.OnClickListener {
    TabActivity tabActivity;
    ArrayList<ArrayList<Object>> collectionRoom;
    ArrayList<Note> notes;
    BackgroundRoom backgroundRoom;
    TextView textViewShowTime;

    public RoomOnClickListener(TabActivity tabActivity, BackgroundRoom backgroundRoom){
        this.tabActivity = tabActivity;
        this.collectionRoom = tabActivity.collectionRoom;
        this.notes = tabActivity.notes;
        this.backgroundRoom = backgroundRoom;
        this.textViewShowTime =tabActivity.textViewShowTime;
    }
    @Override
    public void onClick(View v) {
        if (v == backgroundRoom) {
            tabActivity.collectionNote.clear();
            tabActivity.checkSelectionNote = false;

            for(int j=0;j<tabActivity.imageViewStringNumber.length;j++){
                tabActivity.imageViewStringNumber[j].setBackgroundResource(0);
            }
            for (int i=0;i<notes.size();i++){
                notes.get(i).setBackgroundResource(0);
            }
            
            for (int i = 0; i < collectionRoom.size(); i++) {
                for (int j = 0; j < collectionRoom.get(i).size(); j++) {
                    BackgroundRoom backgroundRoomItem = (BackgroundRoom) collectionRoom.get(i).get(j);
                    if (backgroundRoomItem.getNote() == backgroundRoom.getNote()) {
                        long UpdateTime = backgroundRoomItem.getNote().getTime();
                        long timeStamp = UpdateTime;
                        long Seconds = (int) (UpdateTime / 1000);
                        long Minutes = Seconds / 60;
                        Seconds = Seconds % 60;
                        int MilliSeconds = (int) (UpdateTime % 1000);
                        textViewShowTime.setText("" +String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds) + ":" + String.format("%03d", MilliSeconds));

//                        backgroundRoomItem.getImageViewRoom().setBackgroundResource(R.color.colorGreen);
                    }
                    else {
//                        backgroundRoomItem.setBackgroundResource(R.color.colorGreen);
//                        backgroundRoomItem.getImageViewRoom().setBackgroundResource(R.color.colorBlack);
                    }
                }
            }
        }
    }
}
