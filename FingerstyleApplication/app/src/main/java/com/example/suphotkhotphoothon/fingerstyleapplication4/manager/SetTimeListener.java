package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import java.util.ArrayList;
import java.util.Timer;

public class SetTimeListener implements View.OnTouchListener {
    NoteListener noteListener;
    Note note;
    TextView textViewShowTime;
    TabActivity tabActivity;
    AppCompatImageView imageViewSetPlusTime;
    AppCompatImageView imageViewSetDeleteTime;
    ArrayList<Note> collectionNote;
    Note selectionNote[];
    ThreadSetTime threadSetTime;
    public SetTimeListener(TabActivity tabActivity) {
        this.tabActivity = tabActivity;
        this.selectionNote = tabActivity.selectionNote;
        this.collectionNote = tabActivity.collectionNote;
//        this.note = noteListener.note;
        this.imageViewSetDeleteTime = tabActivity.imageViewSetDeleteTime;
        this.imageViewSetPlusTime = tabActivity.imageViewSetPlusTime;
        threadSetTime = new ThreadSetTime(tabActivity);
        threadSetTime.start();
//        new Thread((Runnable) threadSetTime,"ss").start();
//        threadSetTime.start();
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
//        Log.i("Show",""+(v == imageViewSetPlusTime));
        if(v == imageViewSetDeleteTime){
            tabActivity.setChoiceSetTime("-");
        }
        if(v == imageViewSetPlusTime){
            tabActivity.setChoiceSetTime("+");
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            tabActivity.setCheckSetTime(true);
        }
        else if (event.getAction() == MotionEvent.ACTION_UP) {
            tabActivity.setCheckSetTime(false);
//            Log.i("checkSelectionNote",""+tabActivity.checkSelectionNote);
            if(tabActivity.checkSelectionNote){
                for(int j=0;j<collectionNote.size();j++){
                    int height = tabActivity.getHeightTeb(collectionNote.get(j), tabActivity.relativeBackgroundString[0]);
                    int width = height;
                    if (tabActivity.getChoiceSetTime() == "+") {
//                                        Log.i("Show",""+(i++));
                        collectionNote.get(j).setTime(collectionNote.get(j).getTime() + 1);
                        Log.i("Time", "" + collectionNote.get(j).getTime());
                    } else {
//                                        Log.i("Show",""+(i--));
                        collectionNote.get(j).setTime(collectionNote.get(j).getTime() - 1);
                        Log.i("Time", "" + collectionNote.get(j).getTime());
                    }
                    RelativeLayout.LayoutParams relativeNote = tabActivity.layoutParamsRelativeLayoutNote(width, height, tabActivity.millisecondToPx(collectionNote.get(j).getTime()));
                    collectionNote.get(j).setLayoutParams(relativeNote);
                }
                tabActivity.setTextViewShowTime(collectionNote.get(0).getTime());
            }
            else {
                if(selectionNote[0] != null) {
                    int height = tabActivity.getHeightTeb(selectionNote[0], tabActivity.relativeBackgroundString[0]);
                    int width = height;
                    if (tabActivity.getChoiceSetTime() == "+") {
//                                        Log.i("Show",""+(i++));
                        selectionNote[0].setTime(selectionNote[0].getTime() + 1);
                        Log.i("Time", "" + selectionNote[0].getTime());
                    } else {
//                                        Log.i("Show",""+(i--));
                        selectionNote[0].setTime(selectionNote[0].getTime() - 1);
                        Log.i("Time", "" + selectionNote[0].getTime());
                    }
                    tabActivity.setTextViewShowTime(selectionNote[0].getTime());
                    Log.i("Note", " S:" + selectionNote[0].getString() + " F:"
                            + selectionNote[0].getFret() + " T:" + selectionNote[0].getTime() + " R:" + selectionNote[0].getRoom());
                    Log.i("millisecondToPx",""+tabActivity.millisecondToPx(selectionNote[0].getTime()));
                    RelativeLayout.LayoutParams relativeNote = tabActivity.layoutParamsRelativeLayoutNote(width, height, tabActivity.millisecondToPx(selectionNote[0].getTime()));
                    selectionNote[0].setLayoutParams(relativeNote);
                }
            }
        }
        tabActivity.saveFileXML();
        return true;
    }
}
