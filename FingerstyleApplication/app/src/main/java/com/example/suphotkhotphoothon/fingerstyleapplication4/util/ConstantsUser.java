package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.provider.BaseColumns;

public interface ConstantsUser extends BaseColumns {
    public static final String TABLE_USER_NAME = "User";
    public static final String COLUMN_USER_ID = "userID";
}
