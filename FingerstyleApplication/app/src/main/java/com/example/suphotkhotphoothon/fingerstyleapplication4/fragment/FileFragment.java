package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.FileActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.BackgroundListComment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.pdf.codec.Base64;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_CREATOR;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_DATE;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_FROM_SONG_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_FROM_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_LONGTIME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_SIGNATURE;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_STATUS;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_BPM;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.TABLE_NAME;

/**
 * A simple {@link Fragment} subclass.
 */
public class FileFragment extends Fragment implements View.OnClickListener,View.OnKeyListener{

    private View view;
    private ImageView starRating[];
    private TextView textViewStar;
    private TextView textViewTitle;
    private TextView textViewCreator;
    private TextView textViewBpm;
    private TextView textViewSignature;
    private TextView textViewLongtime;
    private TextView textViewPublished;
    private TextView textViewScore;
    private TextView textViewView;
    private TextView textViewDownload;
    private TextView textViewComment;
    private TextView textViewSendScore;
    private EditText editTextComment;
    private AppCompatImageView imageViewImageUser;
    private AppCompatImageView imageViewImageSend;
    private RelativeLayout relativeBackGroundTextComment;
    private RelativeLayout relativeBackGroundTextSendScore;
    private RelativeLayout relativeBackGroundSendScore;
    private RelativeLayout relativeBackGroundImageUser;
    private LinearLayout linearBackGroundComment;
    private LinearLayout linearComment;
    private Button buttonDownload;
    private ProgressBar progressBarDownload;

    private FileActivity fileActivity;
    private JsonObject jsonObject;
    private SimpleDateFormat sdf;
    private SQLiteHelper sqLiteHelper;

    private CircleImageView circleImageViewUser;
    private DecimalFormat decimalFormat;

    public ArrayList<BackgroundListComment> arrayListBackground;

    private String fileID = null;
    private String userID = null;
    private int scoreSong = 0;
    private float density;
    private Bitmap bitmapImageUser;

    public FileFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_file, container, false);

        if (savedInstanceState == null) {
            Bundle extras = getActivity().getIntent().getExtras();
            if(extras == null) {
                fileID = null;
                userID = null;
            } else {
                fileID = extras.getString("file_id");
                userID = extras.getString("user_id");
            }
        } else {
            fileID = (String) savedInstanceState.getSerializable("file_id");
            userID = (String) savedInstanceState.getSerializable("user_id");
        }
        Log.i("User ID", userID);
        Log.i("File ID", fileID);
        init();
        getScoreSong();
        setNumberView();
//        setNumberDownload();
        selectFileByID();

        return view;
    }


    private void init() {
        starRating = new ImageView[5];
        starRating[0] = (ImageView) view.findViewById(R.id.fragmentFile_image_star1);
        starRating[1] = (ImageView) view.findViewById(R.id.fragmentFile_image_star2);
        starRating[2] = (ImageView) view.findViewById(R.id.fragmentFile_image_star3);
        starRating[3] = (ImageView) view.findViewById(R.id.fragmentFile_image_star4);
        starRating[4] = (ImageView) view.findViewById(R.id.fragmentFile_image_star5);
        textViewStar = (TextView)view.findViewById(R.id.fragmentFile_textView_star);
        textViewTitle = (TextView)view.findViewById(R.id.fragmentFile_textView_title);
        textViewCreator = (TextView)view.findViewById(R.id.fragmentFile_textView_creator);
        textViewBpm = (TextView)view.findViewById(R.id.fragmentFile_textView_bpm);
        textViewSignature = (TextView)view.findViewById(R.id.fragmentFile_textView_signature);
        textViewLongtime = (TextView)view.findViewById(R.id.fragmentFile_textView_longtime);
        textViewPublished = (TextView)view.findViewById(R.id.fragmentFile_textView_published);
        textViewScore = (TextView)view.findViewById(R.id.fragmentFile_textView_score);
        textViewView = (TextView)view.findViewById(R.id.fragmentFile_textView_view);
        textViewDownload = (TextView)view.findViewById(R.id.fragmentFile_textView_download);
        linearBackGroundComment = (LinearLayout)view.findViewById(R.id.fragmentFile_ll_backGroundComment);
        relativeBackGroundSendScore = (RelativeLayout)view.findViewById(R.id.fragmentFile_rl_backGroundSendScore);
        buttonDownload = (Button)view.findViewById(R.id.fragmentFile_button_download);
        progressBarDownload = (ProgressBar)view.findViewById(R.id.fragmentFile_pb_download);

        sqLiteHelper = new SQLiteHelper(getContext());
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        textViewComment = new TextView(getContext());
        textViewSendScore = new TextView(getContext());
        editTextComment = new EditText(getContext());
        relativeBackGroundTextComment = new RelativeLayout(getContext());
        relativeBackGroundTextSendScore = new RelativeLayout(getContext());
        relativeBackGroundImageUser = new RelativeLayout(getContext());
        linearComment = new LinearLayout(getContext());
        imageViewImageUser = new AppCompatImageView(getContext());
        imageViewImageSend = new AppCompatImageView(getContext());
        circleImageViewUser = new CircleImageView(getContext());
        decimalFormat = new DecimalFormat(".##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);

        arrayListBackground = new ArrayList<>();

        linearComment.setOrientation(LinearLayout.VERTICAL);

        textViewComment.setText("comment");
        textViewComment.setTextColor(getContext().getResources().getColor(R.color.colorBlueStar));
        editTextComment.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        editTextComment.setBackgroundResource(R.drawable.edittext_border);
        editTextComment.setTextSize(16);
        textViewSendScore.setText("send");
        textViewSendScore.setTextColor(getContext().getResources().getColor(R.color.colorBlueStar));
        imageViewImageSend.setImageResource(R.mipmap.baseline_send_black_18dp);
        imageViewImageSend.setColorFilter(getContext().getResources().getColor(R.color.colorBlueStar));
        relativeBackGroundSendScore.setVisibility(View.INVISIBLE);

        linearBackGroundComment.setOrientation(LinearLayout.VERTICAL);
        buttonDownload.setOnClickListener(this);
        for(int i=0;i<starRating.length;i++){
            starRating[i].setOnClickListener(this);
        }
        relativeBackGroundTextComment.setOnClickListener(this);
        relativeBackGroundTextSendScore.setOnClickListener(this);
        imageViewImageSend.setOnClickListener(this);

        RelativeLayout.LayoutParams paramsRelativeTextComment = paramsRelativeTextComment();
        RelativeLayout.LayoutParams paramsRelativeTextSendScore = paramsRelativeTextSendScore();
        RelativeLayout.LayoutParams paramsRelativeBackgroundSendScore = paramsRelativeBackgroundSendScore();
        LinearLayout.LayoutParams paramsBackGroundTextComment = paramsLinearBackGroundTextComment();
        LinearLayout.LayoutParams paramsBackGroundComment = paramsLinearBackGroundComment();

        relativeBackGroundTextComment.addView(textViewComment,paramsRelativeTextComment);
        relativeBackGroundTextSendScore.addView(textViewSendScore,paramsRelativeTextSendScore);
        relativeBackGroundSendScore.addView(relativeBackGroundTextSendScore,paramsRelativeBackgroundSendScore);
        linearComment.addView(relativeBackGroundTextComment,paramsBackGroundTextComment);
        linearBackGroundComment.addView(linearComment, paramsBackGroundComment);
    }

    @Override
    public void onClick(View v) {
        if(v == buttonDownload){
            Log.i("Download","Download");
            loadFileXML();
            setNumberDownload();
//            Cursor cursorMaxId = getMAxID();
//            cursorMaxId.moveToNext();
//            Log.i("MAX Id",""+cursorMaxId.getColumnName(0));
        }
        else if(v == relativeBackGroundTextComment){
            relativeBackGroundTextComment.removeAllViews();
            selectAllUser();
//            circleImageViewUser.setImageResource(R.mipmap.note);

            LinearLayout.LayoutParams paramsLinearBackGroundImageUser = paramsLinearBackGroundImageUser();
            LinearLayout.LayoutParams paramsLinearBackGroundImageSend = paramsLinearBackGroundImageSend();
            LinearLayout.LayoutParams paramsLinearBackGroundEditTextComment = paramsLinearBackGroundEditTextComment();

            linearComment.setOrientation(LinearLayout.HORIZONTAL);

            linearComment.addView(circleImageViewUser, paramsLinearBackGroundImageUser);
            linearComment.addView(editTextComment,paramsLinearBackGroundEditTextComment);
            linearComment.addView(imageViewImageSend,paramsLinearBackGroundImageSend);
            getAllHeadComment();
        }
        else if(v == relativeBackGroundTextSendScore){
            Log.i("Score","Score");
            setScoreSong();
        }
        else if(v == imageViewImageSend){
            jsonObject = new JsonObject();
            String currentTime = sdf.format(new Date());
            jsonObject.addProperty("user_id",userID);
            jsonObject.addProperty("fileServer_id",fileID);
            jsonObject.addProperty("comment_message",editTextComment.getText().toString());
            jsonObject.addProperty("comment_date",currentTime);
            if(!editTextComment.getText().toString().equals("")) {
                editTextComment.setText("");
                Ion.with(getContext())
                        .load("POST", "http://guitar.msuproject.net/public/managecomment.php/sendHeadComment")
                        .setJsonObjectBody(jsonObject)
                        .asJsonObject()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<JsonObject>>() {
                            @Override
                            public void onCompleted(Exception e, Response<JsonObject> result) {
                                Log.i("Response Send", "" + result.getResult());
//                                editTextComment.setText("");
                                JsonObject jsonObject = new JsonParser().parse(""+result.getResult()).getAsJsonObject();
                                showComment(jsonObject);
                                closeKeyboard();
//                                editTextComment.setVisibility(View.GONE);
                            }
                        });
            }
            else {
                Toast.makeText(getActivity(), "Must have text!", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            relativeBackGroundSendScore.setVisibility(View.VISIBLE);
            for (int i = 0; i < starRating.length; i++) {
                starRating[i].setImageResource(R.mipmap.baseline_star_black_18dp);
                starRating[i].setColorFilter(this.getResources().getColor(R.color.colorBlueStar));
                if (v == starRating[i]) {
                    if (i == 0) {
                        textViewStar.setText("ไม่ชอบเลย");
                        scoreSong = 1;
                    }
                    else if (i == 1) {
                        textViewStar.setText("ไม่ค่อยเลย");
                        scoreSong = 2;
                    }
                    else if (i == 2) {
                        textViewStar.setText("เฉยๆ");
                        scoreSong = 3;
                    }
                    else if (i == 3) {
                        textViewStar.setText("ชอบ");
                        scoreSong = 4;
                    }
                    else if (i == 4) {
                        textViewStar.setText("ชอบที่สุด");
                        scoreSong = 5;
                    }
                    for (int j = i + 1; j < starRating.length; j++) {
                        starRating[j].setImageResource(R.mipmap.baseline_star_border_black_18dp);
                        starRating[j].setColorFilter(this.getResources().getColor(R.color.colorBlack));
                    }
                    break;
                }
            }
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        return true;
    }

    private void selectAllUser() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("user_id",userID);
        Ion.with(getContext())
                .load("POST","http://guitar.msuproject.net/public/user.php/user")
                .setJsonObjectBody(jsonObject)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.i("JsonObject",""+result);

                        byte[] bytes = Base64.decode(result.get("user_image").getAsString());
                        bitmapImageUser = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        Log.i("Bitmap Input:",""+bitmapImageUser);
                        circleImageViewUser.setImageBitmap(bitmapImageUser);
                    }
                });
    }

    public void selectFileByID(){
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("POST","http://guitar.msuproject.net/public/manageFile.php/selectFileByID")
                .setJsonObjectBody(jsonObject)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.i("JsonObject",""+result);
                        textViewTitle.setText(result.get("fileServer_name").getAsString());
                        textViewCreator.setText(result.get("user_name").getAsString());
                        textViewBpm.setText(result.get("fileServer_bpm").getAsString());
                        textViewSignature.setText(result.get("fileServer_signature").getAsString());
                        textViewLongtime.setText(result.get("fileServer_longtime").getAsString());
                        textViewPublished.setText(result.get("fileServer_date").getAsString());
                        textViewDownload.setText(result.get("fileServer_numberDownload").getAsString());
                        textViewView.setText(result.get("fileServer_numberView").getAsString());
//                        textViewScore.setText(result.get("fileServer_score").getAsString());
                    }
                });
    }
    private void loadFileXML() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("POST","http://guitar.msuproject.net/public/manageFile.php/selectFileByID")
                .setJsonObjectBody(jsonObject)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        final String userID = result.get("user_id").getAsString();
                        final String fileID = result.get("fileServer_id").getAsString();
                        final String fileName = result.get("fileServer_name").getAsString();
                        final String userName = result.get("user_name").getAsString();
                        final String fileBpm = result.get("fileServer_bpm").getAsString();
                        final String fileSignature = result.get("fileServer_signature").getAsString();
                        String str[] = fileSignature.split("/");
                        final String signature1 = str[0];
                        final String signature2 = str[1];
                        final String fileLongtime = result.get("fileServer_longtime").getAsString();
                        final String currentTime = sdf.format(new Date());
                        Log.i("Shoe","From ID:"+userID+" File ID"+fileID+" File Name:"+fileName+" User Name:"+userName+" File Bpm:"+fileBpm+" File Signature:"+fileSignature);
                        Ion.with(getContext())
                                .load("POST","http://guitar.msuproject.net/public/manageFile.php/downloadFileXML")
                                .setJsonObjectBody(jsonObject)
                                .asString()
                                .withResponse()
                                .setCallback(new FutureCallback<Response<String>>() {
                                    @Override
                                    public void onCompleted(Exception e, Response<String> result) {
                                        Log.i("XML",""+result.getResult());
                                        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
                                        ContentValues values = new ContentValues();
                                        values.put(COLUMN_NAME,fileName);
                                        values.put(COLUMN_CREATOR,userName);
                                        values.put(COLUMN_BPM,fileBpm);
                                        values.put(COLUMN_SIGNATURE,signature1+"/"+signature2);
                                        values.put(COLUMN_LONGTIME,fileLongtime);
                                        values.put(COLUMN_DATE,currentTime);
                                        values.put(COLUMN_STATUS,"download");
                                        values.put(COLUMN_FROM_SONG_ID,fileID);
                                        values.put(COLUMN_FROM_USER_ID,userID);
                                        db.insertOrThrow(TABLE_NAME,null,values);

                                        Cursor cursorMaxId = getMAxID();
                                        cursorMaxId.moveToNext();
                                        String path = saveXML(cursorMaxId.getString(0).toString(),fileName,result.getResult());
                                        setUpdatePath(path,cursorMaxId.getString(0).toString());
                                    }
                                });
                    }
                });
    }

    private void getAllHeadComment() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("http://guitar.msuproject.net/public/manageComment.php/selectHeadComment")
                .setJsonObjectBody(jsonObject)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray jsonArray) {
                        Log.i("JsonArray",""+jsonArray);
//                        linearBackGroundListTab = new BackgroundListTab[result.size()];
//                        showComments(result);
                        try {
                            for (int i = 0; i<jsonArray.size(); i++) {
                                Log.i("jsonArray", "" + jsonArray.get(i));
                                JsonObject jsonObject = (JsonObject) jsonArray.get(i);
                                showComment(jsonObject);
                            }
                        } catch (Exception e1) {
                            Log.i("JSONException", "Kuy");
                            e1.printStackTrace();
                        }
                    }
                });
    }

    private void  getScoreSong() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("POST", "http://guitar.msuproject.net/public/manageFile.php/selectScoreSong")
                .setJsonObjectBody(jsonObject)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        Log.i("Number getScoreSong",""+result.getResult());
                        double score = Double.parseDouble(result.getResult());
                        if(score>0){
                            textViewScore.setText(decimalFormat.format(score));
                        }
                        else {
                            textViewScore.setText("0");
                        }
                    }
                });
    }

    private void setScoreSong() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("user_id",userID);
        jsonObject.addProperty("fileServer_id",fileID);
        jsonObject.addProperty("score_song",scoreSong);
        Ion.with(getContext())
                .load("POST", "http://guitar.msuproject.net/public/manageFile.php/scoreSong")
                .setJsonObjectBody(jsonObject)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        Log.i("Number setScoreSong",""+result.getResult());
                        getScoreSong();
//                        textViewScore.setText(result.getResult());
                    }
                });
    }

    private void setNumberView() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("POST", "http://guitar.msuproject.net/public/manageFile.php/numberViewFile")
                .setJsonObjectBody(jsonObject)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        Log.i("Number view",""+result.getResult());
                        textViewView.setText(result.getResult());
                    }
                });
    }

    private void setNumberDownload() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("fileServer_id",fileID);
        Ion.with(getContext())
                .load("POST", "http://guitar.msuproject.net/public/manageFile.php/numberDownloadFile")
                .setJsonObjectBody(jsonObject)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        textViewDownload.setText(result.getResult());
                        Log.i("Number Download",""+result.getResult());
                    }
                });
    }
    private void showComment(JsonObject jsonObject) {
        arrayListBackground.add(new BackgroundListComment(this,jsonObject));
        linearBackGroundComment.addView(arrayListBackground.get(arrayListBackground.size()-1),1);
    }

    private String saveXML(String fileID, String fileName, String fileXML) {
        String path = "";
        StringWriter writer = new StringWriter();
        Log.i("MAX Id",""+fileID);
        Log.i("XML",""+writer.toString());

        File internalDir = getActivity().getFilesDir();
        File dir = new File(internalDir.getAbsolutePath()+"/Fingerstyle XML");
        if(!dir.exists()){
            dir.mkdir();
        }

        File file = new File(dir,"FID"+fileID+fileName+".xml");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(fileXML.toString().getBytes());
            fileOutputStream.close();
            Toast.makeText(getContext(),"Download success", Toast.LENGTH_LONG).show();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        path = file.getAbsolutePath();
        Log.w("PATH",path);
        return path;
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    private void openKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null){
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    private LinearLayout.LayoutParams paramsLinearBackGroundComment() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = dpToPx(8);
        layoutParams.rightMargin = dpToPx(8);
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearBackGroundListComment() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        return layoutParams;
    }
    private RelativeLayout.LayoutParams paramsRelativeTextComment() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = dpToPx(8);
        layoutParams.bottomMargin = dpToPx(8);
        return layoutParams;
    }
    private RelativeLayout.LayoutParams paramsRelativeTextSendScore() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = dpToPx(5);
        layoutParams.bottomMargin = dpToPx(5);
        layoutParams.rightMargin = dpToPx(5);
        layoutParams.leftMargin = dpToPx(5);
        return layoutParams;
    }
    private RelativeLayout.LayoutParams paramsRelativeBackgroundSendScore() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearBackGroundTextComment() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        return layoutParams;
    }

    private LinearLayout.LayoutParams paramsLinearBackGroundEditTextComment() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT,1);
        layoutParams.gravity = Gravity.CENTER;
        return layoutParams;
    }
    private ViewGroup.LayoutParams paramsImageUser() {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(dpToPx(30),dpToPx(30));
        return layoutParams;
    }
    private ViewGroup.LayoutParams paramsImageSend() {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(dpToPx(30),dpToPx(30));
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearBackGroundImageUser() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dpToPx(30),dpToPx(30));
        layoutParams.topMargin = dpToPx(8);
        layoutParams.rightMargin = dpToPx(5);
        layoutParams.bottomMargin = dpToPx(8);
        layoutParams.gravity = Gravity.CENTER;
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearBackGroundImageSend() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dpToPx(30),dpToPx(30));
        layoutParams.topMargin = dpToPx(8);
        layoutParams.leftMargin = dpToPx(8);
        layoutParams.bottomMargin = dpToPx(8);
        layoutParams.gravity = Gravity.CENTER;
        return layoutParams;
    }

    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    private Cursor getMAxID() {
        String sql = "SELECT MAX(_id) FROM FileTabGuitar";
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    private void setUpdatePath(String path,String fileId) {
        String sql = "UPDATE FileTabGuitar SET path = ? WHERE _id = ?";
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        String[] dataPath = {path,fileId};
        db.execSQL(sql,dataPath);
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public CircleImageView getCircleImageViewUser() {
        return circleImageViewUser;
    }

    public void setCircleImageViewUser(CircleImageView circleImageViewUser) {
        this.circleImageViewUser = circleImageViewUser;
    }
    public Bitmap getBitmapImageUser() {
        return bitmapImageUser;
    }

    public void setBitmapImageUser(Bitmap bitmapImageUser) {
        this.bitmapImageUser = bitmapImageUser;
    }

}
