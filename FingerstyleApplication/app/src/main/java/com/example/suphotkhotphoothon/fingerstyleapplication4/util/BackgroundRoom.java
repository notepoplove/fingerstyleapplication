package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;

public class BackgroundRoom extends RelativeLayout {
    TabActivity tabActivity;
    Note note;
    ImageView imageViewRoom;
    private float density;
    public BackgroundRoom(TabActivity tabActivity, Note note) {
        super(tabActivity);
        this.tabActivity = tabActivity;
        this.note = note;
        imageViewRoom = new ImageView(tabActivity);
        imageViewRoom.setBackgroundColor(tabActivity.getResources().getColor(R.color.colorGray));
        RelativeLayout.LayoutParams layoutParamsString = layoutParamsString();
        addView(imageViewRoom,layoutParamsString);
    }
    public Note getNote(){ return this.note; }
    public void setNote(Note note){
         this.note = note;
    }
    public ImageView getImageViewRoom(){ return this.imageViewRoom; }
    public void setImageViewRoom(ImageView imageViewRoom){
        this.imageViewRoom = imageViewRoom;
    }

    private RelativeLayout.LayoutParams layoutParamsString() {
        RelativeLayout.LayoutParams layoutParamsString = new RelativeLayout.LayoutParams(dpToPx(3),ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParamsString.addRule(RelativeLayout.CENTER_HORIZONTAL);
        return layoutParamsString;
    }
    public int dpToPx(int dp) {
        density = tabActivity.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
}
