package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.TABLE_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants._ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_CREATOR;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_BPM;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_SIGNATURE;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_LONGTIME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_DATE;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_PATH;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_STATUS;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_FROM_SONG_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_FROM_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.COLUMN_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.TABLE_USER_NAME;

public class SQLiteHelper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "TabGuitar.db";
    private static final int DATABASE_VERSION = 8;
    public SQLiteHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    private static final String CREATE_TABLE_NAME = "CREATE TABLE " + TABLE_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_NAME + " TEXT, "
            + COLUMN_CREATOR + " TEXT, "
            + COLUMN_BPM + " TEXT, "
            + COLUMN_SIGNATURE + " TEXT, "
            + COLUMN_LONGTIME + " TEXT, "
            + COLUMN_DATE + " DATETIME , "
            + COLUMN_PATH + " PATH, "
            + COLUMN_STATUS + " TEXT, "
            + COLUMN_FROM_SONG_ID + " TEXT DEFAULT NULL, "
            + COLUMN_FROM_USER_ID + " TEXT DEFAULT NULL);";
    private static final String CREATE_TABLE_USER_NAME = "CREATE TABLE " + TABLE_USER_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
            + COLUMN_USER_ID + " TEXT DEFAULT NULL);";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_NAME);
        db.execSQL(CREATE_TABLE_USER_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_NAME + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_USER_NAME + "'");
        onCreate(db);
    }
}
