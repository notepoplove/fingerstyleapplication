package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.MainActivity;

public class NetworkConnectReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent!=null){
            ConnectivityManager connectManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connectManager.getActiveNetworkInfo();
            String resultConnect = "";
            boolean connect = false;

            if(netInfo != null && netInfo.isConnected()){
                resultConnect = "Connect success";
                connect = true;
            }
            else {
                resultConnect = "Connect fail";
                connect = false;
            }
            Log.w("Connect Network Receive",resultConnect);
        }
    }
}
