package com.example.suphotkhotphoothon.fingerstyleapplication4.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.EditProjectFragment;

public class EditProjectActivity extends AppCompatActivity implements View.OnClickListener {

    private FragmentTransaction fragmentTran;
    private FragmentManager fragmentManager;
    private EditProjectFragment editProjectFragment;

    private FrameLayout frameEditProject;
    private AppCompatImageButton imageBack;

    private String fileID = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project);

        init();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                fileID = null;
            } else {
                fileID = extras.getString("file_id");
            }
        } else {
            fileID = (String) savedInstanceState.getSerializable("file_id");
        }
        if(fileID != null){


        }
    }

    private void init() {
        fragmentTran = getSupportFragmentManager().beginTransaction();
        frameEditProject = (FrameLayout)findViewById(R.id.editProject_frame);
        imageBack = (AppCompatImageButton)findViewById(R.id.editProject_imb_back);
        fragmentManager = getSupportFragmentManager();

        editProjectFragment = new EditProjectFragment();


        setFragment(editProjectFragment);

        imageBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == imageBack){
            onBackPressed();
        }

    }

    private void setFragment(Fragment fragment) {
        fragmentTran.replace(frameEditProject.getId(),fragment);
        //fragmentTran.addToBackStack(null);
        fragmentTran.commit();
    }
}
