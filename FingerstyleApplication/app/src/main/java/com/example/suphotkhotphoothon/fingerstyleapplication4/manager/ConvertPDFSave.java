package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.PDFActivity;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class ConvertPDFSave {
    public ConvertPDFSave(PDFActivity pdfActivity, ArrayList<LinearLayout> pdf, String name) {
        File internalDir = Environment.getExternalStorageDirectory();
        File dir = new File(internalDir.getAbsolutePath()+"/Fingerstyle PDF");//tabEX.pdf
        if(!dir.exists()){
            dir.mkdir();
        }
        String directoryPath =  dir.getAbsolutePath();
        Log.w("Path:",""+directoryPath);
        File file = new File(directoryPath, pdfActivity.nameSong+".pdf");
        int count = 1;
        while (file.exists()){
//            Log.w("File :",""+file.getAbsolutePath());
            String suffix = "-"+count;
            file = new File(directoryPath+"/"+pdfActivity.nameSong+suffix+".pdf");
            count++;
        }

        Document document = new Document(PageSize.A4, 20, 20, 20, 20);
        try {
            PdfWriter.getInstance(document, new FileOutputStream(file)); //  Change pdf's name.
            document.open();
            for(int i=0;i<pdf.size();i++){
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                pdf.get(i).setDrawingCacheEnabled(true);
                pdf.get(i).measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                pdf.get(i).buildDrawingCache(true);
                Bitmap b = Bitmap.createBitmap(pdf.get(i).getDrawingCache());
                pdf.get(i).setDrawingCacheEnabled(false);
                b.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());
                float scale = ((document.getPageSize().getWidth() - document.leftMargin()
                        - document.rightMargin() - 0) / image.getWidth()) * 100;
                image.scalePercent(scale);
                image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
                document.add(image);
            }
            Toast.makeText(pdfActivity,"Save success", Toast.LENGTH_LONG).show();
            document.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
