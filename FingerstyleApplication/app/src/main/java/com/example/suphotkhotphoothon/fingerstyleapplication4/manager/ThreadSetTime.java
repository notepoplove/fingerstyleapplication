package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.util.Log;
import android.widget.RelativeLayout;

import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import java.util.ArrayList;

public class ThreadSetTime extends Thread{
    TabActivity tabActivity;
    ArrayList<Note> collectionNote;
    Note selectionNote[];
    boolean checkPress = false;
    int i = 0;
    int time = 0;
    public ThreadSetTime(TabActivity tabActivity) {
        this.tabActivity = tabActivity;
        this.selectionNote = tabActivity.selectionNote;
        this.collectionNote = tabActivity.collectionNote;
//        tabActivity.setCheckSetTime(true);
    }

    @Override
    public void run() {
        while (true){
            try {
                while (tabActivity.getCheckSetTime()) {
                    if(checkPress == false){
                        time++;
//                        Log.i("Check",""+(time));
                        Thread.sleep(1);
                        if(time == 100){
                            checkPress = true;
                        }
                    }
                    if(checkPress) {
                        tabActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if(tabActivity.checkSelectionNote){
                                        for(int j=0;j<collectionNote.size();j++){
                                            int height = tabActivity.getHeightTeb(collectionNote.get(j), tabActivity.relativeBackgroundString[0]);
                                            int width = height;
                                            if (tabActivity.getChoiceSetTime() == "+") {
//                                        Log.i("Show",""+(i++));
                                                collectionNote.get(j).setTime(collectionNote.get(j).getTime() + 1);
                                                Log.i("Time", "" + collectionNote.get(j).getTime());
                                            } else {
//                                        Log.i("Show",""+(i--));
                                                collectionNote.get(j).setTime(collectionNote.get(j).getTime() - 1);
                                                Log.i("Time", "" + collectionNote.get(j).getTime());
                                            }
                                            RelativeLayout.LayoutParams relativeNote = tabActivity.layoutParamsRelativeLayoutNote(width, height, tabActivity.millisecondToPx(collectionNote.get(j).getTime()));
                                            collectionNote.get(j).setLayoutParams(relativeNote);
                                        }
                                        tabActivity.setTextViewShowTime(collectionNote.get(0).getTime());
                                    }
                                    else {
                                        if (selectionNote[0] != null) {
                                            int height = tabActivity.getHeightTeb(selectionNote[0], tabActivity.relativeBackgroundString[0]);
                                            int width = height;
                                            if (tabActivity.getChoiceSetTime() == "+") {
//                                        Log.i("Show",""+(i++));
                                                selectionNote[0].setTime(selectionNote[0].getTime() + 1);
//                                            Log.i("Time", "" + selectionNote[0].getTime());
                                            } else {
//                                        Log.i("Show",""+(i--));
                                                selectionNote[0].setTime(selectionNote[0].getTime() - 1);
//                                            Log.i("Time", "" + selectionNote[0].getTime());
                                            }
                                            tabActivity.setTextViewShowTime(selectionNote[0].getTime());
//                                        Log.i("Note", " S:" + selectionNote[0].getString() + " F:"+ selectionNote[0].getFret() + " T:" + selectionNote[0].getTime() + " R:" + selectionNote[0].getRoom());
//                                        Log.i("millisecondToPx",""+tabActivity.millisecondToPx(selectionNote[0].getTime()));
                                            RelativeLayout.LayoutParams relativeNote = tabActivity.layoutParamsRelativeLayoutNote(width, height, tabActivity.millisecondToPx(selectionNote[0].getTime()));
                                            selectionNote[0].setLayoutParams(relativeNote);
                                        }
                                    }
                                } catch (Exception e) {

                                }
                            }
                        });
                        Thread.sleep(5);
                    }
                }
                checkPress = false;
                time = 0;
            }catch (Exception e){

            }
        }
    }
}
