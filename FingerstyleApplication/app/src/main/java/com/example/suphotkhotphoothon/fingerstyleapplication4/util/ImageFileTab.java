package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;

public class ImageFileTab extends AppCompatImageView {
    String fileId = null;
    public ImageFileTab(Context context) {
        super(context);
    }
    public void setFileId(String fileId){
        this.fileId = fileId;
    }
    public String getFileId(){
        return this.fileId;
    }
}
