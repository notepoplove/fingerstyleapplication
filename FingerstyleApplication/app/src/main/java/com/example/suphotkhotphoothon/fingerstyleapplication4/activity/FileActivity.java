package com.example.suphotkhotphoothon.fingerstyleapplication4.activity;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.FileFragment;

public class FileActivity extends AppCompatActivity implements View.OnClickListener{
    private FragmentTransaction fragmentTran;
    private FrameLayout frameFile;
    private FragmentManager fragmentManager;
    private FileFragment fileFragment;
    private AppCompatImageButton imageButtonBackFile;

//    public String fileID = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);
        init();


    }

    private void init() {
        fragmentTran = getSupportFragmentManager().beginTransaction();
        imageButtonBackFile = (AppCompatImageButton)findViewById(R.id.file_imb_back);
        frameFile = (FrameLayout) findViewById(R.id.file_frame);

        fragmentManager = getSupportFragmentManager();
        fileFragment = new FileFragment();

        setFragment(fileFragment);

        imageButtonBackFile.setOnClickListener(this);
    }

    private void setFragment(Fragment fragment) {
        fragmentTran.replace(frameFile.getId(),fragment);
        //fragmentTran.addToBackStack(null);
        fragmentTran.commit();
    }

    @Override
    public void onClick(View v) {
        if(v == imageButtonBackFile){
            onBackPressed();
//            if(fragmentManager.getBackStackEntryCount()>0){
//                fragmentManager.popBackStack();
//            }
//            else {
//                Toast.makeText(FileActivity.this,"Back",Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(FileActivity.this, MainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                finish();
//            }
        }
    }

//    public String getFileID(){
//        return this.fileID;
//    }
//    public void setFileID(String fileID){
//        this.fileID = fileID;
//    }
}
