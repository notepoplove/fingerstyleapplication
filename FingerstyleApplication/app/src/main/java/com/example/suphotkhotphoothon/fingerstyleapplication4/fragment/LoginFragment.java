package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.MainActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.COLUMN_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.TABLE_USER_NAME;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener{

    SQLiteHelper sqLiteHelper;
    SQLiteDatabase db;
    ContentValues values;

    private View view;
    private FragmentTransaction fragmentTran;
    private RegisterFragment registerFragment;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private Button buttonLogin;
    private Button buttonRegister;
    private AppCompatImageButton imageButtonFacebook;
    private AppCompatImageButton imageAccount;

    private JsonObject jsonObjectLogin;

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);



    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);

        init();
        sqLiteHelper = new SQLiteHelper(getContext());

        return view;
    }

    private void init() {
        fragmentTran = getActivity().getSupportFragmentManager().beginTransaction();
        editTextEmail = (EditText)view.findViewById(R.id.fragmentLoginedt_et_email);
        editTextPassword = (EditText)view.findViewById(R.id.fragmentLoginedt_et_password);
        buttonLogin = (Button) view.findViewById(R.id.fragmentLogin_btn_login);
        buttonRegister = (Button) view.findViewById(R.id.fragmentLogin_btn_register);
        imageButtonFacebook = (AppCompatImageButton)view.findViewById(R.id.fragmentLogin_imb_facebook);
        imageAccount = (AppCompatImageButton) view.findViewById(R.id.image_account);

        registerFragment = new RegisterFragment();
        jsonObjectLogin = new JsonObject();

        buttonLogin.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);
        imageButtonFacebook.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v == buttonLogin){
            if(connectNetwork()){
                login();
            }
            else {
                messageDialogConnect();
            }
        }
        else if(v == buttonRegister){
            setFragment(registerFragment);
        }
        else if(v == imageButtonFacebook){

        }
    }

    private void login(){
        db = sqLiteHelper.getReadableDatabase();
        values = new ContentValues();
        String emailStr = editTextEmail.getText().toString();
        if(validate(emailStr)){
            jsonObjectLogin.addProperty("user_email", emailStr);
            jsonObjectLogin.addProperty("user_password", editTextPassword.getText().toString());
            Ion.with(this)
                    .load("POST", "http://guitar.msuproject.net/public/user.php/login")
                    .setJsonObjectBody(jsonObjectLogin)
                    .asString()
                    .withResponse()
                    .setCallback(new FutureCallback<Response<String>>() {
                        @Override
                        public void onCompleted(Exception e, Response<String> result) {
                            String message = result.getResult().toString();
                            if (message.equals("Unsuccessful")) {
                                Toast.makeText(getActivity(), "email or code is invalid", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.w("Message", "" + message);
                                values.put(COLUMN_USER_ID, message);
                                db.insert(TABLE_USER_NAME, null, values);

                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                getActivity().startActivity(intent);
                                getActivity().finish();
                            }
                        }
                    });
        }
        else {
            Toast.makeText(getActivity(), "email is invalid", Toast.LENGTH_SHORT).show();
        }
    }

    private Boolean connectNetwork() {
        ConnectivityManager connectManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectManager.getActiveNetworkInfo();
        String resultConnect = "";
        boolean connect = false;

        if(netInfo != null && netInfo.isConnected()){
            resultConnect = "Connect success";
            connect = true;
        }
        else {
            resultConnect = "Connect fail";
            connect = false;
        }
        Log.w("Connect Network",resultConnect);
        return connect;
    }

    private void messageDialogConnect() {
        String message = "Please connect to the internet.";

        new AlertDialog.Builder(getActivity())
                .setIcon(R.mipmap.baseline_report_black_48dp)
                .setTitle("Internet")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(connectNetwork()){
                            login();
                        }
                        else {
                            messageDialogConnect();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Cancel","Cancel");
//                        getActivity().onBackPressed();
                    }
                })
                .show();
    }

    public static boolean validate(String emailString) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailString);
        return matcher.find();
    }

    private void setFragment(Fragment fragment) {
        fragmentTran.replace(R.id.account_frame, fragment);
        fragmentTran.addToBackStack(null);
        fragmentTran.commit();
    }
}
