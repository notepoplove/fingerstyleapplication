package com.example.suphotkhotphoothon.fingerstyleapplication4.manager;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import java.util.ArrayList;

public class NoteOnTouchListener implements View.OnTouchListener {
    TabActivity tabActivity;
    ArrayList<ArrayList<Object>> collectionRoom;
    ArrayList<Note> notes;
    Note note;
    SeekBar seekBarFret;
    RelativeLayout relativeLayoutBackgroundSeekBar;
    RelativeLayout relativeBackgroundString;

    private int xDelta;
    public NoteOnTouchListener(TabActivity tabActivity, Note note, SeekBar seekBarFret){
        this.tabActivity = tabActivity;
        relativeBackgroundString = tabActivity.relativeBackgroundString[note.getString()-1];
        relativeLayoutBackgroundSeekBar = tabActivity.relativeLayoutBackgroundSeekBar;
        this.notes = tabActivity.notes;
        this.collectionRoom = tabActivity.collectionRoom;
        this.note = note;
        this.seekBarFret = seekBarFret;
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int x = (int) event.getRawX();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v.getLayoutParams();

                xDelta = x - lParams.leftMargin;
                break;

            case MotionEvent.ACTION_UP:
                Toast.makeText(tabActivity,
                        "thanks for new location!", Toast.LENGTH_SHORT)
                        .show();
                break;

            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
//                layoutParams.width = 56;
//                layoutParams.height = 56;
                layoutParams.leftMargin = x - xDelta;
                layoutParams.rightMargin = 0;
                layoutParams.bottomMargin = 0;
                v.setLayoutParams(layoutParams);
                break;
        }
//        relativeBackgroundString.invalidate();
        return true;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutSeekBar(SeekBar seekBar) {
        RelativeLayout.LayoutParams relativeSeekBar = new RelativeLayout.LayoutParams(seekBar.getWidth(),seekBar.getHeight());
        relativeSeekBar.width = ViewGroup.LayoutParams.MATCH_PARENT;
        relativeSeekBar.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        relativeSeekBar.addRule(RelativeLayout.CENTER_VERTICAL);
        return relativeSeekBar;
    }
}
