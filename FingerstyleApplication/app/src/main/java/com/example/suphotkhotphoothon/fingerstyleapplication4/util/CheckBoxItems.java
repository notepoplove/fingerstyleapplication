package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.widget.CheckBox;

public class CheckBoxItems extends android.support.v7.widget.AppCompatCheckBox {
    String fileId = null;
    public CheckBoxItems(Context context) {
        super(context);
    }
    public void setFileId(String fileId){
        this.fileId = fileId;
    }
    public String getFileId(){
        return this.fileId;
    }
}
