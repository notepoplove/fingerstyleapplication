package com.example.suphotkhotphoothon.fingerstyleapplication4.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.MachineFragment;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.PublicFragment;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.TunerFragment;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.COLUMN_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.TABLE_USER_NAME;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private FragmentTransaction fragmentTran;
    private FrameLayout mMainFrame;
    private Fragment indexFragment;
    public BottomNavigationView mMainNav;
    private MachineFragment machineFragment;
    private TunerFragment tunerFragment;
    private PublicFragment publicFragment;
    private AppCompatImageView imvAccount;
    private LinearLayout linearBackgroundActionBar;

    private String fragment = "";
    private String userID = null;
    boolean doubleBackToExitPressedOnce = false;

    private SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.w("Main","Main");
        linearBackgroundActionBar =(LinearLayout) findViewById(R.id.mainActivity_ll_ActionBar);
        mMainFrame = (FrameLayout)findViewById(R.id.main_frame);
        mMainNav = (BottomNavigationView)findViewById(R.id.main_nav);
        imvAccount = (AppCompatImageView)findViewById(R.id.image_account);

        sqLiteHelper = new SQLiteHelper(this);
        machineFragment = new MachineFragment();
        tunerFragment = new TunerFragment();
        publicFragment = new PublicFragment();

        setFragment(machineFragment);

        linearBackgroundActionBar.setOnClickListener(this);
        imvAccount.setOnClickListener(this);

        mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId()){
//                    case R.id.nav_machine : {
//                        setFragment(machineFragment);
//                        return true;
//                    }
//                    case R.id.nav_tuner : {
//                        setFragment(tunerFragment);
//                        return true;
//                    }
//                    case R.id.nav_public : {
//                        setFragment(publicFragment);
//                        return true;
//                    }
//                }
                if(item.getItemId() == R.id.nav_machine){
                    setFragment(machineFragment);
                    return true;
                }
                else if(item.getItemId() == R.id.nav_tuner){
                    setFragment(tunerFragment);
                    return true;
                }
                else if(item.getItemId() == R.id.nav_public){
                    Cursor cursorUserId = getUserID();
                    cursorUserId.moveToNext();
                    Log.w("cursor",""+cursorUserId.getCount());
                    if (cursorUserId.getCount() > 0) {
                        userID = cursorUserId.getString(0);
                        Log.w("UserID",""+userID);
                        setFragment(publicFragment);
                    }
                    else {
                        messageDialogConnect();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Cursor cursorUserId = getUserID();
        cursorUserId.moveToNext();
        Log.w("cursor",""+cursorUserId.getCount());
        if (cursorUserId.getCount() < 1) {
            mMainNav.setSelectedItemId(R.id.nav_machine);
            setFragment(machineFragment);
        }
    }

    private void messageDialogConnect() {
        String message = "Please login.";

        new AlertDialog.Builder(this)
                .setIcon(R.mipmap.baseline_report_black_48dp)
                .setTitle("Login")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this, AccountActivity.class);
                        intent.putExtra("key", "ds"); //Optional parameters
                        MainActivity.this.startActivity(intent);
                        closeKeyboard();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Cancel","Cancel");
                        mMainNav.setSelectedItemId(R.id.nav_machine);
                    }
                })
                .show();
    }


   /* @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
    }
    */

    public void setFragment(Fragment fragment) {
        closeKeyboard();

        fragmentTran = getSupportFragmentManager().beginTransaction();
        fragmentTran.replace(R.id.main_frame,fragment);
        fragmentTran.commit();
    }


    @Override
    public void onClick(View v) {
        if (v == imvAccount){
            //Toast.makeText(MainActivity.this, "Account", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(MainActivity.this, AccountActivity.class);
            intent.putExtra("key", "ds"); //Optional parameters
            MainActivity.this.startActivity(intent);
        }
        closeKeyboard();
    }

    @Override
    public void onBackPressed() {
        machineFragment.onBackPressed();
        publicFragment.onBackPressed();
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(this.getCurrentFocus() != null) {
            inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

    }

    private Cursor getUserID() {
        String sql = "SELECT "+COLUMN_USER_ID+" FROM "+ TABLE_USER_NAME;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }

    public MachineFragment getMachineFragment() {
        return machineFragment;
    }

    public void setMachineFragment(MachineFragment machineFragment) {
        this.machineFragment = machineFragment;
    }
}
