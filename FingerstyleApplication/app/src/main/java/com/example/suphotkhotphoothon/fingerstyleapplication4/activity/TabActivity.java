package com.example.suphotkhotphoothon.fingerstyleapplication4.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.util.Xml;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.NoteListener;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.NoteOnTouchListener;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.RoomOnClickListener;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SeekBarOnClickListener;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SetTimeListener;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.StringListener;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.BackgroundLinePlay;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.BackgroundRoom;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.SilenceDetector;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_PATH;

public class TabActivity extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener,SeekBar.OnSeekBarChangeListener,
        AdapterView.OnItemSelectedListener{

    private SilenceDetector silenceDetector;
    private AudioDispatcher dispatcher;

    private FragmentTransaction fragmentTran;
    public LinearLayout linearBackGroundTab;
    public LinearLayout linearBackgroundSetTime;
    private LinearLayout scrollBackGroundTab;
    private LinearLayout linearBackgroundString[] = new LinearLayout[6];
    private LinearLayout linearBackground[] = new LinearLayout[7];
    private RelativeLayout relativeLayoutBackgroundTime;
    public RelativeLayout relativeLayoutBackgroundSeekBar;
    public RelativeLayout relativeBackgroundSetTime;
    public RelativeLayout relativeBackgroundShowTime;
    public RelativeLayout relativeBackgroundDeleteTime;
    public RelativeLayout relativeBackgroundPlusTime;
    public RelativeLayout relativeBackgroundDelete;
    public RelativeLayout relativeBackgroundModePlay;
    public RelativeLayout relativeBackground[] = new RelativeLayout[7];
    public RelativeLayout relativeBackgroundString[] = new RelativeLayout[6];
    public RelativeLayout relativeBackgroundNumberString[] = new RelativeLayout[6];
    public BackgroundLinePlay backgroundLinePlay[] = new BackgroundLinePlay[11];
    private Spinner spinnerModePlay;
    private TextView timeNumber;
    public TextView textViewShowTime;
    private EditText editTextModePlay;
    private ImageView imageViewMicrophone;
    public ImageView imageViewPlay;
    public ImageView imageViewStop;
    private ImageView imageViewSetting;
    private ImageView imageViewTime;
    public ImageView imageViewDelete;
    private ImageView imageViewString[] = new ImageView[6];
    private HorizontalScrollView scrollViewBackGroundTab;

    private AppCompatImageButton imageButtonFormatTime;
    private AppCompatImageButton imageButtonPlusTime;
    private AppCompatImageButton imageButtonDeleteTime;
    public AppCompatImageView imageViewSetPlusTime;
    public AppCompatImageView imageViewSetDeleteTime;
    public AppCompatImageView imageViewStringNumber[]  = new AppCompatImageView[6];
    SeekBar seekBarFret;

    private RelativeLayout.LayoutParams layoutParamsLineTime;
    private RelativeLayout.LayoutParams layoutParamsTimeNumber;
    private RelativeLayout.LayoutParams layoutParamsNote;

    private Boolean checkImageMicrophone = false;
    private Boolean checkImagePlay = false;
    private Boolean checkRemoveNote = false;
    public Boolean checkSelectionNote = false;
    public Boolean checkPlusTime = false;
    public Boolean checkDeleteTime = false;
    public Boolean checkSetTime = false;
    public String choiceSetTime = "";
    private String fileID;
    private String filePath;
    private String typeTime;
    private int Seconds, Minutes, MilliSeconds ;
    private int maximumFrequency = 22;
    public int distanceSecond = 200;
    private int distanceTempo = 200;
    private int defaultTime = 60000;
    public int timeLinePlay = 0;
    private int timeStart = 0;
    private int timeStop = 0;
    public int indexPlayNote = 0;
    public int indexPlayRoom = 0;
    public int indexStopNote = 0;
    private int widthBackGroundTab;
    private int countNote = 0;
    private int distanceRoom = 0;
    private int heightBackgroundNumber;
    private int bpm;
    private int signature;
    private int longtime;
    private int stringPixelX;
    private int backgroundPixelX;
    private float pitchInHz;
    private float density;
    private double levelAudio;
    private long millisecondTime, startTime, timeBuff, UpdateTime = 0L ;

    private float difference[] = new float[25+maximumFrequency];
    private long timeStamp;

    private ArrayList<String> audioInfo;
    private ArrayList<Float> modelFrequency;
    private ArrayList<String> boxFrequency;
    private ArrayList<String> countData;
    public ArrayList<Note> collectionNote;
    private ArrayList<String> modePlay;
    public ArrayList<ArrayList<Object>> collectionRoom;
    public ArrayList<Note> notes;
    private ArrayAdapter<String> adapterModePlay;

    SQLiteHelper sqLiteHelper;
    GestureDetector gestureDeleteTimeListener;
    SeekBarOnClickListener seekBarOnClickListener;
    NoteOnTouchListener noteOnTouchListener;
    SetTimeListener setTimeListener;
    NoteListener noteListener;
    public Note selectionNote[] = new Note[1];
    StringListener stringListener;
    MediaPlayer mediaPlayerString1;
    MediaPlayer mediaPlayerString2;
    MediaPlayer mediaPlayerString3;
    MediaPlayer mediaPlayerString4;
    MediaPlayer mediaPlayerString5;
    MediaPlayer mediaPlayerString6;

    private SoundPool soundPool;
    int sound[][] = new int[6][23];


    private Handler handler;


//    Error longtime = "" (/)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        fragmentTran = this.getSupportFragmentManager().beginTransaction();
        init();
//        createViewTimeAndString(60L);

//        double threshold = 0;
//        dispatcher = AudioDispatcherFactory.fromDefaultMicrophone( 44100, AudioRecord.getMinBufferSize
//                (44100, AudioFormat.CHANNEL_IN_MONO,AudioFormat.ENCODING_PCM_16BIT),0);
//        silenceDetector = new SilenceDetector(threshold,false);
//        dispatcher.addAudioProcessor(silenceDetector);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        standardFrequency();
//        detector();
//
//        new Thread(dispatcher,"Audio Dispatcher").start();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                fileID = null;
            } else {
                fileID = extras.getString("file_id");
            }
        } else {
            fileID = (String) savedInstanceState.getSerializable("file_id");
        }
        if(fileID != null){
            getDataTab();
//            longtime = setSecond(longtime);
//            createTab();
//            Log.w("Long Time",""+longtime);

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    //    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//        int width  = relativeBackgroundNumberString[0].getWidth();
//        int height = relativeBackgroundNumberString[0].getHeight();
//        Log.w("BackgroundNumberString","Height:"+height);
//    }

    private void init() {
        imageViewMicrophone = (ImageView)findViewById(R.id.fragmentCreateTab_image_microphone);
        imageViewPlay = (ImageView)findViewById(R.id.fragmentCreateTab_image_play);
        imageViewStop = (ImageView)findViewById(R.id.fragmentCreateTab_image_stop);
        imageViewSetting = (ImageView)findViewById(R.id.fragmentCreateTab_image_setting);
        scrollViewBackGroundTab = (HorizontalScrollView)findViewById(R.id.fragmentCreateTab_scrollView_backgroundTab);
        linearBackGroundTab = (LinearLayout)findViewById(R.id.fragmentCreateTab_ll_backgroundTab);
        linearBackgroundSetTime = (LinearLayout)findViewById(R.id.fragmentCreateTab_ll_backgroundSetTime);
        relativeLayoutBackgroundSeekBar = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundSeekBar);
        relativeBackgroundSetTime = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundSetTime);
        relativeBackgroundShowTime = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundShowTime);
        relativeBackgroundDeleteTime = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundDeleteTime);
        relativeBackgroundPlusTime = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundPlusTime);
        relativeBackgroundDelete = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundDelete);
        relativeBackgroundModePlay = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundModePlay);
        editTextModePlay = (EditText)findViewById(R.id.fragmentCreateTab_editText_ModePlay);
        relativeBackgroundNumberString[0] = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundNumberString1);
        relativeBackgroundNumberString[1] = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundNumberString2);
        relativeBackgroundNumberString[2] = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundNumberString3);
        relativeBackgroundNumberString[3] = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundNumberString4);
        relativeBackgroundNumberString[4] = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundNumberString5);
        relativeBackgroundNumberString[5] = (RelativeLayout)findViewById(R.id.fragmentCreateTab_rl_backgroundNumberString6);
        spinnerModePlay = (Spinner)findViewById(R.id.fragmentCreateTab_spinner_modePlay);
        imageButtonFormatTime = (AppCompatImageButton)findViewById(R.id.fragmentCreateTab_image_clock);
        imageButtonPlusTime = (AppCompatImageButton)findViewById(R.id.fragmentCreateTab_image_plusTime);
        imageButtonDeleteTime = (AppCompatImageButton)findViewById(R.id.fragmentCreateTab_image_deleteTime);
        imageViewStringNumber[0] = (AppCompatImageView)findViewById(R.id.fragmentCreateTab_image_StringNumber1);
        imageViewStringNumber[1] = (AppCompatImageView)findViewById(R.id.fragmentCreateTab_image_StringNumber2);
        imageViewStringNumber[2] = (AppCompatImageView)findViewById(R.id.fragmentCreateTab_image_StringNumber3);
        imageViewStringNumber[3] = (AppCompatImageView)findViewById(R.id.fragmentCreateTab_image_StringNumber4);
        imageViewStringNumber[4] = (AppCompatImageView)findViewById(R.id.fragmentCreateTab_image_StringNumber5);
        imageViewStringNumber[5] = (AppCompatImageView)findViewById(R.id.fragmentCreateTab_image_StringNumber6);

        for(int i=0;i<backgroundLinePlay.length;i++){
            backgroundLinePlay[i] = new BackgroundLinePlay(this);
        }

        sqLiteHelper = new SQLiteHelper(this);
        handler = new Handler() ;
        audioInfo = new ArrayList<String>();
        modelFrequency = new ArrayList<Float>();
        boxFrequency = new ArrayList<String>();
        countData = new ArrayList<String>();
//        resultNote = new ArrayList<String>();
        modePlay = new ArrayList<String>();

        modePlay.add("All");
        modePlay.add("Room");
        modePlay.add("Custom");

        notes = new ArrayList<Note>();
        collectionRoom  = new  ArrayList<ArrayList<Object>>();
        collectionNote = new ArrayList<Note>();
        adapterModePlay = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, modePlay);
        textViewShowTime = new TextView(this);
        imageViewSetDeleteTime = new AppCompatImageView(this);
        imageViewSetPlusTime = new AppCompatImageView(this);
        imageViewDelete = new ImageView(this);
        setTimeListener = new SetTimeListener(this);

        imageViewSetDeleteTime.setImageResource(R.mipmap.baseline_remove_black_18dp);
        imageViewSetPlusTime.setImageResource(R.mipmap.baseline_add_black_18dp);
        imageViewDelete.setImageResource(R.mipmap.baseline_delete_black_18dp);
        imageViewDelete.setColorFilter(this.getResources().getColor(R.color.colorWhite));
        textViewShowTime.setTextSize(18);
        textViewShowTime.setLayoutParams(layoutParamsRelativeLayoutShowTime());
        spinnerModePlay.setAdapter(adapterModePlay);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
//        Log.e("height", "" + height);
        widthBackGroundTab = width-(dpToPx(35+60));
        Log.w("Width Back Ground Tab",""+widthBackGroundTab);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(6)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            soundPool = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        }

        Resources res = getBaseContext().getResources();
        int rawId;
        for(int i=0;i<6;i++){
            for(int j=0;j<23;j++){
                if(i == 0) {
                    rawId = res.getIdentifier("one_" + j, "raw", getBaseContext().getPackageName());
                    sound[i][j] = soundPool.load(this, rawId, 1);
                }
                if(i == 1) {
                    rawId = res.getIdentifier("two_" + j, "raw", getBaseContext().getPackageName());
                    sound[i][j] = soundPool.load(this, rawId, 1);
                }
                if(i == 2) {
                    rawId = res.getIdentifier("three_" + j, "raw", getBaseContext().getPackageName());
                    sound[i][j] = soundPool.load(this, rawId, 1);
                }
                if(i == 3) {
                    rawId = res.getIdentifier("four_" + j, "raw", getBaseContext().getPackageName());
                    sound[i][j] = soundPool.load(this, rawId, 1);
                }
                if(i == 4) {
                    rawId = res.getIdentifier("five_" + j, "raw", getBaseContext().getPackageName());
                    sound[i][j] = soundPool.load(this, rawId, 1);
                }
                if(i == 5) {
                    rawId = res.getIdentifier("six_" + j, "raw", getBaseContext().getPackageName());
                    sound[i][j] = soundPool.load(this, rawId, 1);
                }
            }
        }


        for(int i=0;i<imageViewStringNumber.length;i++){
            stringListener = new StringListener(this,i+1);
            imageViewStringNumber[i].setOnClickListener(stringListener);
        }

        imageViewMicrophone.setOnClickListener(this);
        imageViewPlay.setOnClickListener(this);
        imageViewStop.setOnClickListener(this);
        imageViewSetting.setOnClickListener(this);
        imageButtonFormatTime.setOnClickListener(this);
        imageButtonPlusTime.setOnClickListener(this);
        imageButtonDeleteTime.setOnClickListener(this);
        imageViewDelete.setOnClickListener(this);
        imageViewSetDeleteTime.setOnTouchListener(setTimeListener);
        imageViewSetPlusTime.setOnTouchListener(setTimeListener);
        spinnerModePlay.setOnItemSelectedListener(this);


        relativeBackgroundDeleteTime.addView(imageViewSetDeleteTime);
        relativeBackgroundShowTime.addView(textViewShowTime);
        relativeBackgroundPlusTime.addView(imageViewSetPlusTime);
        relativeBackgroundDelete.addView(imageViewDelete);

        imageViewDelete.setVisibility(View.INVISIBLE);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        soundPool.release();
        soundPool = null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            try {
                dispatcher.stop();
            }catch (Exception ex){
                ex.printStackTrace();
            }

            stopLinePlay();

            Collections.sort(notes);
            String path = createXML();
            setUpdatePath(path);
            setUpdateLongtime(getLongtime());

            Intent intent = new Intent(this, MainActivity.class);
//        intent.putExtra("key", "ds"); //Optional parameters
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {

        if(v == imageViewMicrophone){
            checkImageMicrophone = !checkImageMicrophone;
            checkImagePlay = false;
            if (checkImageMicrophone) {

                double threshold = 0;
                dispatcher = AudioDispatcherFactory.fromDefaultMicrophone( 44100, AudioRecord.getMinBufferSize
                        (44100, AudioFormat.CHANNEL_IN_MONO,AudioFormat.ENCODING_PCM_16BIT),0);
                silenceDetector = new SilenceDetector(threshold,false);
                dispatcher.addAudioProcessor(silenceDetector);
                detector();
                new Thread(dispatcher,"Audio Dispatcher").start();

                imageViewMicrophone.setColorFilter(this.getResources().getColor(R.color.colorRed));
                imageViewPlay.setColorFilter(this.getResources().getColor(R.color.colorWhite));
                imageViewStop.setColorFilter(this.getResources().getColor(R.color.colorWhite));
                startTime = SystemClock.uptimeMillis();
                handler.postDelayed(runnable, 0);

            }
            else {
                dispatcher.stop();
                imageViewMicrophone.setColorFilter(this.getResources().getColor(R.color.colorWhite));
                timeBuff += millisecondTime;
                checkRemoveNote = true;
                handler.removeCallbacks(runnable);
                saveFileXML();
            }
        }
        else if(v == imageViewPlay && !checkImageMicrophone){
            Collections.sort(notes);
            checkImagePlay = true;
            imageViewPlay.setColorFilter(this.getResources().getColor(R.color.colorRed));
            imageViewStop.setColorFilter(this.getResources().getColor(R.color.colorWhite));
            startTime = SystemClock.uptimeMillis();
//            Log.i("Start Time",""+timeBuff);
            countNote = 0;
            if (editTextModePlay.getText().toString().equals("All")) {
                for (int i = 0; i < notes.size(); i++) {
                    if (notes.get(i).getRoom() == false) {
                        if (notes.get(i).getTime() >= timeBuff) {
                            indexPlayNote = i;
                            break;
                        }
                    }
                }
            }
            else if (editTextModePlay.getText().toString().equals("Room")) {
                indexPlayNote = indexPlayRoom;
            }
            else{
                for (int i = 0; i < notes.size(); i++) {
                    if (notes.get(i).getRoom() == false) {
                        if (notes.get(i).getTime() >= timeBuff) {
                            indexPlayNote = i;
                            break;
                        }
                    }
                }
            }
//            Log.i("Index Play Note Time",""+indexPlayNote);
//            Log.i("Note"+ indexPlayNote, " S:" + notes.get(indexPlayNote).getString()
//                        + " F:" + notes.get(indexPlayNote).getFret()
//                        + " T:" + notes.get(indexPlayNote).getTime()
//                        + " R:" + notes.get(indexPlayNote).getRoom());
            handler.postDelayed(runnable, 0);
        }
        else if(v == imageViewStop && !checkImageMicrophone && checkImagePlay){

//            if (editTextModePlay.getText().toString().equals("All")) {
                stopLinePlay();
//            }
//            else if (editTextModePlay.getText().toString().equals("Room")) {
//                checkImagePlay = false;
//                imageViewStop.setColorFilter(getBaseContext().getResources().getColor(R.color.colorRed));
//                imageViewPlay.setColorFilter(getBaseContext().getResources().getColor(R.color.colorWhite));
//                handler.removeCallbacks(runnable);
//            }
        }
        if(v == imageViewSetting){
            Log.i("Setting","Setting");
//            Log.i("Get X",""+scrollViewBackGroundTab.getX());
//            Log.i("Get Width",""+scrollViewBackGroundTab.getWidth());
//            Log.i("Get Scale X",""+scrollViewBackGroundTab.getScaleX());
//            Log.i("Get Px",""+scrollViewBackGroundTab.getWidth());
//            Log.i("Get ScrollX",""+scrollViewBackGroundTab.getScrollX());
//            Log.i("Get ",""+scrollViewBackGroundTab.getScrollIndicators());
//            Log.i("Get Position",""+relativeBackgroundSetTime.get);

            try {
                dispatcher.stop();
            }catch (Exception ex){
                ex.printStackTrace();
            }

            Toast.makeText(TabActivity.this,"Back",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(TabActivity.this, EditProjectActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("file_id", fileID);
            startActivity(intent);
//            finish();
        }
        else if(v == imageButtonFormatTime){
            linearBackGroundTab.removeAllViews();
            for(int i=0;i<relativeBackgroundString.length;i++){
                relativeBackgroundString[i].removeAllViews();
            }
            for(int i=0;i<relativeBackground.length;i++){
                relativeBackground[i].removeAllViews();
            }
            distanceSecond = setDistanceSecond(distanceSecond);
            createTab();
        }
        else if(v == imageButtonPlusTime){
            Collections.sort(notes);
            for(int j=notes.size()-1;j>=0;j--){
                if(notes.get(j).getRoom()==true){
                    while (true){
                        longtime++;
                        createLineTime(longtime);
                        if(longtime*1000 >= notes.get(j).getTime()+distanceRoom){
                            break;
                        }
                    }
//                            Log.i("Room","T1 "+(longtime*1000)+" >= T2 "+(notes.get(j).getTime()+distanceRoom));
                    Note note = new Note(this,0,0,notes.get(j).getTime()+distanceRoom,true);
                    notes.add(note);
                    addLinePlayOnTab(note);
                    for (int k=0;k<backgroundLinePlay.length;k++){
                        backgroundLinePlay[k].bringToFront();
                    }
                    break;
                }
            }
            scrollViewBackGroundTab.post(new Runnable() {
                public void run() {
                    scrollViewBackGroundTab.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }
            });
        }
        else if(v == imageButtonDeleteTime){
            Collections.sort(notes);
//            relativeLayoutBackgroundTime.removeAllViews();
//            longtime --;
//            for(int i=notes.size()-1;i>=0;i--){
//                if(notes.get(i).getTime()>longtime*1000){
//                    notes.remove(i);
//                }
//                else {
//                    break;
//                }
//            }

            notes.remove(notes.size()-1);
            for(int i=notes.size()-1;i>=0;i--){
                if(notes.get(i).getRoom()==true){
                    longtime = (int) Math.ceil(notes.get(i).getTime()/1000);
                    break;
                }
                notes.remove(i);
            }
            for (int k=0;k<backgroundLinePlay.length;k++){
                backgroundLinePlay[k].bringToFront();
            }

//            for(int i=0;i<=longtime;i++) {
//                createLineTime(i);
//            }
            linearBackGroundTab.removeAllViews();
            for(int i=0;i<relativeBackgroundString.length;i++){
                relativeBackgroundString[i].removeAllViews();
            }
            for(int i=0;i<relativeBackground.length;i++){
                relativeBackground[i].removeAllViews();
            }
            createTab();
            scrollViewBackGroundTab.post(new Runnable() {
                public void run() {
                    scrollViewBackGroundTab.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }
            });
        }
        else if(v == imageViewDelete){
            if(checkSelectionNote){
                for(int j=0;j<collectionNote.size();j++){
                    notes.remove(collectionNote.get(j));
                }
            }
            else {
                if (selectionNote[0] != null) {
                    Log.w("Note", " S:" + selectionNote[0].getString()
                            + " F:" + selectionNote[0].getFret() + " T:" + selectionNote[0].getTime() + " R:" + selectionNote[0].getRoom());
//                int numberSting = selectionNote[0].getString();
                    notes.remove(selectionNote[0]);
                }
            }
            linearBackGroundTab.removeAllViews();
            for (int i = 0; i < relativeBackgroundString.length; i++) {
                relativeBackgroundString[i].removeAllViews();
            }
            for (int i = 0; i < relativeBackground.length; i++) {
                relativeBackground[i].removeAllViews();
            }
            createTab();
            selectionNote[0] = null;
            textViewShowTime.setText("");
            collectionNote.clear();
            relativeLayoutBackgroundSeekBar.removeAllViews();
            imageViewDelete.setVisibility(View.INVISIBLE);
            checkSelectionNote = false;
            saveFileXML();
        }
        for(int i=0;i<relativeBackgroundString.length;i++){
            if(v == relativeBackgroundString[i]){
//                Log.w("String",""+(i+1));
//                Log.w("String PxX",""+ stringPixelX);
                int time = stringPixelX*(1000/distanceSecond);
                int string = i+1;

                if(checkSelectionNote){
                    Collections.sort(collectionNote);
                    Log.w("Time",""+time);
                    for(int j=collectionNote.size()-1;j>=0;j--){
                        if(j>0){
                            Log.i("Note"+j, "T1:"+collectionNote.get(j).getTime()+" - T2:"+collectionNote.get(0).getTime()+
                                    " = "+(collectionNote.get(j).getTime()- collectionNote.get(0).getTime()));
                            collectionNote.get(j).setTime(collectionNote.get(j).getTime()- collectionNote.get(0).getTime());
                        }
                    }
                    for(int j=0;j<collectionNote.size();j++){
                        if(j==0){
                            collectionNote.get(j).setTime(time);
                            Log.i("Note"+j, "T1:"+collectionNote.get(j).getTime());
                        }
                        else {
                            collectionNote.get(j).setTime(collectionNote.get(j).getTime()+time);
                        }
                    }
                    if(collectionNote.size()>0){
                        setTextViewShowTime(collectionNote.get(0).getTime());
                    }

//                    long UpdateTime = collectionNote.get(0).getTime();
//                    long timeStamp = UpdateTime;
//                    long Seconds = (int) (UpdateTime / 1000);
//                    long Minutes = Seconds / 60;
//                    Seconds = Seconds % 60;
//                    int MilliSeconds = (int) (UpdateTime % 1000);
//                    textViewShowTime.setText("" +String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds) + ":" + String.format("%03d", MilliSeconds));

                    for(int j=0;j<collectionNote.size();j++){
                        int height = getHeightTeb(collectionNote.get(j),relativeBackgroundString[0]);
                        int width = height;
                        RelativeLayout.LayoutParams relativeNote = layoutParamsRelativeLayoutNote(width, height, millisecondToPx(collectionNote.get(j).getTime()));
                        collectionNote.get(j).setLayoutParams(relativeNote);
                    }
                }
                else {
//                    collectionNote.clear();
//                    checkSelectionNote = false;
                    setDefaultColorRoom();

                    seekBarFret = new SeekBar(this);
                    seekBarFret.setMax(22);
//                Log.w("stringPixelX"," stringPixelX: "+stringPixelX);
                    Log.w("note",string+" : "+0+" : "+time);
                    notes.add(new Note(this,string,0,time,false));
                    int indexNew = notes.size() - 1;
                    setTextViewShowTime(notes.get(indexNew).getTime());
                    for (int j=0;j<notes.size();j++){
                        if(notes.get(j).getRoom() == false && notes.get(j) != notes.get(indexNew)){
                            notes.get(j).setBackgroundResource(0);
                        }
                    }

                    noteListener = new NoteListener(this, notes.get(indexNew), seekBarFret);
                    notes.get(indexNew).setOnClickListener(noteListener);
                    notes.get(indexNew).setOnLongClickListener(noteListener);
                    selectionNote[0] = notes.get(indexNew);
//                noteOnTouchListener = new NoteOnTouchListener(this, notes.get(indexNew), seekBarFret);
//                notes.get(indexNew).setOnTouchListener(noteOnTouchListener);

                    seekBarOnClickListener = new SeekBarOnClickListener(this, notes.get(indexNew), seekBarFret);
                    seekBarFret.setOnSeekBarChangeListener(seekBarOnClickListener);

                    notes.get(indexNew).setBackgroundResource(R.color.colorGreen);
                    imageViewStringNumber[string-1].setBackgroundResource(R.color.colorGreen);

                    for(int j=0;j<imageViewStringNumber.length;j++){
                        if(j!=string-1){
                            imageViewStringNumber[j].setBackgroundResource(0);
                        }
                    }

                    addNoteOnString(notes.get(indexNew));


                    for(int j=notes.size()-1;j>=0;j--){
                        if(notes.get(j).getRoom()==true){
                            if(notes.get(indexNew).getTime()>notes.get(j).getTime()) {
                                while (true){
                                    longtime++;
                                    createLineTime(longtime);
                                    if(longtime*1000 >= notes.get(j).getTime()+distanceRoom){
                                        break;
                                    }
                                }
//                                Log.i("Room", "T1 " + (longtime * 1000) + " >= T2 " + (notes.get(j).getTime() + distanceRoom));
//                            Log.i("Note"+i,"S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());

                                Note note = new Note(this, 0, 0, notes.get(j).getTime() + distanceRoom, true);
                                notes.add(note);
                                addLinePlayOnTab(note);
                                for (int k = 0; k < backgroundLinePlay.length; k++) {
                                    backgroundLinePlay[k].bringToFront();
                                }
                            }
                            break;
                        }

                    }

                    RelativeLayout.LayoutParams relativeLayoutSeekBar = layoutParamsRelativeLayoutSeekBar(seekBarFret);
                    relativeLayoutBackgroundSeekBar.removeAllViews();
                    relativeLayoutBackgroundSeekBar.addView(seekBarFret, relativeLayoutSeekBar);
                    imageViewDelete.setVisibility(View.VISIBLE);

                    saveFileXML();
                }
            }
        }
        for(int i=0;i<relativeBackground.length;i++){
            if(v == relativeBackground[i]){
                selectionNote[0] = null;
                collectionNote.clear();
                relativeLayoutBackgroundSeekBar.removeAllViews();
                imageViewDelete.setVisibility(View.INVISIBLE);
                checkSelectionNote = false;

                Log.w("Background",""+(i));
                Log.w("Background PxX",""+backgroundPixelX);
                timeLinePlay  = backgroundPixelX*(1000/distanceSecond);
                timeBuff = timeLinePlay;

                for(int j=0;j<notes.size();j++){
                    if (notes.get(j).getTime() >= timeBuff) {
                        indexPlayNote = j;
                        for(int k=indexPlayNote-1;k>=0;k--){
                            if (notes.get(k).getRoom() == true) {
                                indexPlayRoom = k;
                                break;
                            }
                        }
                        break;
                    }
                }

                countNote = 0;

                if (editTextModePlay.getText().toString().equals("Room")) {
                    indexPlayNote = indexPlayRoom;
                    timeLinePlay = notes.get(indexPlayRoom).getTime();
                    timeBuff = timeLinePlay;
                }

                setTextViewShowTime(timeBuff);
                handler.removeCallbacks(runnable);
                startTime = SystemClock.uptimeMillis();

                Log.i("check",checkImageMicrophone +" "+checkImagePlay);
                if(checkImageMicrophone || checkImagePlay){
                    handler.postDelayed(runnable, 0);
                }
                setTimeLinePlayOnTab(millisecondToPx(timeLinePlay));

                for(int j=0;j<imageViewStringNumber.length;j++){
                    imageViewStringNumber[j].setBackgroundResource(0);
                }
                for (int j=0;j<notes.size();j++){
                    notes.get(j).setBackgroundResource(0);
                }
            }
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        for(int i=0;i<relativeBackgroundString.length;i++){
            if(v == relativeBackgroundString[i]){
//                Log.w("String",""+(i+1));
//                Log.w("String PX Width",""+event.getX());
                stringPixelX = (int)event.getX();
            }
        }
        for(int i=0;i<relativeBackground.length;i++){
            if(v == relativeBackground[i]){
//                Log.w("Background PX Width",""+event.getX());
                backgroundPixelX = (int)event.getX();
            }
        }
        return false;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        Log.w("Seek Bar",""+progress);
        seekBar.setThumb(getThumb(progress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.i("Mode Play",""+modePlay.get(position));
        Log.i("Mode Play",""+(""+modePlay.get(position).equals("Custom")));
        if(modePlay.get(position).equals("Custom")){
            editTextModePlay.setEnabled(true);
            editTextModePlay.setText("");
        }
        else {
            editTextModePlay.setEnabled(false);
            editTextModePlay.setMaxLines(modePlay.get(position).length());
            editTextModePlay.setText(""+modePlay.get(position));
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void saveFileXML(){
        Collections.sort(notes);
        String path = createXML();
        setUpdatePath(path);
        setUpdateLongtime(getLongtime());
    }

    private String getLongtime() {
        String longtime = "";
        /*Boolean checkNote = false;
        for(int i=notes.size()-1;i>=0;i--){
//            Log.w("Room"+i," S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
            if(notes.get(i).getRoom() == false){
                longtime = ""+notes.get(i).getTime();
                checkNote = true;
                break;
            }
        }
        if(checkNote == false){
            longtime = ""+defaultTime;
        }*/
        longtime = ""+notes.get(notes.size()-1).getTime();
        return longtime;
    }

    private void getDataTab() {
        Cursor cursor = getDatabase();
        cursor.moveToNext();
        Log.w("bpm",""+cursor.getString(3));
        Log.w("sig",""+cursor.getString(4));
        Log.w("long time",""+cursor.getString(5));
        Log.w("path",""+cursor.getString(7));
        bpm = Integer.parseInt(cursor.getString(3));
        String splitSignature[] =  cursor.getString(4).split("/");
        signature = Integer.parseInt(splitSignature[0]);
        String millisecondLongtime = cursor.getString(5);
        filePath = cursor.getString(7);
        if(millisecondLongtime == null){
            longtime = setSecond(defaultTime);
            distanceTempo = setMillisecondTempo(bpm);
            Log.w("Distance Tempo","T:"+distanceTempo);
            distanceRoom = distanceTempo*signature;
            for(int time =0;time<=defaultTime;time+=distanceRoom){
                int px = millisecondToPx(time);
                notes.add(new Note(this,0,0,time,true));
            }
        }
        else{
            longtime = setSecond(Integer.parseInt(millisecondLongtime)); //Error longtime = "" (/)
            distanceTempo = setMillisecondTempo(bpm);
            Log.w("Distance Tempo","T:"+distanceTempo);
            distanceRoom = distanceTempo*signature;
            if(longtime<60){
                longtime = setSecond(defaultTime);
            }
            try {
                FileInputStream fis = new FileInputStream (new File(filePath));
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fis);
                Element element = doc.getDocumentElement();
                element.normalize();

                NodeList nList = doc.getElementsByTagName("Note");

                for (int i=0; i<nList.getLength(); i++) {

                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element2 = (Element) node;
//                        Log.i("Tag:",""+element.getElementsByTagName("String").item(0).getNodeName());
//                        Log.i("String:",getValue("String", element2));
//                        Log.i("Fret:",getValue("Fret", element2));
//                        Log.i("Time:",getValue("Time", element2));
//                        Log.i("Time:",getValue("Room", element2));
//                        Log.i("Line:","-----------------------");
                        notes.add(new Note(this,
                                Integer.parseInt(getValue("String", element2)),
                                Integer.parseInt(getValue("Fret", element2)),
                                Integer.parseInt(getValue("Time", element2)),
                                Boolean.parseBoolean(getValue("Room", element2))));
                    }
                }
                fis.close();
            } catch (Exception e) {e.printStackTrace();}

            Log.w("Distance Tempo","T:"+distanceTempo);
            Collections.sort(notes);
            distanceRoom = distanceTempo*signature;
            int time = 0;
            for(int j=notes.size()-1;j>=0;j--){
                if (notes.get(j).getRoom() == true) {
                    time = notes.get(j).getTime();
                    Log.w("Time Room Last", "T:" + time);
                    break;
                }
            }
            if(time<Integer.parseInt(millisecondLongtime)) {
                for (; time <= Integer.parseInt(millisecondLongtime)+distanceRoom; time += distanceRoom) {
                    int px = millisecondToPx(time);
//                    Log.w("Time Room2", "T:" + time);
                    notes.add(new Note(this, 0, 0, time, true));
                }
            }
        }
        getHeightAndCreateTeb();
    }

    private void getHeightAndCreateTeb() {
        ViewTreeObserver viewTreeObserver = relativeBackgroundNumberString[0].getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                relativeBackgroundNumberString[0].getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int width  = relativeBackgroundNumberString[0].getMeasuredWidth();
                int height = relativeBackgroundNumberString[0].getMeasuredHeight();
//                Log.w("BackgroundNumberString","Height:"+height);
                heightBackgroundNumber = height;
                Collections.sort(notes);
                createTab();
            }
        });
    }

    private String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }

    private Drawable getThumb(int progress) {
        TextView textView = new TextView(getBaseContext());
        textView.setText(""+progress);
        textView.setTextSize(16);

        textView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(textView.getMeasuredWidth(), textView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        textView.layout(0, 0, textView.getMeasuredWidth(), textView.getMeasuredHeight());
        textView.draw(canvas);

        return new BitmapDrawable(getResources(), bitmap);
    }


    private int setSecond(int milliSeconds) {
        return (int) Math.ceil(milliSeconds/1000.f);
    }
    private int setMillisecond(int seconds) {
        return (int) Math.ceil(seconds*1000.f);
    }
    private int setMillisecondTempo(int bpm) {
        return Math.round(defaultTime/bpm);
    }
    public int setDistanceSecond(int distanceSecond) {
        int distance = 0;
        if(distanceSecond == 200){
            distance = 500;
        }
        else if(distanceSecond == 500){
            distance = 200;
        }
        return distance;
    }
    public void setDefaultColorRoom() {
        for (int j = 0; j < collectionRoom.size(); j++) {
            for (int k = 0; k < collectionRoom.get(j).size(); k++) {
                BackgroundRoom backgroundRoomItem = (BackgroundRoom) collectionRoom.get(j).get(k);
                backgroundRoomItem.getImageViewRoom().setBackgroundResource(R.color.colorBlack);
            }
        }
    }
    public void setTextViewShowTime(long time){
        long UpdateTime = time;
        long timeStamp = UpdateTime;
        long Seconds = (int) (UpdateTime / 1000);
        long Minutes = Seconds / 60;
        Seconds = Seconds % 60;
        int MilliSeconds = (int) (UpdateTime % 1000);
        textViewShowTime.setText("" +String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds) + ":" + String.format("%03d", MilliSeconds));

    }

    public void createTab() {
        relativeLayoutBackgroundTime = new RelativeLayout(this);
        relativeLayoutBackgroundTime.setBackgroundColor(this.getResources().getColor(R.color.colorGray));
        RelativeLayout.LayoutParams relativeTime = layoutParamsRelativeTime(relativeLayoutBackgroundTime);
        linearBackGroundTab.addView(relativeLayoutBackgroundTime,relativeTime);
        for(int i=0;i<=longtime;i++){
            createLineTime(i);
        }

        for (int i=0;i<=linearBackgroundString.length;i++) {
            if(i<linearBackgroundString.length) {
                linearBackgroundString[i] = new LinearLayout(this);
                linearBackground[i] = new LinearLayout(this);
                relativeBackground[i] = new RelativeLayout(this);
                relativeBackgroundString[i] = new RelativeLayout(this);
                imageViewString[i] = new ImageView(this);

                imageViewString[i].setBackgroundColor(this.getResources().getColor(R.color.colorGray));

                LinearLayout.LayoutParams linearLayoutString = layoutParamsLinearLayoutString(linearBackgroundString[i]);
                LinearLayout.LayoutParams linearLayoutBackground = layoutParamsLinearBackground(linearBackground[i]);
                RelativeLayout.LayoutParams relativeLayoutBackground = layoutParamsRelativeBackground(relativeBackground[i]);
                RelativeLayout.LayoutParams relativeLayoutString = layoutParamsRelativeLayoutString(relativeBackgroundString[i]);
                RelativeLayout.LayoutParams layoutParamsString = layoutParamsString(imageViewString[i]);

                relativeBackgroundString[i].addView(imageViewString[i], layoutParamsString);
                linearBackgroundString[i].addView(relativeBackgroundString[i], relativeLayoutString);
                linearBackground[i].addView(relativeBackground[i],relativeLayoutBackground);
                linearBackGroundTab.addView(linearBackground[i], linearLayoutBackground);
                linearBackGroundTab.addView(linearBackgroundString[i], linearLayoutString);

                relativeBackgroundString[i].setOnTouchListener(this);
                relativeBackgroundString[i].setOnClickListener(this);
                relativeBackground[i].setOnTouchListener(this);
                relativeBackground[i].setOnClickListener(this);
            }
            else{
                linearBackground[i] = new LinearLayout(this);
                relativeBackground[i] = new RelativeLayout(this);
                LinearLayout.LayoutParams linearLayoutBackground = layoutParamsLinearBackground(linearBackground[i]);
                RelativeLayout.LayoutParams relativeLayoutBackground = layoutParamsRelativeBackground(relativeBackground[i]);
                linearBackground[i].addView(relativeBackground[i],relativeLayoutBackground);
                linearBackGroundTab.addView(linearBackground[i], linearLayoutBackground);

                relativeBackground[i].setOnTouchListener(this);
                relativeBackground[i].setOnClickListener(this);
            }
        }
        if(notes!=null){

            for (int i=0;i<notes.size();i++){
                if(i>0 && notes.get(i).getRoom() == true){
//                    Log.w("Room"+i," S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
//                    int px = millisecondToPx(i);
//                    Log.w("Distance Room","T:"+i+" TToPX:"+px);
                    addLinePlayOnTab(notes.get(i));
//                    Log.i("Room",""+(collectionRoom.get(0).get(0).equals(notes.get(4))));
//                    for (int j=0;j<collectionRoom.get(collectionRoom.size()-1).size();j++){
//                        Log.i("Room",""+(collectionRoom.get(collectionRoom.size()-1).get(1)));
//                    }
//                    Log.i("Line","------------------------------------------------");
                }
            }
            for (int i=0;i<notes.size();i++){
                 if(notes.get(i).getRoom() == false){
                     seekBarFret = new SeekBar(this);
                     seekBarFret.setMax(22);
                     noteListener = new NoteListener(this, notes.get(i), seekBarFret);
                     notes.get(i).setOnClickListener(noteListener);
                     notes.get(i).setOnLongClickListener(noteListener);
//                     notes.get(i).setOnTouchListener(new NoteOnTouchListener(this, notes.get(i), seekBarFret));
                     notes.get(i).setBackgroundResource(0);
                     addNoteOnString(notes.get(i));
                }
            }
        }
        else{

        }
        addLinePlayOnTab(millisecondToPx(timeLinePlay));
        if(collectionNote.size()>0){
            for(int j=0;j<collectionNote.size();j++) {
                collectionNote.get(j).setBackgroundResource(R.color.colorYellow);
            }
        }
    }
    public void addLinePlayOnTab(int timeLinePlay) {
        int indexLinePlay = 0;
        int height = getHeightTeb(null,relativeBackgroundString[0]);
        int width = height;

        for(int i=0;i<relativeBackgroundString.length;i++){
            RelativeLayout.LayoutParams relativeLinePlay = layoutParamsRelativeLayoutLinePlay(width,height, timeLinePlay);
            relativeBackgroundString[i].addView(backgroundLinePlay[indexLinePlay], relativeLinePlay);
            indexLinePlay++;
        }
        for(int i=0;i<relativeBackground.length;i++){
            if(i>0&&i<6){
                RelativeLayout.LayoutParams relativeLinePlay = layoutParamsRelativeLayoutLinePlay(width,height, timeLinePlay);
                relativeBackground[i].addView(backgroundLinePlay[indexLinePlay], relativeLinePlay);
                indexLinePlay++;
            }
        }

    }
    public void addLinePlayOnTab(Note note) {
        ArrayList<Object> itemRoom = new ArrayList();
        BackgroundRoom backgroundRoom;
        RoomOnClickListener roomOnClickListener;
        int height = getHeightTeb(note,relativeBackground[0]);
        int width = height;
        for(int i=0;i<relativeBackgroundString.length;i++){
            backgroundRoom = new BackgroundRoom(this,note);
            roomOnClickListener = new RoomOnClickListener(this,backgroundRoom);
            backgroundRoom.setOnClickListener(roomOnClickListener);
            itemRoom.add(backgroundRoom);

            RelativeLayout.LayoutParams relativeRoom =  null;
            if(i==0){
                relativeRoom = layoutParamsRelativeLayoutRoomBottom(width,height, millisecondToPx(note.getTime()));
            }
            else if(i==5){
                relativeRoom = layoutParamsRelativeLayoutRoomTop(width,height, millisecondToPx(note.getTime()));
            }
            else {
                relativeRoom = layoutParamsRelativeLayoutRoom(width,height, millisecondToPx(note.getTime()));
            }
            relativeBackgroundString[i].addView(backgroundRoom, relativeRoom);
        }
        for(int i=0;i<relativeBackground.length;i++){
            if(i>0&&i<6){
                backgroundRoom = new BackgroundRoom(this,note);
                roomOnClickListener = new RoomOnClickListener(this,backgroundRoom);
                backgroundRoom.setOnClickListener(roomOnClickListener);
                itemRoom.add(backgroundRoom);

                RelativeLayout.LayoutParams relativeRoom = layoutParamsRelativeLayoutRoom(width,height, millisecondToPx(note.getTime()));
                relativeBackground[i].addView(backgroundRoom, relativeRoom);
            }
        }
        collectionRoom.add(itemRoom);
    }
    public void addNoteOnString(Note note) {
        int height = getHeightTeb(note,relativeBackgroundString[0]);
        int width = height;
        for(int i=0;i<relativeBackgroundString.length;i++){
//            Log.w("id rela","["+i+"]"+relativeBackgroundString[i].getClass().getName());
            if(note.getString() == i+1){
                Log.i("millisecondToPx",""+millisecondToPx(note.getTime()));
                RelativeLayout.LayoutParams relativeNote = layoutParamsRelativeLayoutNote(width, height, millisecondToPx(note.getTime()));
                note.setImageBitmap(Bitmap.createScaledBitmap(getImageBitmap(note.getFret()), width, height, false));
                relativeBackgroundString[i].addView(note,relativeNote);
            }
        }
        for (int i=0;i<backgroundLinePlay.length;i++){
            backgroundLinePlay[i].bringToFront();
        }
    }

    public void setTimeLinePlayOnTab(int timeLinePlay) {
        int indexLinePlay = 0;
        int height = getHeightTeb(null,relativeBackgroundString[0]);
        int width = height;

        for(int i=0;i<relativeBackgroundString.length;i++){
            RelativeLayout.LayoutParams relativeLinePlay = layoutParamsRelativeLayoutLinePlay(width,height, timeLinePlay);
            backgroundLinePlay[indexLinePlay].setLayoutParams(relativeLinePlay);
            indexLinePlay++;
        }
        for(int i=0;i<relativeBackground.length;i++){
            if(i>0&&i<6){
                RelativeLayout.LayoutParams relativeLinePlay = layoutParamsRelativeLayoutLinePlay(width,height, timeLinePlay);
                backgroundLinePlay[indexLinePlay].setLayoutParams(relativeLinePlay);
                indexLinePlay++;
            }
        }

    }

    public int getHeightTeb(Note note, RelativeLayout relativeLayout) {
        int height = 0;
        if(note != null) {
            if (note.getHeight() != 0) {
                height = note.getHeight();
            }
        }
        if(relativeLayout.getHeight() != 0) {
            height =relativeLayout.getHeight();
        }
        if(heightBackgroundNumber != 0) {
            height = heightBackgroundNumber;
        }
        return height;
    }
    private void stopLinePlay(){
        checkImagePlay = false;
        imageViewStop.setColorFilter(this.getResources().getColor(R.color.colorRed));
        imageViewPlay.setColorFilter(this.getResources().getColor(R.color.colorWhite));
        timeBuff += millisecondTime;
        timeLinePlay = (int) timeStamp;
        Log.i("TimeBuff",""+timeBuff);
        handler.removeCallbacks(runnable);
    }


    public Bitmap getImageBitmap(int fret) {
        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), 0);
        if(fret == 0){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_0_black);
        }
        else if(fret == 1){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_1_black);
        }
        else if(fret == 2){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_2_black);
        }
        else if(fret == 3){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_3_black);
        }
        else if(fret == 4){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_4_black);
        }
        else if(fret == 5){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_5_black);
        }
        else if(fret == 6){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_6_black);
        }
        else if(fret == 7){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_7_black);
        }
        else if(fret == 8){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_8_black);
        }
        else if(fret == 9){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_9_black);
        }
        else if(fret == 10){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_10_black);
        }
        else if(fret == 11){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_11_black);
        }
        else if(fret == 12){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_12_black);
        }
        else if(fret == 13){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_13_black);
        }
        else if(fret == 14){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_14_black);
        }
        else if(fret == 15){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_15_black);
        }
        else if(fret == 16){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_16_black);
        }
        else if(fret == 17){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_17_black);
        }
        else if(fret == 18){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_18_black);
        }
        else if(fret == 19){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_19_black);
        }
        else if(fret == 20){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_20_black);
        }
        else if(fret == 21){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_21_black);
        }
        else if(fret == 22){
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.number_22_black);
        }
        return  bitmap;
    }

    public int millisecondToPx(int time) {
        return Math.round(time/(1000/distanceSecond));
    }

    private void createLineTime(int i) {
        imageViewTime = new ImageView(this);
        timeNumber = new TextView(this);
        imageViewTime.setBackgroundColor(this.getResources().getColor(R.color.colorWhite));
        timeNumber.setTextColor(this.getResources().getColor(R.color.colorWhite));

        timeNumber.setText(modifyTextTime(i));
        RelativeLayout.LayoutParams layoutParamsTime = layoutParamsTime(imageViewTime,i*distanceSecond);
        RelativeLayout.LayoutParams layoutParamsTimeNumber = layoutParamsTimeNumber(timeNumber,i*distanceSecond);
        relativeLayoutBackgroundTime.addView(imageViewTime,layoutParamsTime);
        relativeLayoutBackgroundTime.addView(timeNumber,layoutParamsTimeNumber);
    }
    private String modifyTextTime(int i) {
        return String.format("%d:%02d", i / 60, i % 60);
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeBackground(RelativeLayout relativeLayout) {
        RelativeLayout.LayoutParams relativeBackground = new RelativeLayout.LayoutParams(relativeLayout.getWidth(),relativeLayout.getHeight());
        relativeBackground.width = ViewGroup.LayoutParams.MATCH_PARENT;
        relativeBackground.height = ViewGroup.LayoutParams.MATCH_PARENT;
        return relativeBackground;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutString(RelativeLayout relativeLayout) {
        RelativeLayout.LayoutParams relativeString = new RelativeLayout.LayoutParams(relativeLayout.getWidth(),relativeLayout.getHeight());
        relativeString.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        relativeString.height = ViewGroup.LayoutParams.MATCH_PARENT;
        relativeString.addRule(RelativeLayout.CENTER_VERTICAL);
        return relativeString;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutSeekBar(SeekBar seekBar) {
        RelativeLayout.LayoutParams relativeSeekBar = new RelativeLayout.LayoutParams(seekBar.getWidth(),seekBar.getHeight());
        relativeSeekBar.width = ViewGroup.LayoutParams.MATCH_PARENT;
        relativeSeekBar.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        relativeSeekBar.addRule(RelativeLayout.CENTER_VERTICAL);
        return relativeSeekBar;
    }
    public RelativeLayout.LayoutParams layoutParamsRelativeLayoutNote(int width, int height, int distance) {
        RelativeLayout.LayoutParams relativeNote = new RelativeLayout.LayoutParams(width,height);
        relativeNote.leftMargin = distance-(width/2);
        relativeNote.addRule(RelativeLayout.CENTER_VERTICAL);
        return relativeNote;
    }

    private RelativeLayout.LayoutParams layoutParamsRelativeTime(RelativeLayout relativeLayout) {
        RelativeLayout.LayoutParams relativeTime = new RelativeLayout.LayoutParams(relativeLayout.getWidth(),relativeLayout.getHeight());
        relativeTime.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        relativeTime.height = dpToPx(25);
        return relativeTime;
    }
    private LinearLayout.LayoutParams layoutParamsLinearLayoutString(LinearLayout linearLayout) {
        LinearLayout.LayoutParams linearLayoutString = new LinearLayout.LayoutParams(linearLayout.getWidth(),linearLayout.getHeight(),1);
        linearLayoutString.width = ViewGroup.LayoutParams.MATCH_PARENT;
        return linearLayoutString;
    }
    private LinearLayout.LayoutParams layoutParamsLinearBackground(LinearLayout linearLayout) {
        LinearLayout.LayoutParams linearLayoutBackground = new LinearLayout.LayoutParams(linearLayout.getWidth(),linearLayout.getHeight(),1);
        linearLayoutBackground.width = ViewGroup.LayoutParams.MATCH_PARENT;
        return linearLayoutBackground;
    }
    private RelativeLayout.LayoutParams layoutParamsTime(ImageView imageView,int distance) {
        RelativeLayout.LayoutParams layoutParamsTime = new RelativeLayout.LayoutParams(imageView.getWidth(),imageView.getHeight());
        layoutParamsTime.width=dpToPx(2);
        layoutParamsTime.height = ViewGroup.LayoutParams.MATCH_PARENT;
        layoutParamsTime.bottomMargin = dpToPx(5);
        layoutParamsTime.topMargin = 0;
        layoutParamsTime.leftMargin = distance-(dpToPx(2)/2);
        return layoutParamsTime;
    }
    private RelativeLayout.LayoutParams layoutParamsTimeNumber(TextView textView, int distance) {
        RelativeLayout.LayoutParams layoutParamsTimeNumber = new RelativeLayout.LayoutParams(textView.getWidth(),textView.getHeight());
        layoutParamsTimeNumber.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParamsTimeNumber.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParamsTimeNumber.addRule(RelativeLayout.CENTER_VERTICAL);
        layoutParamsTimeNumber.leftMargin = distance+10;
        return layoutParamsTimeNumber;
    }
    private RelativeLayout.LayoutParams layoutParamsString(ImageView imageView) {
        RelativeLayout.LayoutParams layoutParamsString = new RelativeLayout.LayoutParams(imageView.getWidth(),imageView.getHeight());
        layoutParamsString.width = ViewGroup.LayoutParams.MATCH_PARENT;
        layoutParamsString.height = dpToPx(3);
        layoutParamsString.addRule(RelativeLayout.CENTER_VERTICAL);
        return layoutParamsString;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutHeight(ImageView imageView) {
        RelativeLayout.LayoutParams layoutParamsHeight = new RelativeLayout.LayoutParams(imageView.getWidth(),imageView.getHeight());
        layoutParamsHeight.width = 50;
        layoutParamsHeight.height = ViewGroup.LayoutParams.MATCH_PARENT;
        layoutParamsHeight.addRule(RelativeLayout.CENTER_VERTICAL);
        return layoutParamsHeight;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutRoom(int width, int height, int distance) {
        RelativeLayout.LayoutParams layoutParamsRoom = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsRoom.width = width;
        layoutParamsRoom.height = ViewGroup.LayoutParams.MATCH_PARENT;
        layoutParamsRoom.leftMargin = distance-(width/2);
        return layoutParamsRoom;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutRoomTop(int width, int height, int distance) {
        RelativeLayout.LayoutParams layoutParamsRoom = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsRoom.width = width;
        layoutParamsRoom.height = height/2+dpToPx(3)/2;
        layoutParamsRoom.leftMargin = distance-(width/2);
        layoutParamsRoom.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        return layoutParamsRoom;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutRoomBottom(int width, int height, int distance) {
        RelativeLayout.LayoutParams layoutParamsRoom = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamsRoom.width = width;
        layoutParamsRoom.height = height/2+dpToPx(3)/2;
        layoutParamsRoom.leftMargin = distance-(width/2);
        layoutParamsRoom.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        return layoutParamsRoom;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutLinePlay(int width, int height, int distance) {
        RelativeLayout.LayoutParams layoutParamsLinePlay = new RelativeLayout.LayoutParams(width,ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParamsLinePlay.leftMargin = distance-(width/2);
        return layoutParamsLinePlay;
    }
    private RelativeLayout.LayoutParams layoutParamsRelativeLayoutShowTime() {
        RelativeLayout.LayoutParams relativeShowTime = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        relativeShowTime.addRule(RelativeLayout.CENTER_IN_PARENT);
        return relativeShowTime;
    }

    private String createXML() {
        String path = "";
        if(notes!=null){
            Cursor cursor = getDatabase();
            cursor.moveToNext();
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                xmlSerializer.setOutput(writer);
                xmlSerializer.startDocument("UTF-8", true);
                xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

                xmlSerializer.startTag("", "TabGuitar");
                xmlSerializer.startTag("", "Bpm");
                xmlSerializer.attribute("", "bit", cursor.getString(3));
                xmlSerializer.startTag("", "Signature");
                xmlSerializer.attribute("", "Signature", cursor.getString(4));
                int n = 0;
                int r = 0;
                for(int i=0;i<notes.size();i++) {
//                    Log.i("Note"+i,"S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
                    xmlSerializer.startTag("", "Note");
                    if(notes.get(i).getRoom() == false) {
                        xmlSerializer.attribute("", "note", "" + (n + 1));
                    }
                    else if(notes.get(i).getRoom() == true){
                        xmlSerializer.attribute("", "room", "" + r);
                    }
                        xmlSerializer.startTag("", "String");
                        xmlSerializer.text(""+notes.get(i).getString());
                        xmlSerializer.endTag("", "String");
                        xmlSerializer.startTag("", "Fret");
                        xmlSerializer.text(""+notes.get(i).getFret());
                        xmlSerializer.endTag("", "Fret");
                        xmlSerializer.startTag("", "Time");
                        xmlSerializer.text(""+notes.get(i).getTime());
                        xmlSerializer.endTag("", "Time");
                        xmlSerializer.startTag("", "Room");
                        xmlSerializer.text(""+notes.get(i).getRoom());
                        xmlSerializer.endTag("", "Room");
                    xmlSerializer.endTag("", "Note");
                    n++;
                    r++;
                }
                xmlSerializer.endTag("", "Signature");
                xmlSerializer.endTag("", "Bpm");
                //end tag <file>
                xmlSerializer.endTag("", "TabGuitar");
                xmlSerializer.endDocument();
            }catch (Exception ex){
                ex.printStackTrace();
            }
            Log.i("XML",""+writer.toString());

            File internalDir = getFilesDir();
            File dir = new File(internalDir.getAbsolutePath()+"/Fingerstyle XML");
            if(!dir.exists()){
                dir.mkdir();
            }
            Cursor cursorGetFileById = getFileById(fileID);
            cursorGetFileById.moveToNext();
            String newPath = "";
            File file = null;
            Log.i("Path First:",""+cursorGetFileById.getString(cursorGetFileById.getColumnIndex(COLUMN_PATH)));
            if(cursorGetFileById.getString(cursorGetFileById.getColumnIndex(COLUMN_PATH)) == null){
                newPath = "FID"+fileID+cursor.getString(1)+".xml";
                file = new File(dir,newPath);
            }
            else {
                newPath = cursorGetFileById.getString(cursorGetFileById.getColumnIndex(COLUMN_PATH));
                file = new File(newPath);
            }
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(writer.toString().getBytes());
                fileOutputStream.close();
//                Toast.makeText(this,"Save OK", Toast.LENGTH_LONG).show();
            }catch (FileNotFoundException e) {
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }
            path = file.getAbsolutePath();
            Log.w("PATH",path);
        }
        return path;
    }

    public void detector(){
        dispatcher.addAudioProcessor(new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.FFT_YIN,44100, AudioRecord.getMinBufferSize
                (44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT), new PitchDetectionHandler() {
            @Override
            public void handlePitch(PitchDetectionResult pitchDetectionResult,
                                    final AudioEvent audioEvent) {
                pitchInHz = pitchDetectionResult.getPitch();
                levelAudio = silenceDetector.currentSPL();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (checkImageMicrophone) {
                            if (levelAudio >= -70 && pitchInHz != -1) {
                                Log.i("",timeStamp + "#" + String.format
                                        ("%.4f", pitchInHz) + "#" + String.format("%.4f", levelAudio));
                                audioInfo.add(timeStamp + "#" + String.format
                                        ("%.4f", pitchInHz) + "#" + String.format("%.4f", levelAudio));
                            }
                        } else {
                            if(checkRemoveNote){
                                Log.i("Show Time Start",""+timeLinePlay);
                                Log.i("Show Time Stop",""+timeBuff);
                                for(int i=0;i<notes.size();){
                                    if(!notes.get(i).getRoom()&&notes.get(i).getTime()>=timeLinePlay&&notes.get(i).getTime()<=timeBuff){
//                                        Log.i("Delete Note","S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
                                        notes.remove(i);
                                    }
                                    else{
                                        i++;
                                    }
                                }
                            }
                            if (audioInfo.size() != 0) {
                                DetectionNote(audioInfo);
//                                createNoteOnString(resultNote);
                                audioInfo.clear();
                            }
                            if(checkRemoveNote){

                                timeLinePlay = (int) timeBuff;
                                Log.i("TimeLinePlay",""+timeLinePlay);

                                linearBackGroundTab.removeAllViews();
                                for(int i=0;i<relativeBackgroundString.length;i++){
                                    relativeBackgroundString[i].removeAllViews();
                                }
                                for(int i=0;i<relativeBackground.length;i++){
                                    relativeBackground[i].removeAllViews();
                                }
                                createTab();

                                checkRemoveNote = false;
                            }
                        }
                    }
                });
            }
        }));
    }


    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    public void standardFrequency(){
        for(int i=0;i<6;i++){
            for(float j=0;j<=maximumFrequency;j++) {
                float frequency = 0;
                int checkBreak = 0;
                if(i==0){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*82.41));
                    if(j==4){ checkBreak =1; }
                }
                else if(i==1){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*110.00));
                    if(j==4){ checkBreak =1; }
                }
                else if(i==2){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*146.83));
                    if(j==4){ checkBreak =1; }
                }
                else if(i==3){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*196.00));
                    if(j==3){ checkBreak =1; }
                }
                else if(i==4){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*246.94));
                    if(j==4){ checkBreak =1; }
                }
                else if(i==5){
                    frequency = Float.parseFloat(""+(Math.pow(2, j/12f)*329.63));
                }
                modelFrequency.add(frequency);
                if(checkBreak ==1){
                    break;
                }
            }
        }
    }

    public void DetectionNote(ArrayList<String> audioInfo) {
        long time;
        float frequency;
        float soundValue;
        float soundTemp = 0;
        String result;
        for(int i=0;i<audioInfo.size();i++) {
            String str[] = audioInfo.get(i).split("#");
            time = Long.parseLong(str[0]);
            frequency = Float.parseFloat(str[1]);
            soundValue = Math.abs(Float.parseFloat(str[2]));
            if(i==0) {
                soundTemp=soundValue;
                boxFrequency.add(time+"#"+frequency+"#"+soundValue);
            }
            else if(soundTemp>soundValue) {
                if(soundTemp-soundValue>=1) {
                    if(boxFrequency.size()>=2) {
                        Log.i ("Box Frequency" ,  ""+boxFrequency);
                        result = detectoinFrequency(modelFrequency,boxFrequency);
                        resultNote(result);
                    }
                    boxFrequency.clear();
                }
                soundTemp = soundValue;
                boxFrequency.add(time+"#"+frequency+"#"+soundValue);
            }
            else if(i==audioInfo.size()-1) {
                boxFrequency.add(time+"#"+frequency+"#"+soundValue);
                result = detectoinFrequency(modelFrequency,boxFrequency);
                Log.d ("LOGMessage" ,  ""+result);
                resultNote(result);
                boxFrequency.clear();
            }
            else if(soundTemp<=soundValue) {
                soundTemp = soundValue;
                boxFrequency.add(time+"#"+frequency+"#"+soundValue);
            }
        }
    }

    private void resultNote(String result) {
        String splitAudioInfo[] = result.split("#");
        String fretString[] = checkFret(splitAudioInfo[1]).split("#");
        Log.w("NOTE",fretString[0]+"#"+fretString[1]+"#"+splitAudioInfo[0]);
//        resultNote.add(fretString[0]+"#"+fretString[1]+"#"+splitAudioInfo[0]);/

        //add Sting,Fret,Time,Room
        notes.add(new Note(this,Integer.parseInt(fretString[0]),Integer.parseInt(fretString[1]),Integer.parseInt(splitAudioInfo[0]),false));
        int indexNew = notes.size() - 1;
//        int height = getHeightTeb(notes.get(indexNew),relativeBackgroundString[0]);
//        Log.w("Height",""+height);

        noteListener = new NoteListener(this, notes.get(indexNew), seekBarFret);
        notes.get(indexNew).setOnClickListener(noteListener);
        notes.get(indexNew).setBackgroundResource(0);
        addNoteOnString(notes.get(indexNew));

        Collections.sort(notes);
    }

    private String checkFret(String frequency) {
        int indextString=0,indexFret =0;
        for(int i=0;i<6;i++){
            indextString = i;
            int checkBack = 0;
            float j=0;
            for(;j<=maximumFrequency;j++) {
                indexFret = Math.round(j);
                float frequencyModel =0;
                if(i==0){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*329.63));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==1){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*246.94));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==2){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*196.00));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==3){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*146.83));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==4){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*110.00));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
                else if(i==5){
                    frequencyModel = Float.parseFloat(""+(Math.pow(2, j/12f)*82.41));
                    if(Float.parseFloat(frequency) == frequencyModel){ checkBack = 1;break; }
                }
            }
            if(checkBack == 1){ break; }
        }
        return (indextString+1)+"#"+indexFret;
    }

    String detectoinFrequency(ArrayList<Float> modelFrequency , ArrayList<String> boxFrequency) {
        for(int i=0;i<boxFrequency.size();i++) {
            String str[] = boxFrequency.get(i).split("#");
            for(int j=0;j<modelFrequency.size();j++) {
                difference[j] = Math.abs(Float.parseFloat(str[1])-modelFrequency.get(j));
            }
            int indexDifferrence=0;
            float minDifference = difference[0];
            for(int j=0;j<difference.length;j++) {
                if(difference[j]<minDifference) {
                    minDifference = difference[j];
                    indexDifferrence = j;
                }
            }
            boxFrequency.set(i, str[0]+"#"+modelFrequency.get(indexDifferrence)+"#"+str[2]);
        }
        String result;
        countData.clear();
        for(int i=0;i<boxFrequency.size();i++) {
            String strBoxFrequency[] = boxFrequency.get(i).split("#");
            if(countData.size()==0) {
                countData.add(boxFrequency.get(i)+"#"+1);
            }
            else {
                int check = 0;
                for(int j=0;j<countData.size();j++) {
                    String strCountData[] = countData.get(j).split("#");
                    if(strCountData[1].equals(strBoxFrequency[1])) {
                        check = 1;
                        int number  = Integer.parseInt(strCountData[3]);
                        number++;
                        countData.set(j, strCountData[0]+"#"+strCountData[1]+"#"+strCountData[2]+"#"+number);
                    }
                }
                if(check == 0) {
                    countData.add(boxFrequency.get(i)+"#"+1);
                }
            }
        }
        result = countData.get(0);
        for(int i=0;i<countData.size();i++) {
            String str[] = result.split("#");
            String strCountData[] = countData.get(i).split("#");
            if(Integer.parseInt(str[3])<Integer.parseInt(strCountData[3])) {
                result = countData.get(i);
            }
        }
        String strBoxFrequency[] = boxFrequency.get(0).split("#");
        String strResult[] = result.split("#");
        result = strBoxFrequency[0]+"#"+strResult[1]+"#"+strBoxFrequency[2]+"#"+strResult[3];
        return result;
    }
    /*Detect Note*/

    public Runnable runnable = new Runnable() {
        public void run() {
//            Log.i("Get Text",""+ checkImageMicrophone);
            if(checkImagePlay && indexPlayNote < notes.size()-1 || checkImageMicrophone) {
                millisecondTime = SystemClock.uptimeMillis() - startTime;
                UpdateTime = timeBuff + millisecondTime;
                timeStamp = UpdateTime;
                Seconds = (int) (UpdateTime / 1000);
                Minutes = Seconds / 60;
                Seconds = Seconds % 60;
                MilliSeconds = (int) (UpdateTime % 1000);

                textViewShowTime.setText("" + String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds) + ":" + String.format("%03d", MilliSeconds));
//            Log.i("Get Text",""+ (editTextModePlay.getText().toString().equals("All")));
                if(millisecondToPx((int) timeStamp) >= scrollViewBackGroundTab.getScrollX()+widthBackGroundTab){
                    scrollViewBackGroundTab.scrollTo(scrollViewBackGroundTab.getScrollX()+widthBackGroundTab,0);
                    Log.w("ScrollX",""+scrollViewBackGroundTab.getScrollX());
                }
                if (checkImageMicrophone == false) {
                    if (editTextModePlay.getText().toString().equals("All")) {
                        while ((int) timeStamp >= notes.get(indexPlayNote).getTime()) {
                            if (notes.get(indexPlayNote).getRoom() == false) {
                                Log.i("Note" + indexPlayNote, " S:" + notes.get(indexPlayNote).getString()
                                        + " F:" + notes.get(indexPlayNote).getFret() + " T:" + notes.get(indexPlayNote).getTime() + " R:" + notes.get(indexPlayNote).getRoom());
                                soundPool.play(sound[notes.get(indexPlayNote).getString()-1][notes.get(indexPlayNote).getFret()], 1, 1, 1, 0, 1);

                            }
                            indexPlayNote++;
//                            Log.i("Check",""+ ("Index: "+indexPlayNote+" Size: "+notes.size()));
                            if(indexPlayNote > notes.size()-1 ){
                                stopLinePlay();
                                break;
                            }
                        }
                    } else if (editTextModePlay.getText().toString().equals("Room")) {
//                        Log.i("Check",""+ ("Time Stamp: "+(int) timeStamp+" >= Time Note: "+notes.get(indexPlayNote).getTime()));
                        while ((int) timeStamp >= notes.get(indexPlayNote).getTime()) {
                            Log.i("Note" + indexPlayNote, " S:" + notes.get(indexPlayNote).getString()
                                    + " F:" + notes.get(indexPlayNote).getFret() + " T:" + notes.get(indexPlayNote).getTime() + " R:" + notes.get(indexPlayNote).getRoom());

                            if (notes.get(indexPlayNote).getRoom() == false) {
//                                Log.i("Note" + indexPlayNote, " S:" + notes.get(indexPlayNote).getString()
//                                        + " F:" + notes.get(indexPlayNote).getFret() + " T:" + notes.get(indexPlayNote).getTime() + " R:" + notes.get(indexPlayNote).getRoom());
                                soundPool.play(sound[notes.get(indexPlayNote).getString()-1][notes.get(indexPlayNote).getFret()], 1, 1, 1, 0, 1);

                            }
                            else if(notes.get(indexPlayNote).getRoom() == true && notes.get(indexPlayNote) != notes.get(indexPlayRoom)){
                                checkImagePlay = false;
                                imageViewStop.setColorFilter(getBaseContext().getResources().getColor(R.color.colorRed));
                                imageViewPlay.setColorFilter(getBaseContext().getResources().getColor(R.color.colorWhite));
                                timeBuff = notes.get(indexPlayRoom).getTime();
//                                timeLinePlay = (int) timeStamp;
                                handler.removeCallbacks(runnable);
                            }

                            indexPlayNote++;
                            if(indexPlayNote > notes.size()-1 ){
                                checkImagePlay = false;
                                imageViewStop.setColorFilter(getBaseContext().getResources().getColor(R.color.colorRed));
                                imageViewPlay.setColorFilter(getBaseContext().getResources().getColor(R.color.colorWhite));
                                timeBuff = notes.get(indexPlayRoom).getTime();
//                                timeLinePlay = (int) timeStamp;
                                handler.removeCallbacks(runnable);
                                break;
                            }
                        }
                    }
                    else {
                        int maxNote = 0;
                        try {
                            maxNote = Integer.parseInt(editTextModePlay.getText().toString());
                            while ((int) timeStamp >= notes.get(indexPlayNote).getTime()) {
                                if (notes.get(indexPlayNote).getRoom() == false) {
                                    Log.i("Note" + indexPlayNote, " S:" + notes.get(indexPlayNote).getString()
                                            + " F:" + notes.get(indexPlayNote).getFret() + " T:" + notes.get(indexPlayNote).getTime() + " R:" + notes.get(indexPlayNote).getRoom());
                                    soundPool.play(sound[notes.get(indexPlayNote).getString()-1][notes.get(indexPlayNote).getFret()], 1, 1, 1, 0, 1);

                                    /*Resources res = getBaseContext().getResources();
                                    int rawId;
                                    if (notes.get(indexPlayNote).getString() == 1) {
                                        rawId = res.getIdentifier("one_" + notes.get(indexPlayNote).getFret(), "raw", getBaseContext().getPackageName());
                                        Log.i("Note", "one_" + notes.get(indexPlayNote).getFret() + " = " + rawId);
                                        playAudioString1(rawId);
                                    } else if (notes.get(indexPlayNote).getString() == 2) {
                                        rawId = res.getIdentifier("two_" + notes.get(indexPlayNote).getFret(), "raw", getBaseContext().getPackageName());
                                        Log.i("Note", "one_" + notes.get(indexPlayNote).getFret() + " = " + rawId);
                                        playAudioString2(rawId);

                                    } else if (notes.get(indexPlayNote).getString() == 3) {
                                        rawId = res.getIdentifier("three_" + notes.get(indexPlayNote).getFret(), "raw", getBaseContext().getPackageName());
                                        Log.i("Note", "three_" + notes.get(indexPlayNote).getFret() + " = " + rawId);
                                        playAudioString3(rawId);

                                    } else if (notes.get(indexPlayNote).getString() == 4) {
                                        rawId = res.getIdentifier("four_" + notes.get(indexPlayNote).getFret(), "raw", getBaseContext().getPackageName());
                                        Log.i("Note", "four_" + notes.get(indexPlayNote).getFret() + " = " + rawId);
                                        playAudioString4(rawId);

                                    } else if (notes.get(indexPlayNote).getString() == 5) {
                                        rawId = res.getIdentifier("five_" + notes.get(indexPlayNote).getFret(), "raw", getBaseContext().getPackageName());
                                        Log.i("Note", "five_" + notes.get(indexPlayNote).getFret() + " = " + rawId);
                                        playAudioString5(rawId);

                                    } else if (notes.get(indexPlayNote).getString() == 6) {
                                        rawId = res.getIdentifier("six_" + notes.get(indexPlayNote).getFret(), "raw", getBaseContext().getPackageName());
                                        Log.i("Note", "six_" + notes.get(indexPlayNote).getFret() + " = " + rawId);
                                        playAudioString6(rawId);
                                    }*/

                                    countNote++;
                                    if(countNote==maxNote){
                                        checkImagePlay = false;
                                        imageViewStop.setColorFilter(getBaseContext().getResources().getColor(R.color.colorRed));
                                        imageViewPlay.setColorFilter(getBaseContext().getResources().getColor(R.color.colorWhite));
                                        handler.removeCallbacks(runnable);
                                        break;
                                    }
                                }
                                indexPlayNote++;
                                if(indexPlayNote > notes.size()-1 ){
                                    checkImagePlay = false;
                                    imageViewStop.setColorFilter(getBaseContext().getResources().getColor(R.color.colorRed));
                                    imageViewPlay.setColorFilter(getBaseContext().getResources().getColor(R.color.colorWhite));
                                    handler.removeCallbacks(runnable);
                                    break;
                                }
                            }
                        }catch (NumberFormatException ex){
                            editTextModePlay.setText("1");
                            ex.printStackTrace();
                        }
                    }
                }
                else {
                    if ((int) timeStamp >= notes.get(notes.size()-1).getTime()){//ArrayIndexOutOfBoundsException
                        while (true) {
                            longtime++;
                            createLineTime(longtime);
                            if (longtime * 1000 >= notes.get(notes.size()-1).getTime() + distanceRoom) {
                                break;
                            }
                        }
                        Note note = new Note(getBaseContext(), 0, 0, notes.get(notes.size()-1).getTime() + distanceRoom, true);
                        notes.add(note);
                        addLinePlayOnTab(note);
                    }
                }
                setTimeLinePlayOnTab(millisecondToPx((int) timeStamp));
                handler.postDelayed(this, 0);
            }

        }
    };

    public void playAudioString1(int noteId){
        stopPlay(mediaPlayerString1);
        mediaPlayerString1 = MediaPlayer.create(getBaseContext(),noteId);
        mediaPlayerString1.seekTo(0);
        mediaPlayerString1.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                mediaPlayerString1.start();
            }
        });
    }
    public void playAudioString2(int noteId){
        stopPlay(mediaPlayerString2);
        mediaPlayerString2 = MediaPlayer.create(getBaseContext(),noteId);
        mediaPlayerString2.seekTo(0);
        mediaPlayerString2.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                mediaPlayerString2.start();
            }
        });

    }
    public void playAudioString3(int noteId){
        stopPlay(mediaPlayerString3);
        mediaPlayerString3 = MediaPlayer.create(getBaseContext(),noteId);
        mediaPlayerString3.seekTo(0);
        mediaPlayerString3.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                mediaPlayerString3.start();
            }
        });
    }
    public void playAudioString4(int noteId){
        stopPlay(mediaPlayerString4);
        mediaPlayerString4 = MediaPlayer.create(getBaseContext(),noteId);
        mediaPlayerString4.seekTo(0);
        mediaPlayerString4.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                mediaPlayerString4.start();
            }
        });
    }
    public void playAudioString5(int noteId){
        stopPlay(mediaPlayerString5);
        mediaPlayerString5 = MediaPlayer.create(getBaseContext(),noteId);
        mediaPlayerString5.seekTo(0);
        mediaPlayerString5.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                mediaPlayerString5.start();
            }
        });

    }
    public void playAudioString6(int noteId){
        stopPlay(mediaPlayerString6);
        mediaPlayerString6 = MediaPlayer.create(getBaseContext(),noteId);
        mediaPlayerString6.seekTo(0);
        mediaPlayerString6.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                mediaPlayerString6.start();
            }
        });
    }

    public void stopPlay(MediaPlayer mediaPlayer){
        if(mediaPlayer != null){
           mediaPlayer.stop();
           mediaPlayer.release();
           mediaPlayer = null;
        }
    }

    private Cursor getDatabase() {
        String sql = "SELECT * FROM FileTabGuitar WHERE _id = "+fileID;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }

    private Cursor getFileById(String clickFileID) {
        String sql = "SELECT * FROM FileTabGuitar WHERE _id = "+clickFileID;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }

    private void setUpdatePath(String path) {
        String sql = "UPDATE FileTabGuitar SET path = ? WHERE _id = ?";
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        String[] dataPath = {path,fileID};
        db.execSQL(sql,dataPath);
        Cursor cursor = getDatabase();
        cursor.moveToNext();
        Log.w("SELECTPATH",cursor.getString(7));
    }

    private void setUpdateLongtime(String longtime) {
        Log.w("LONGTIME",longtime);
        String sql = "UPDATE FileTabGuitar SET longtime = ? WHERE _id = ?";
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        String[] dataLongtime = {longtime,fileID};
        db.execSQL(sql,dataLongtime);

        Cursor cursor = getDatabase();
        cursor.moveToNext();
        Log.w("SELECT LONGTIME",cursor.getString(5));
    }

    public int getDistanceSeconds(){
        return this.distanceSecond;
    }
    public void setDistanceSeconds(int distanceSecond){
        this.distanceSecond = distanceSecond;
    }
    public int getTimeLinePlay(){
        return this.timeLinePlay;
    }
    public void setTimeLinePlay(int timeLinePlay){
        this.timeLinePlay = timeLinePlay;
    }
    public Boolean getCheckSelectionNote(){
        return this.checkSelectionNote;
    }
    public void setCheckSelectionNote(Boolean checkSelectionNote){
        this.checkSelectionNote = checkSelectionNote;
    }
    public Boolean getCheckSetTime(){
        return this.checkSetTime;
    }
    public void setCheckSetTime(Boolean checkSetTime){
        this.checkSetTime = checkSetTime;
    }
    public String getChoiceSetTime(){
        return this.choiceSetTime;
    }
    public void setChoiceSetTime(String choiceSetTime){
        this.choiceSetTime = choiceSetTime;
    }

//    boolean doubleBackToExitPressedOnce = false;
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Log.w("Back","Main");
//        if (doubleBackToExitPressedOnce) {
//            Intent intent = new Intent(this, MainActivity.class);
//            intent.putExtra("key", "ds"); //Optional parameters
//            this.startActivity(intent);
//            finish();
//            return;
//        }
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce=false;
//            }
//        }, 2000);
//    }
}

