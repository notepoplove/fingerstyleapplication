package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.EditProjectActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.TabActivity;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;
import com.example.suphotkhotphoothon.fingerstyleapplication4.util.Note;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_BPM;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_CREATOR;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_NAME;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_PATH;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.Constants.COLUMN_SIGNATURE;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProjectFragment extends Fragment implements View.OnClickListener, TextWatcher {

    private View view;
    private String fileID = null;

    private EditText editTextTitle;
    private EditText editTextBy;
    private EditText editTextBpm;
    private EditText editTextSignature1;
    private EditText editTextSignature2;
    private TextView textErrorTitle;
    private TextView textErrorCreator;
    private TextView textErrorBpm;
    private TextView textErrorSignature;
    private Button createProjectButtonOk;

    private ArrayList<Note> notes;

    private SQLiteHelper sqLiteHelper;
    private SimpleDateFormat sdf;

    private String filePath = null;
    private int defaultTime = 60000;
    private int distanceTempo;
    private int distanceRoom = 0;
    private int originalBpm = 0;
    private int bpm = 0;
    private int signature = 0;

    private static final Pattern VALID_NUMBER_REGEX = Pattern.compile("[^0-9]|^'|'$|''\"");

    public EditProjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_edit_project, container, false);

        init();

        if (savedInstanceState == null) {
            Bundle extras = getActivity().getIntent().getExtras();
            if(extras == null) {
                fileID = null;
            } else {
                fileID = extras.getString("file_id");
            }
        } else {
            fileID = (String) savedInstanceState.getSerializable("file_id");
        }
        if(fileID != null){
            Log.w("File ID",""+fileID);
            Cursor cursor = getDatabase();
            cursor.moveToNext();

            editTextTitle.setText(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
            editTextBy.setText(cursor.getString(cursor.getColumnIndex(COLUMN_CREATOR)));
            editTextBpm.setText(cursor.getString(cursor.getColumnIndex(COLUMN_BPM)));
            originalBpm = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_BPM)));
            String splitSignature[] =  cursor.getString(cursor.getColumnIndex(COLUMN_SIGNATURE)).split("/");
            editTextSignature1.setText(splitSignature[0]);
            editTextSignature2.setText(splitSignature[1]);
            filePath = cursor.getString(cursor.getColumnIndex(COLUMN_PATH));


            readXMLFile();
            Log.i("Notes Size",""+notes.size());


        }

        return view;
    }

    private void init() {
        createProjectButtonOk = (Button)view.findViewById(R.id.fragmentEditProject_bt_ok);
        editTextTitle = (EditText) view.findViewById(R.id.fragmentEditProject_ed_title);
        editTextBy =  (EditText) view.findViewById(R.id.fragmentEditProject_ed_by);
        editTextBpm = (EditText) view.findViewById(R.id.fragmentEditProject_ed_bpm);
        editTextSignature1 = (EditText) view.findViewById(R.id.fragmentEditProject_ed_signature1);
        editTextSignature2 = (EditText) view.findViewById(R.id.fragmentEditProject_ed_signature2);
        textErrorTitle = (TextView)view.findViewById(R.id.fragmentEditProject_textView_errorTitle);
        textErrorCreator = (TextView)view.findViewById(R.id.fragmentEditProject_textView_errorCreator);
        textErrorBpm = (TextView)view.findViewById(R.id.fragmentEditProject_textView_errorBpm);
        textErrorSignature = (TextView)view.findViewById(R.id.fragmentEditProject_textView_errorSignature);

        sqLiteHelper = new SQLiteHelper(getContext());
        notes = new ArrayList<Note>();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        createProjectButtonOk.setOnClickListener(this);
        editTextSignature1.addTextChangedListener(this);
        editTextSignature2.addTextChangedListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == createProjectButtonOk){
            String title = editTextTitle.getText().toString();
            String creator = editTextBy.getText().toString();
            String bpm = editTextBpm.getText().toString();
            String signature1 = editTextSignature1.getText().toString();
            String signature2 = editTextSignature2.getText().toString();
            String currentTime = sdf.format(new Date());

            Boolean checkCreateProject = true;
            textErrorTitle.setText("");
            textErrorCreator.setText("");
            textErrorBpm.setText("");
            textErrorSignature.setText("");

            if(title.equals("")){
                checkCreateProject = false;
                textErrorTitle.setText("*please enter a title.");
            }

            if(creator.equals("")){
                checkCreateProject = false;
                textErrorCreator.setText("*please enter a creator.");
            }

            if(bpm.equals("")){
                checkCreateProject = false;
                textErrorBpm.setText("*please enter a bpm.");
            }
            else {
                if(validateNumber(bpm)){
                    checkCreateProject = false;
                    textErrorBpm.setText("*please enter 0-9 only.");
                }
            }

            if(signature1.equals("") || signature2.equals("")){
                checkCreateProject = false;
                textErrorSignature.setText("*please enter a signature.");
            }
            else {
                if(validateNumber(signature1) || validateNumber(signature2)){
                    checkCreateProject = false;
                    textErrorSignature.setText("*please enter 0-9 only.");
                }
            }

            if(checkCreateProject) {
                this.bpm = Integer.parseInt(bpm);
                signature = Integer.parseInt(signature1);
                distanceTempo = setMillisecondTempo(this.bpm);
                distanceRoom = distanceTempo*signature;
                Log.i("Distance Tempo",""+distanceTempo);
                setTime();
                String path = createXML();
                setUpdateFile(path);
                Collections.sort(notes);
                setUpdateLongtime(""+notes.get(notes.size()-1).getTime());

                Intent intent = new Intent(getActivity(), TabActivity.class);
                intent.putExtra("file_id", fileID);
                startActivity(intent);
                getActivity().finish();
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        Log.i("Edit","Edit");
        if(editTextSignature1.getText().hashCode() == s.hashCode()){
            Log.i("EditTextSignature1","EditTextSignature1");
        }
        else if(editTextSignature2.getText().hashCode() == s.hashCode()){
            Log.i("EditTextSignature2","EditTextSignature2");
        }
    }

    private void readXMLFile() {
        try {
            FileInputStream fis = new FileInputStream (new File(filePath));
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fis);
            Element element = doc.getDocumentElement();
            element.normalize();

            NodeList nList = doc.getElementsByTagName("Note");

            for (int i=0; i<nList.getLength(); i++) {

                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element2 = (Element) node;
//                        Log.i("Tag:",""+element.getElementsByTagName("String").item(0).getNodeName());
//                        Log.i("String:",getValue("String", element2));
//                        Log.i("Fret:",getValue("Fret", element2));
//                        Log.i("Time:",getValue("Time", element2));
//                        Log.i("Time:",getValue("Room", element2));
//                        Log.i("Line:","-----------------------");
                    notes.add(new Note(getContext(),
                            Integer.parseInt(getValue("String", element2)),
                            Integer.parseInt(getValue("Fret", element2)),
                            Integer.parseInt(getValue("Time", element2)),
                            Boolean.parseBoolean(getValue("Room", element2))));
                }
            }
            fis.close();
        } catch (Exception e) {e.printStackTrace();}
    }

    private void setTime() {
        int countRoom = 1;
        for(int i=1;i<notes.size();i++){
//            Log.w("Room"+i," S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
            if(notes.get(i).getRoom() == false){
                int time = notes.get(i).getTime();
                if(bpm>originalBpm) {
                    time = notes.get(i).getTime() - setMillisecondTempo(bpm-originalBpm);
                }
                else if(bpm<originalBpm){
                    time = notes.get(i).getTime() + setMillisecondTempo(originalBpm-bpm);
                }

                if(time<0){
                    time = 0;
                }
                notes.get(i).setTime(time);
            }
            else {
                int time = countRoom*distanceRoom;
                notes.get(i).setTime(time);
                countRoom++;
            }
        }
//        for(int i=1;i<notes.size();i++){
//            Log.i("Room"+i," S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
//        }
    }

    private String createXML() {
        String path = "";
        if(notes!=null){
            Cursor cursor = getDatabase();
            cursor.moveToNext();
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                xmlSerializer.setOutput(writer);
                xmlSerializer.startDocument("UTF-8", true);
                xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

                xmlSerializer.startTag("", "TabGuitar");
                xmlSerializer.startTag("", "Bpm");
                xmlSerializer.attribute("", "bit", cursor.getString(3));
                xmlSerializer.startTag("", "Signature");
                xmlSerializer.attribute("", "Signature", cursor.getString(4));
                int n = 0;
                int r = 0;
                for(int i=0;i<notes.size();i++) {
//                    Log.i("Note"+i,"S:"+notes.get(i).getString()+" F:"+notes.get(i).getFret()+" T:"+notes.get(i).getTime()+" R:"+notes.get(i).getRoom());
                    xmlSerializer.startTag("", "Note");
                    if(notes.get(i).getRoom() == false) {
                        xmlSerializer.attribute("", "note", "" + (n + 1));
                    }
                    else if(notes.get(i).getRoom() == true){
                        xmlSerializer.attribute("", "room", "" + r);
                    }
                    xmlSerializer.startTag("", "String");
                    xmlSerializer.text(""+notes.get(i).getString());
                    xmlSerializer.endTag("", "String");
                    xmlSerializer.startTag("", "Fret");
                    xmlSerializer.text(""+notes.get(i).getFret());
                    xmlSerializer.endTag("", "Fret");
                    xmlSerializer.startTag("", "Time");
                    xmlSerializer.text(""+notes.get(i).getTime());
                    xmlSerializer.endTag("", "Time");
                    xmlSerializer.startTag("", "Room");
                    xmlSerializer.text(""+notes.get(i).getRoom());
                    xmlSerializer.endTag("", "Room");
                    xmlSerializer.endTag("", "Note");
                    n++;
                    r++;
                }
                xmlSerializer.endTag("", "Signature");
                xmlSerializer.endTag("", "Bpm");
                //end tag <file>
                xmlSerializer.endTag("", "TabGuitar");
                xmlSerializer.endDocument();
            }catch (Exception ex){
                ex.printStackTrace();
            }

//            Log.i("XML",""+writer.toString());
            File file = null;
            file = new File(filePath);
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(writer.toString().getBytes());
                fileOutputStream.close();
//                Toast.makeText(getActivity(),"Save OK", Toast.LENGTH_LONG).show();
            }catch (FileNotFoundException e) {
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }
            path = file.getAbsolutePath();
            Log.w("PATH",path);
        }
        return path;
    }

    private String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }
    private int setMillisecondTempo(int bpm) {
        return Math.round(defaultTime/bpm);
    }

    private void setUpdateFile(String path) {
        String sql = "UPDATE FileTabGuitar SET "+COLUMN_NAME+" = ?,"+COLUMN_CREATOR+" = ?,"+COLUMN_BPM+" = ?,"+COLUMN_SIGNATURE+" = ?,"+COLUMN_PATH+" = ? WHERE _id = ?";
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();

        String[] dataPath = {editTextTitle.getText().toString(),
                editTextBy.getText().toString(),
                editTextBpm.getText().toString(),
                editTextSignature1.getText().toString()+"/"+editTextSignature2.getText().toString(),
                path,fileID};
        db.execSQL(sql,dataPath);
        Cursor cursor = getDatabase();
        cursor.moveToNext();
        Log.w("Cursor",cursor.getString(1));
        Log.w("SELECTPATH",cursor.getString(7));
    }

    private void setUpdateLongtime(String longtime) {
        Log.w("LONGTIME",longtime);
        String sql = "UPDATE FileTabGuitar SET longtime = ? WHERE _id = ?";
        SQLiteDatabase db = sqLiteHelper.getWritableDatabase();
        String[] dataLongtime = {longtime,fileID};
        db.execSQL(sql,dataLongtime);

        Cursor cursor = getDatabase();
        cursor.moveToNext();
        Log.w("SELECT LONGTIME",cursor.getString(5));
    }

    private Cursor getDatabase() {
        String sql = "SELECT * FROM FileTabGuitar WHERE _id = "+fileID;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }
    public static boolean validateNumber(String numberString) {
        Matcher matcher = VALID_NUMBER_REGEX.matcher(numberString);
        return matcher.find();
    }

}
