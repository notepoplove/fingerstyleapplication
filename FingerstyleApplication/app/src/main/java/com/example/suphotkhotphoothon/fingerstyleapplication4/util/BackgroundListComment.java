package com.example.suphotkhotphoothon.fingerstyleapplication4.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.fragment.FileFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.pdf.codec.Base64;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BackgroundListComment extends LinearLayout implements View.OnClickListener {
    String commentId = null;
    String userId = null;
    private float density;
    private CircleImageView circleImageViewUses;
    private LinearLayout linearBackgroundMessage;
    private LinearLayout linearBackgroundName;
    private LinearLayout linearBackgroundReply;
    private LinearLayout linearBackgroundSendReply;
    private RelativeLayout relativeBackgroundMessage;
    private RelativeLayout relativeBackgroundReply;
    private AppCompatImageView imageViewImageSend;
    private TextView textViewName;
    private TextView textViewDate;
    private TextView textViewMessage;
    private TextView editTextReply;
    private EditText editTextSendReply;

    private ArrayList<BackgroundListComment> arrayListBackground;
    private Bitmap bitmapImageViewUses;

    private boolean checkShow = false;

    private JsonObject jsonObject;
    private Context context;
    private SimpleDateFormat sdf;
    private FileFragment fileFragment;
    private Bitmap bitmapResultUser;
    private Bitmap bitmapImageUser;

    public BackgroundListComment(FileFragment fileFragment, JsonObject jsonObject) {
        super(fileFragment.getContext());
        this.fileFragment = fileFragment;
        this.jsonObject = jsonObject;
        this.context = fileFragment.getContext();
        this.arrayListBackground = fileFragment.arrayListBackground;
        this.bitmapImageUser = fileFragment.getBitmapImageUser();

        userId = fileFragment.getUserID();
        commentId = jsonObject.get("comment_id").getAsString();
        LinearLayout.LayoutParams paramsLinearBackGroundComment = paramsLinearBackGroundComment();
        LinearLayout.LayoutParams paramsLinearBackGroundMessage = paramsLinearBackGroundMessage();
        LinearLayout.LayoutParams paramsLinearMarginLeft = paramsLinearMarginLeft();
        RelativeLayout.LayoutParams paramsRelativeBackgroundReply = paramsRelativeBackgroundReply();
        LinearLayout.LayoutParams paramsLinearBackgroundReply = paramsLinearBackgroundReply();
        ViewGroup.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(paramsLinearBackGroundComment);

        setOrientation(LinearLayout.HORIZONTAL);
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        circleImageViewUses = new CircleImageView(context);
        linearBackgroundMessage = new LinearLayout(context);
        linearBackgroundName = new LinearLayout(context);
        linearBackgroundReply = new LinearLayout(context);
        linearBackgroundSendReply = new LinearLayout(context);
        relativeBackgroundMessage = new RelativeLayout(context);
        relativeBackgroundReply = new RelativeLayout(context);
        textViewName = new TextView(context);
        textViewDate = new TextView(context);
        textViewMessage = new TextView(context);
        editTextReply = new TextView(context);

        linearBackgroundMessage.setOrientation(LinearLayout.VERTICAL);
        linearBackgroundName.setOrientation(LinearLayout.HORIZONTAL);
        linearBackgroundReply.setOrientation(LinearLayout.VERTICAL);
        linearBackgroundSendReply.setOrientation(LinearLayout.HORIZONTAL);

        relativeBackgroundMessage.setBackgroundResource(R.drawable.background_message);
        textViewName.setTypeface(Typeface.DEFAULT_BOLD);
        textViewName.setTextColor(context.getResources().getColor(R.color.colorBlack));
        textViewName.setText(jsonObject.get("user_name").getAsString());
        textViewDate.setTypeface(Typeface.SERIF);
        String dateString = jsonObject.get("comment_date").getAsString();
        try {
            Date date = sdf.parse(dateString);
            SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy HH:mm");
            textViewDate.setText(formatter.format(date));
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
        }
        textViewMessage.setText(jsonObject.get("comment_message").getAsString());
        editTextReply.setTextColor(context.getResources().getColor(R.color.colorBlueStar));
        editTextReply.setText("reply");

//        circleImageViewUses.setImageResource(R.mipmap.note);
        byte[] bytes = Base64.decode(jsonObject.get("user_image").getAsString());
        bitmapResultUser = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        Log.i("Bitmap Input:",""+bitmapResultUser);
        circleImageViewUses.setImageBitmap(bitmapResultUser);
//        circleImageViewUses.setBackgroundResource(R.color.colorRed);

        relativeBackgroundReply.setOnClickListener(this);

        linearBackgroundName.addView(textViewName);
        linearBackgroundName.addView(textViewDate,paramsLinearMarginLeft);
        relativeBackgroundMessage.addView(textViewMessage);
        relativeBackgroundReply.addView(editTextReply);

        linearBackgroundMessage.addView(linearBackgroundName);
        linearBackgroundMessage.addView(relativeBackgroundMessage,params);
        linearBackgroundMessage.addView(relativeBackgroundReply,paramsRelativeBackgroundReply);
        linearBackgroundMessage.addView(linearBackgroundReply,paramsLinearBackgroundReply);

        addView(circleImageViewUses);
        addView(linearBackgroundMessage,paramsLinearBackGroundMessage);

    }

    @Override
    public void onClick(View v) {
        Log.i("Size",""+arrayListBackground.size());
        for(int i=0; i<arrayListBackground.size(); i++){
            if(arrayListBackground.get(i).getRelativeBackgroundReply() != relativeBackgroundReply){
                arrayListBackground.get(i).getLinearBackgroundReply().removeAllViews();
                arrayListBackground.get(i).getLinearBackgroundSendReply().removeAllViews();
                arrayListBackground.get(i).setCheckShow(false);
            }
        }
        if(v == relativeBackgroundReply && checkShow == false){
//            linearBackgroundReply.removeAllViews();
//            linearBackgroundSendReply.removeAllViews();
            checkShow = true;

            jsonObject = new JsonObject();
            jsonObject.addProperty("comment_id",commentId);
            Ion.with(getContext())
                    .load("http://guitar.msuproject.net/public/manageComment.php/selectCommentReply")
                    .setJsonObjectBody(jsonObject)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray result) {
                            Log.i("JsonArray Reply",""+result);
//                        linearBackGroundListTab = new BackgroundListTab[result.size()];
                            showCommentReply(result);
                        }
                    });
//            linearBackgroundMessage.addView(new BackgroundListReply(context,jsonObject));
        }
        else if(v == imageViewImageSend){
            jsonObject = new JsonObject();
            String currentTime = sdf.format(new Date());
            jsonObject.addProperty("user_id",userId);
            jsonObject.addProperty("comment_id",commentId);
            jsonObject.addProperty("commentReply_message",editTextSendReply.getText().toString());
            jsonObject.addProperty("commentReply_date",currentTime);
            if(!editTextSendReply.getText().toString().equals("")) {
                editTextSendReply.setText("");
                Ion.with(getContext())
                        .load("POST", "http://guitar.msuproject.net/public/managecomment.php/sendCommentReply")
                        .setJsonObjectBody(jsonObject)
                        .asJsonObject()
                        .withResponse()
                        .setCallback(new FutureCallback<Response<JsonObject>>() {
                            @Override
                            public void onCompleted(Exception e, Response<JsonObject> result) {
                                Log.i("Response Send", "" + result.getResult());
                                JsonObject jsonObject = new JsonParser().parse("" + result.getResult()).getAsJsonObject();
                                linearBackgroundReply.addView(new BackgroundListReply(context, jsonObject));
                                closeKeyboard();
                            }
                        });
            }
        }
    }

    private void showCommentReply(JsonArray jsonArray) {
        int n = 0;
        try {
            for (int i = 0; i < jsonArray.size() ; i++) {
//                Log.i("jsonArray", "" + jsonArray.get(i));
                JsonObject jsonObject = (JsonObject) jsonArray.get(i);
                linearBackgroundReply.addView(new BackgroundListReply(context,jsonObject));
            }

            LinearLayout.LayoutParams paramsLinearBackgroundSendReply = paramsLinearBackgroundSendReply();
            LinearLayout.LayoutParams paramsLinearEditTextSendReply = paramsLinearEditTextSendReply();
            LinearLayout.LayoutParams paramsLinearBackGroundImageSend = paramsLinearBackGroundImageSend();

            linearBackgroundSendReply = new LinearLayout(context);
            circleImageViewUses = new CircleImageView(context);
            editTextSendReply = new EditText(context);
            imageViewImageSend = new AppCompatImageView(context);

            circleImageViewUses.setImageBitmap(bitmapImageUser);
//            circleImageViewUses.setBackgroundResource(R.color.colorGreen);

//            byte[] bytes = Base64.decode(jsonObject.get("user_image").getAsString());
//            Bitmap bitmapResult = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
//            Log.i("Bitmap Input:",""+bitmapResult);
//            circleImageViewUses.setImageBitmap(bitmapResult);
            editTextSendReply.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
            editTextSendReply.setBackgroundResource(R.drawable.edittext_border);
            editTextSendReply.setTextSize(16);
            imageViewImageSend.setImageResource(R.mipmap.baseline_send_black_18dp);
            imageViewImageSend.setColorFilter(getContext().getResources().getColor(R.color.colorBlueStar));

            imageViewImageSend.setOnClickListener(this);

            linearBackgroundSendReply.addView(circleImageViewUses);
            linearBackgroundSendReply.addView(editTextSendReply,paramsLinearEditTextSendReply);
            linearBackgroundSendReply.addView(imageViewImageSend,paramsLinearBackGroundImageSend);
            linearBackgroundMessage.addView(linearBackgroundSendReply,paramsLinearBackgroundSendReply);
            linearBackgroundMessage.invalidate();
        } catch (Exception e1) {
            Log.i("JSONException", "Kuy");
            e1.printStackTrace();
        }
    }
    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)fileFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(fileFragment.getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    private void openKeyboard() {
        InputMethodManager imm = (InputMethodManager)fileFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null){
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    private LinearLayout.LayoutParams paramsLinearBackGroundComment() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = dpToPx(8);
        layoutParams.rightMargin = dpToPx(8);
        layoutParams.topMargin = dpToPx(3);
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearMarginLeft() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = dpToPx(8);
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearBackGroundMessage() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearBackgroundReply() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearBackgroundSendReply() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.topMargin = dpToPx(3);
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearEditTextSendReply() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT,1);
        layoutParams.gravity = Gravity.CENTER;
        return layoutParams;
    }
    private LinearLayout.LayoutParams paramsLinearBackGroundImageSend() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dpToPx(30),dpToPx(30));
        layoutParams.leftMargin = dpToPx(8);
        layoutParams.gravity = Gravity.CENTER;
        return layoutParams;
    }
    private RelativeLayout.LayoutParams paramsRelativeBackgroundReply() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        return layoutParams;
    }
    private RelativeLayout.LayoutParams paramsRelativeBackgroundSendReply() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.topMargin = dpToPx(50);
        return layoutParams;
    }

    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    public void setCommentId(String commentId){
        this.commentId = commentId;
    }
    public String getCommentId(){
        return this.commentId;
    }
    public void setRelativeBackgroundReply(RelativeLayout relativeBackgroundReply){
        this.relativeBackgroundReply = relativeBackgroundReply;
    }
    public RelativeLayout getRelativeBackgroundReply(){
        return this.relativeBackgroundReply;
    }
    public void setLinearBackgroundMessage(LinearLayout linearBackgroundMessage){
        this.linearBackgroundMessage = linearBackgroundMessage;
    }
    public LinearLayout getLinearBackgroundMessage(){
        return this.linearBackgroundMessage;
    }
    public void setLinearBackgroundReply(LinearLayout linearBackgroundReply){
        this.linearBackgroundReply = linearBackgroundReply;
    }
    public LinearLayout getLinearBackgroundReply(){
        return this.linearBackgroundReply;
    }
    public void setLinearBackgroundSendReply(LinearLayout linearBackgroundSendReply){
        this.linearBackgroundSendReply = linearBackgroundSendReply;
    }
    public LinearLayout getLinearBackgroundSendReply(){
        return this.linearBackgroundSendReply;
    }
    public void setCheckShow(boolean checkShow){
        this.checkShow = checkShow;
    }
    public boolean getCheckShow(){
        return this.checkShow;
    }



}
