package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.manager.SQLiteHelper;
import com.google.gson.JsonObject;
import com.itextpdf.text.pdf.codec.Base64;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.COLUMN_USER_ID;
import static com.example.suphotkhotphoothon.fingerstyleapplication4.util.ConstantsUser.TABLE_USER_NAME;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment implements View.OnClickListener{
    View view;
    private TextView textUpdateProfile;
    private TextView textEditPassword;
    private TextView textViewErrorFirstName;
    private TextView textViewErrorLastName;
    private TextView textViewErrorCurrentPassword;
    private TextView textViewErrorPassword;
    private TextView textViewErrorConfirmPassword;
    private TextView textViewSpinnerDay;
    private TextView textViewSpinnerMonth;
    private TextView textViewSpinnerYear;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextEmail;
    private EditText editTextCurrentPassword;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private Button buttonSave;
    private LinearLayout linearBackgroundPassword;
    private RelativeLayout relativeBackgroundEditPassword;
    private RadioButton radioButtonMan;
    private RadioButton radioButtonFemale;
    private RadioButton checkedRadioButton;
    private RadioGroup radioGroupGender;
    private Spinner spinnerDay;
    private Spinner spinnerMonth;
    private Spinner spinnerYear;
    private ArrayAdapter<String> adapterMonth;
    private ArrayAdapter<String> adapterDay;
    private ArrayAdapter<String> adapterYear;
    private ArrayList<String> listYear;
    private CircleImageView imageProfile;

    private String userID = null;
    private String userPassword = null;
    private String userNewPassword = null;
    private float density;
    private boolean checkSave;
    private boolean checkEditPassword = false;

    private JsonObject jsonObject;
    private SimpleDateFormat sdf;
    private SQLiteHelper sqLiteHelper;
    private Cursor cursor;

    private static final Pattern VALID_PASSWORD_REGEX = Pattern.compile("[^a-zA-Z'0-9]|^'|'$|''\"");
    private static final Pattern VALID_NAME_REGEX = Pattern.compile("[^\\u0E00-\\u0E7Fa-zA-Z' ]|^'|'$|''\"");

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_account, container, false);

        init();

        cursor = getUserID();
        cursor.moveToNext();
        Log.w("cursor",""+cursor.getCount());
        if (cursor.getCount() > 0) {
            userID = cursor.getString(0);
            Log.w("UserID",""+userID);
            Boolean connect = connectNetwork();
            if(connect) {
                selectAllUser();
            }
            else{
                messageDialogConnect();
            }
        }
        else {
            userID = null;
            Log.w("UserID",""+userID);
        }

        return view;
    }


    private void init() {
        imageProfile = (CircleImageView)view.findViewById(R.id.fragmentAccount_image_profile);
        textUpdateProfile = (TextView)view.findViewById(R.id.fragmentAccount_textView_updateProfile);
        textEditPassword = (TextView)view.findViewById(R.id.fragmentAccount_textView_editPassword);
        textViewErrorFirstName = (TextView)view.findViewById(R.id.fragmentAccount_textView_errorFirstName);
        textViewErrorLastName = (TextView)view.findViewById(R.id.fragmentAccount_textView_errorLastName);
        editTextFirstName = (EditText)view.findViewById(R.id.fragmentAccount_editText_firstName);
        editTextLastName = (EditText)view.findViewById(R.id.fragmentAccount_editText_lastName);
        editTextEmail = (EditText)view.findViewById(R.id.fragmentAccount_ditText_email);
        buttonSave = (Button)view.findViewById(R.id.fragmentAccount_button_save);
        linearBackgroundPassword = (LinearLayout)view.findViewById(R.id.fragmentAccount_ll_backgroundPassword);
        radioButtonMan = (RadioButton)view.findViewById(R.id.fragmentAccount_radioButton_genderMan);
        radioButtonFemale = (RadioButton)view.findViewById(R.id.fragmentAccount_radioButton_genderFemale);
        radioGroupGender = (RadioGroup) view.findViewById(R.id.fragmentAccount_radioGroup_gender);
        spinnerDay = (Spinner)view.findViewById(R.id.fragmentAccount_spinner_day);
        spinnerMonth = (Spinner)view.findViewById(R.id.fragmentAccount_spinner_month);
        spinnerYear = (Spinner)view.findViewById(R.id.fragmentAccount_spinner_year);

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        String[] lishDay = getResources().getStringArray(R.array.day);
        String[] lishMonth = getResources().getStringArray(R.array.month);
        listYear = new ArrayList<String>();

        sqLiteHelper = new SQLiteHelper(getContext());

        createYear();

        adapterDay = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lishDay);
        adapterMonth = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lishMonth);
        adapterYear = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, listYear);

        spinnerDay.setAdapter(adapterDay);
        spinnerMonth.setAdapter(adapterMonth);
        spinnerYear.setAdapter(adapterYear);

        radioButtonMan.setOnClickListener(this);
        radioButtonFemale.setOnClickListener(this);
        textUpdateProfile.setOnClickListener(this);
        textEditPassword.setOnClickListener(this);
        buttonSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == radioButtonMan || v == radioButtonFemale){
            int selectedId = radioGroupGender.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            checkedRadioButton = (RadioButton) view.findViewById(selectedId);
        }
        else if(v == textUpdateProfile){
            Intent intent = new Intent(Intent.ACTION_PICK);
            Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            intent.setData(uri);
            startActivityForResult(intent,555);
        }
        else if(v == textEditPassword){
            checkEditPassword = true;
            textEditPassword.setVisibility(View.GONE);

            editTextCurrentPassword = new EditText(getContext());
            editTextPassword = new EditText(getContext());
            editTextConfirmPassword = new EditText(getContext());
            textViewErrorCurrentPassword = new TextView(getContext());
            textViewErrorPassword = new TextView(getContext());
            textViewErrorConfirmPassword = new TextView(getContext());

            LinearLayout.LayoutParams paramsLinearPassword = paramsLinearPassword();
            LinearLayout.LayoutParams paramsLinearErrorPassword = paramsLinearErrorPassword();

            editTextCurrentPassword.setLayoutParams(paramsLinearPassword);
            editTextPassword.setLayoutParams(paramsLinearPassword);
            editTextConfirmPassword.setLayoutParams(paramsLinearPassword);
            textViewErrorCurrentPassword.setLayoutParams(paramsLinearErrorPassword);
            textViewErrorPassword.setLayoutParams(paramsLinearErrorPassword);
            textViewErrorConfirmPassword.setLayoutParams(paramsLinearErrorPassword);

            textViewErrorCurrentPassword.setTextSize(16);
            textViewErrorCurrentPassword.setTextColor(this.getResources().getColor(R.color.colorRed));
            textViewErrorPassword.setTextSize(16);
            textViewErrorPassword.setTextColor(this.getResources().getColor(R.color.colorRed));
            textViewErrorConfirmPassword.setTextSize(16);
            textViewErrorConfirmPassword.setTextColor(this.getResources().getColor(R.color.colorRed));
            editTextCurrentPassword.setHint("current password");
            editTextCurrentPassword.setTextSize(16);
            editTextCurrentPassword.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
            editTextCurrentPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            editTextCurrentPassword.setBackgroundResource(R.drawable.edittext_border_search);
            editTextPassword.setHint("new password");
            editTextPassword.setTextSize(16);
            editTextPassword.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
//            editTextPassword.setGravity(Gravity.CENTER);
            editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            editTextPassword.setBackgroundResource(R.drawable.edittext_border_search);
            editTextConfirmPassword.setHint("confirm password");
            editTextConfirmPassword.setTextSize(16);
            editTextConfirmPassword.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
            editTextConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            editTextConfirmPassword.setBackgroundResource(R.drawable.edittext_border_search);

            linearBackgroundPassword.addView(editTextCurrentPassword);
            linearBackgroundPassword.addView(textViewErrorCurrentPassword);
            linearBackgroundPassword.addView(editTextPassword);
            linearBackgroundPassword.addView(textViewErrorPassword);
            linearBackgroundPassword.addView(editTextConfirmPassword);
            linearBackgroundPassword.addView(textViewErrorConfirmPassword);
        }
        else if(v == buttonSave){
            closeKeyboard();
            checkSave = true;
            textViewErrorFirstName.setText("");
            textViewErrorLastName.setText("");

            String firstName = editTextFirstName.getText().toString();
            String lastName = editTextLastName.getText().toString();

            if(firstName.equals("")){
                checkSave = false;

                textViewErrorFirstName.setText("*enter a name.");
            }
            else {
                if(validateName(firstName)){
                    checkSave = false;
                    textViewErrorFirstName.setText("*enter a-z, ก-ฮ only.");
                }
            }

            if (lastName.equals("")) {
                checkSave = false;

                textViewErrorLastName.setText("*enter a name.");
            } else {
                if (validateName(lastName)) {
                    checkSave = false;
                    textViewErrorLastName.setText("*enter a-z, ก-ฮ only.");
                }
            }

            if(checkEditPassword) {
                Log.w("User Password",userPassword);
                textViewErrorCurrentPassword.setText("");
                textViewErrorPassword.setText("");
                textViewErrorConfirmPassword.setText("");

                String currentPassword = editTextCurrentPassword.getText().toString();
                String password = editTextPassword.getText().toString();
                userNewPassword = password;
                String confirmPassword = editTextConfirmPassword.getText().toString();

                if (currentPassword.equals("")) {
                    checkSave = false;
                    textViewErrorCurrentPassword.setText("*enter a password.");
                } else {
                    if (!currentPassword.equals(userPassword)) {
                        checkSave = false;
                        textViewErrorCurrentPassword.setText("*password is incorrect.");
                    }
                }

                if(password.equals("")){
                    checkSave = false;
                    textViewErrorPassword.setText("*enter a password.");
                }
                else {
                    if(validatePassword(password)){
                        checkSave = false;
                        textViewErrorPassword.setText("*enter a-z, 0-9 only.");
                    }
                    else {
                        if(password.length()<6){
                            checkSave = false;
                            textViewErrorPassword.setText("*Must have a length of 6 or more.");
                        }
                    }
                }

                if(confirmPassword.equals("")){
                    checkSave = false;
                    textViewErrorConfirmPassword.setText("*enter a password.");
                }
                else {
                    if(validatePassword(confirmPassword)){
                        checkSave = false;
                        textViewErrorConfirmPassword.setText("*enter a-z, 0-9 only.");
                    }
                    else {
                        if(confirmPassword.length()<6){
                            checkSave = false;
                            textViewErrorConfirmPassword.setText("*Must have a length of 6 or more.");
                        }
                        else {
                            if (!confirmPassword.equals(password)){
                                checkSave = false;
                                textViewErrorConfirmPassword.setText("*enter the same password.");
                            }
                        }
                    }
                }
            }
            Log.w("Chack save",""+checkSave);
            if(checkSave){

                textViewSpinnerDay = (TextView)spinnerDay.getSelectedView();
                textViewSpinnerMonth = (TextView)spinnerMonth.getSelectedView();
                textViewSpinnerYear = (TextView)spinnerYear.getSelectedView();

                String day = textViewSpinnerDay.getText().toString();
                String month = textViewSpinnerMonth.getText().toString();
                String year = textViewSpinnerYear.getText().toString();
                int monthNumber = 0;
                for(int i=0;i<adapterMonth.getCount();i++){
                    if(month == adapterMonth.getItem(i)){
                        monthNumber = i+1;
                    }
                }

                int selectedId = radioGroupGender.getCheckedRadioButtonId();
                checkedRadioButton = (RadioButton) view.findViewById(selectedId);
                Log.i("selectedId",""+checkedRadioButton.getText().toString());
                String gender = checkedRadioButton.getText().toString();

                jsonObject = new JsonObject();
                jsonObject.addProperty("user_id",userID);
                jsonObject.addProperty("user_firstName",firstName);
                jsonObject.addProperty("user_lastName",lastName);
                jsonObject.addProperty("user_gender",gender);
                jsonObject.addProperty("user_birthday",year+"-"+String.format("%02d",monthNumber)+"-"+day);
                jsonObject.addProperty("user_password",userNewPassword);
                if(connectNetwork()){
                    Ion.with(getContext())
                            .load("POST","http://guitar.msuproject.net/public/user.php/updateUser")
                            .setJsonObjectBody(jsonObject)
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {
                                    Log.i("Result",""+result);
                                    Toast.makeText(getActivity(), "" +result.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }
                else {
                    messageDialogConnect();
                }
            }
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode != RESULT_OK){
            Log.w("Kuy","1");
            return;
        }
        if(requestCode == 555){
            Log.w("Kuy","2");
            Uri uri = data.getData();
            Intent intentCrop = new Intent("com.android.camera.action.CROP")
                    .setDataAndType(uri,"image/*")
                    .putExtra("crop",true)
                    .putExtra("aspectX",1)
                    .putExtra("aspectY",1)
                    .putExtra("outputX",400)
                    .putExtra("outputY",400)
                    .putExtra("scale",true)
                    .putExtra("return-data",true);

            intentCrop.putExtra("outputFormat", Bitmap.CompressFormat.JPEG);
            startActivityForResult(intentCrop,999);
        }
        else if(requestCode == 999){
            Bundle bundle = data.getExtras();
            Bitmap bitmap = bundle.getParcelable("data");
//            imageProfile.setImageBitmap(bitmap);
            updateImageProfile(bitmap);
        }
    }

    private void createYear() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for(int i = year-200;i<=year;i++){
            listYear.add(""+i);
        }
    }

    private void selectAllUser() {
        jsonObject = new JsonObject();
        jsonObject.addProperty("user_id",userID);
        Ion.with(getContext())
                .load("POST","http://guitar.msuproject.net/public/user.php/user")
                .setJsonObjectBody(jsonObject)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.i("JsonObject",""+result);
                        try{
                            byte[] bytes = Base64.decode(result.get("user_image").getAsString());
                            Bitmap bitmapResult = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            Log.i("Bitmap Input:", "" + bitmapResult);
                            imageProfile.setImageBitmap(bitmapResult);
                        }catch (Exception ex) {
                            imageProfile.setImageResource(R.mipmap.note);
                            ex.printStackTrace();
                        }
                        userPassword = result.get("user_password").getAsString();
                        editTextFirstName.setText(result.get("user_firstName").getAsString());
                        editTextLastName.setText(result.get("user_lastName").getAsString());
                        if(result.get("user_gender").getAsString().equals("Man")){
                            radioButtonMan.setChecked(true);
                        }
                        else {
                            radioButtonFemale.setChecked(true);
                        }
                        try {
                            Log.i("Input date",""+result.get("user_birthday").getAsString());
                            Date date = sdf.parse(result.get("user_birthday").getAsString());
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            Log.i("Date","Day:"+calendar.get(Calendar.DAY_OF_MONTH)+" Month:"+(calendar.get(Calendar.MONTH)+1)+" Year:"+calendar.get(Calendar.YEAR));
                            spinnerDay.setSelection(calendar.get(Calendar.DAY_OF_MONTH)-1);
                            spinnerMonth.setSelection(calendar.get(Calendar.MONTH));
                            for (int i=0;i<adapterYear.getCount();i++){
//                                Log.i("Year",""+adapterYear.getItem(i)+" == "+calendar.get(Calendar.YEAR));
//                                Log.i("Year",""+(adapterYear.getItem(i).equals(""+calendar.get(Calendar.YEAR))));
                                if(adapterYear.getItem(i).equals(""+calendar.get(Calendar.YEAR))){
                                    spinnerYear.setSelection(i);
                                    break;
                                }
                            }

                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                        editTextEmail.setText(result.get("user_email").getAsString());
//                        Log.i("spinnerDay",""+spinnerDay.getAdapter().getItem(0));
//                        Log.i("spinnerDay",""+;);
//                        spinnerMonth.setAdapter(adapterMonth);

                    }
                });
    }
    private void updateImageProfile(Bitmap bitmap) {
        Log.w("Bitmap:",""+bitmap);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] photoByte = stream.toByteArray();
        String encodedImage = Base64.encodeBytes(photoByte);
        Log.w("Byte:",""+encodedImage);
        Ion.with(getContext())
                .load("POST","http://guitar.msuproject.net/public/updateProfile.php")
                .setBodyParameter("user_id",userID)
                .setBodyParameter("user_image",encodedImage)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.i("Result",""+result);
                        byte[] bytes = Base64.decode(result);
                        Bitmap bitmapResult = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        Log.i("Bitmap Input:",""+bitmapResult);
                        imageProfile.setImageBitmap(bitmapResult);
                    }
                });

    }

    private Boolean connectNetwork() {
        ConnectivityManager connectManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectManager.getActiveNetworkInfo();
        String resultConnect = "";
        boolean connect = false;

        if(netInfo != null && netInfo.isConnected()){
            resultConnect = "Connect success";
            connect = true;
        }
        else {
            resultConnect = "Connect fail";
            connect = false;
        }
        Log.w("Connect Network",resultConnect);
        return connect;
    }

    private void messageDialogConnect() {
        String message = "Please connect to the internet.";

        new AlertDialog.Builder(getActivity())
                .setIcon(R.mipmap.baseline_report_black_48dp)
                .setTitle("Internet")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(connectNetwork()){
                            selectAllUser();
                        }
                        else {
                            messageDialogConnect();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Cancel","Cancel");
                        getActivity().onBackPressed();
                    }
                })
                .show();
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(getActivity().getCurrentFocus() != null) {
            inputManager.hideSoftInputFromWindow(this.getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static boolean validateName(String nameString) {
        Matcher matcher = VALID_NAME_REGEX.matcher(nameString);
        return matcher.find();
    }
    public static boolean validatePassword(String passwordString) {
        Matcher matcher = VALID_PASSWORD_REGEX.matcher(passwordString);
        return matcher.find();
    }

    private LinearLayout.LayoutParams paramsLinearPassword() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = dpToPx(50);
        params.topMargin = dpToPx(0);
        return params;
    }
    private LinearLayout.LayoutParams paramsLinearErrorPassword() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = dpToPx(50);
        params.topMargin = dpToPx(0);
        params.bottomMargin = dpToPx(2);
        return params;
    }


    public int dpToPx(int dp) {
        density = this.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    private Cursor getUserID() {
        String sql = "SELECT "+COLUMN_USER_ID+" FROM "+ TABLE_USER_NAME;
        SQLiteDatabase db = sqLiteHelper.getReadableDatabase();
        return db.rawQuery(sql,null);
    }

}
