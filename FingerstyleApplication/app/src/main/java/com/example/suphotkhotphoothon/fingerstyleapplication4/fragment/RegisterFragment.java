package com.example.suphotkhotphoothon.fingerstyleapplication4.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suphotkhotphoothon.fingerstyleapplication4.R;
import com.example.suphotkhotphoothon.fingerstyleapplication4.activity.MainActivity;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements View.OnClickListener{


    private TextView textViewSpinnerDay;
    private TextView textViewSpinnerMonth;
    private TextView textViewSpinnerYear;
    private TextView textViewErrorName;
    private TextView textViewErrorEmail;
    private TextView textViewErrorPassword;
    private TextView textViewErrorGender;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private Button buttonOk;
    private RadioGroup radioGroupGender;
    private RadioButton radioButtonMan;
    private RadioButton radioButtonFemale;
    private RadioButton checkedRadioButton;
    private Spinner spinnerDay;
    private Spinner spinnerMonth;
    private Spinner spinnerYear;
    private ArrayAdapter<String> adapterMonth;
    private ArrayAdapter<String> adapterDay;
    private ArrayAdapter<String> adapterYear;
    private ArrayList<String> listYear;
    private View view;

    private FragmentManager fragmentManager;
    private JsonObject jsonObjectRegister;
    private FragmentTransaction fragmentTran;
    private LoginFragment loginFragment;

    private String day = "",month = "",year = "";
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern VALID_PASSWORD_REGEX = Pattern.compile("[^a-zA-Z'0-9]|^'|'$|''\"");
    private static final Pattern VALID_NAME_REGEX = Pattern.compile("[^\\u0E00-\\u0E7Fa-zA-Z' ]|^'|'$|''\"");


    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_register, container, false);

        listYear = new ArrayList<String>();

        textViewErrorName = (TextView)view.findViewById(R.id.fragmentRegister_textView_errorName);
        textViewErrorEmail = (TextView)view.findViewById(R.id.fragmentRegister_textView_errorEmail);
        textViewErrorPassword = (TextView)view.findViewById(R.id.fragmentRegister_textView_errorPassword);
        textViewErrorGender = (TextView)view.findViewById(R.id.fragmentRegister_textView_errorGender);
        editTextFirstName = (EditText) view.findViewById(R.id.fragmentRegister_et_firstName);
        editTextLastName = (EditText) view.findViewById(R.id.fragmentRegister_et_lastName);
        editTextEmail = (EditText) view.findViewById(R.id.fragmentRegister_et_email);
        editTextPassword = (EditText) view.findViewById(R.id.fragmentRegister_et_password);
        editTextConfirmPassword = (EditText) view.findViewById(R.id.fragmentRegister_et_confirmPassword);
        radioGroupGender = (RadioGroup) view.findViewById(R.id.fragmentRegister_radioGroup_gender);
        radioButtonMan = (RadioButton) view.findViewById(R.id.fragmentRegister_radioButton_genderMan);
        radioButtonFemale = (RadioButton) view.findViewById(R.id.fragmentRegister_radioButton_genderFemale);

        buttonOk = (Button) view.findViewById(R.id.fragmentRegister_button_ok);
        spinnerDay = (Spinner)view.findViewById(R.id.fragmentRegister_spinner_day);
        spinnerMonth = (Spinner)view.findViewById(R.id.fragmentRegister_spinner_month);
        spinnerYear = (Spinner)view.findViewById(R.id.fragmentRegister_spinner_year);


        String[] lishDay = getResources().getStringArray(R.array.day);
        String[] lishMonth = getResources().getStringArray(R.array.month);

        createYear();

        jsonObjectRegister = new JsonObject();
        loginFragment = new LoginFragment();
        fragmentManager = getActivity().getSupportFragmentManager();

        adapterDay = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lishDay);
        adapterMonth = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lishMonth);
        //Toast.makeText(getActivity(), ""+adapterMonth.getCount(), Toast.LENGTH_SHORT).show();
        adapterYear = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, listYear);

        spinnerDay.setAdapter(adapterDay);
        spinnerMonth.setAdapter(adapterMonth);
        spinnerYear.setAdapter(adapterYear);

        radioButtonMan.setOnClickListener(this);
        radioButtonFemale.setOnClickListener(this);
        // spinnerDay.setOnItemClickListener(this);
        buttonOk.setOnClickListener(this);

        return view;
    }

    private void createYear() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for(int i = year-200;i<=year;i++){
            listYear.add(""+i);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == buttonOk){
            Boolean checkRegister = true;
            String gender = null;
            textViewErrorEmail.setText("");
            textViewErrorPassword.setText("");
            textViewErrorName.setText("");
            textViewErrorGender.setText("");

            textViewSpinnerDay = (TextView)spinnerDay.getSelectedView();
            textViewSpinnerMonth = (TextView)spinnerMonth.getSelectedView();
            textViewSpinnerYear = (TextView)spinnerYear.getSelectedView();

            day = textViewSpinnerDay.getText().toString();
            month = textViewSpinnerMonth.getText().toString();
            year = textViewSpinnerYear.getText().toString();
            int monthNumber = 0;
            for(int i=0;i<adapterMonth.getCount();i++){
                if(month == adapterMonth.getItem(i)){
                    monthNumber = i+1;
                }
            }


            jsonObjectRegister.addProperty("user_email",editTextEmail.getText().toString());
            jsonObjectRegister.addProperty("user_password",editTextPassword.getText().toString());
            jsonObjectRegister.addProperty("user_firstName",editTextFirstName.getText().toString());
            jsonObjectRegister.addProperty("user_lastName",editTextLastName.getText().toString());
            if(checkedRadioButton != null){
                gender = checkedRadioButton.getText().toString();
            }
            jsonObjectRegister.addProperty("user_gender",gender);
            jsonObjectRegister.addProperty("user_birthday",year+"-"+String.format("%02d",monthNumber)+"-"+day);
            jsonObjectRegister.addProperty("user_registerDate",getPresentTime());
            jsonObjectRegister.addProperty("user_status","customer");

//            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.note);
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//            byte[] photoByte = stream.toByteArray();
//            String encodedImage = Base64.encodeBytes(photoByte);
//
//            jsonObjectRegister.addProperty("user_image",encodedImage);
            String firstName = editTextFirstName.getText().toString();
            String lastName = editTextLastName.getText().toString();
            String email = editTextEmail.getText().toString();
            String password = editTextPassword.getText().toString();
            String confirmPassword = editTextConfirmPassword.getText().toString();
            if(firstName.equals("") || lastName.equals("")){
                checkRegister = false;

                textViewErrorName.setText("*please enter a name.");
            }
            else {
                if(validateName(firstName) || validateName(lastName)){
                    checkRegister = false;
                    textViewErrorName.setText("*please enter a-z, ก-ฮ only.");
                }
            }
//            Log.i("Check Name",""+validateName(editTextFirstName.getText().toString()));
            if(email.equals("")){
                checkRegister = false;
                textViewErrorEmail.setText("*please enter a email.");
            }
            else {
                if(!validateEmail(email)){
                    checkRegister = false;
                    textViewErrorEmail.setText("*please enter a valid email.");
                }
            }
            if(password.equals("")||confirmPassword.equals("")){
                Log.i("Check ",""+(""+editTextPassword.getText() == ""));
                Log.i("Check ",""+(!editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())));
                Log.i("Check ",""+validatePassword(editTextPassword.getText().toString()));
                Log.i("Check ",""+editTextPassword.getText()+" : "+editTextConfirmPassword.getText());
                checkRegister = false;
                textViewErrorPassword.setText("*please enter a password.");
            }
            else {
                if(!password.equals(confirmPassword)){
                    checkRegister = false;
                    textViewErrorPassword.setText("*please enter the same password.");
                }
                else {
                    if(validatePassword(password) || validatePassword(confirmPassword)){
                        checkRegister = false;
                        textViewErrorPassword.setText("*please enter a-z, 0-9 only.");
                    }
                    else {
                        if(password.length()<6){
                            checkRegister = false;
                            textViewErrorPassword.setText("*Must have a length of 6 or more.");
                        }
                    }
                }
            }

            if(gender == null){
                checkRegister = false;
                textViewErrorGender.setText("*error");
            }
            if(checkRegister) {
                if(connectNetwork()){
                    register();
                }
                else {
                    messageDialogConnect();
                }
            }

        }
        else if(v == radioButtonMan || v == radioButtonFemale){
            int selectedId = radioGroupGender.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            checkedRadioButton = (RadioButton) view.findViewById(selectedId);
        }
    }

    private void register(){
        Ion.with(this)
                .load("POST", "http://guitar.msuproject.net/public/user.php/register")
                .setJsonObjectBody(jsonObjectRegister)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {

                        String message = result.getResult().toString();
                        if (message.equals("Duplicate")) {
                            textViewErrorEmail.setText("*error");
                        }
                        else if(message.equals("Success")){
                            Toast.makeText(getActivity(), "" + result.getResult(), Toast.LENGTH_SHORT).show();
                            if (fragmentManager.getBackStackEntryCount() > 0) {
                                fragmentManager.popBackStack();
                            }
                        }
                    }
                });
    }

    private Boolean connectNetwork() {
        ConnectivityManager connectManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectManager.getActiveNetworkInfo();
        String resultConnect = "";
        boolean connect = false;

        if(netInfo != null && netInfo.isConnected()){
            resultConnect = "Connect success";
            connect = true;
        }
        else {
            resultConnect = "Connect fail";
            connect = false;
        }
        Log.w("Connect Network",resultConnect);
        return connect;
    }

    private void messageDialogConnect() {
        String message = "Please connect to the internet.";

        new AlertDialog.Builder(getActivity())
                .setIcon(R.mipmap.baseline_report_black_48dp)
                .setTitle("Internet")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(connectNetwork()){
                            register();
                        }
                        else {
                            messageDialogConnect();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.w("Cancel","Cancel");
                    }
                })
                .show();
    }

    public static boolean validatePassword(String passwordString) {
        Matcher matcher = VALID_PASSWORD_REGEX.matcher(passwordString);
        return matcher.find();
    }
    public static boolean validateEmail(String emailString) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailString);
        return matcher.find();
    }
    public static boolean validateName(String nameString) {
        Matcher matcher = VALID_NAME_REGEX.matcher(nameString);
        return matcher.find();
    }

    public String getPresentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

}
